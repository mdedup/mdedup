package de.hpi.is.mdedup.prediction;

import de.hpi.is.mdedup.MDedupIO;
import de.hpi.is.mdedup.selection.MDedupSelectionIO;
import de.hpi.is.mdedup.selection.utils.OptimalMDCInfo;
import de.hpi.is.mdedup.utils.ClassificationEvaluation;
import de.hpi.is.mdedup.utils.entities.MDC;
import org.apache.commons.lang3.StringUtils;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class MDedupPredictionIO extends MDedupIO {

    MDedupSelectionIO mDedupSelectionIO;

    public MDedupPredictionIO(String dbDriver, String dbConnection, String dbUser, String dbPassword) {
        super(dbDriver, dbConnection, dbUser, dbPassword);
        mDedupSelectionIO = new MDedupSelectionIO(conn);
    }

    public MDedupPredictionIO(Connection conn) {
        super(conn);
        mDedupSelectionIO = new MDedupSelectionIO(conn);
    }


    public static class PrepareStatementParametersPrediction extends PrepareStatementParameters {
        protected Map<MDC, OptimalMDCInfo> mdcsPredicted;
        protected Integer maxLevel;

        protected String predictionModel;
        protected String predictionModelParameters;

        protected Boolean isExecutionInterrupted;
        protected Long maxDuration;
        protected Long latticeExecutionDuration;
        protected Long totalExecutionDuration;

        protected List<Map<String, Object>> data;

        protected Integer topk;
        protected Boolean enabledExpansionPhase;
        protected String featuresInRegression;

        public PrepareStatementParametersPrediction(String dataset, Map<MDC, OptimalMDCInfo> mdcsPredicted,
                                                    Integer maxLevel, Integer topk, Boolean isExecutionInterrupted,
                                                    Long maxDuration, String predictionModelParameters,
                                                    Long latticeExecutionDuration, Long totalExecutionDuration,
                                                    Boolean enabledExpansionPhase,
                                                    String featuresInRegression) {
            super(dataset, "mdedup_mdcs");
            this.mdcsPredicted = mdcsPredicted;
            this.maxLevel = maxLevel;

            this.maxDuration = maxDuration;
            this.latticeExecutionDuration = latticeExecutionDuration;
            this.predictionModelParameters = predictionModelParameters;
            this.totalExecutionDuration = totalExecutionDuration;
            this.isExecutionInterrupted = isExecutionInterrupted;

            this.topk = topk;
            this.enabledExpansionPhase = enabledExpansionPhase;
            this.featuresInRegression = featuresInRegression;
        }

        public List<Map<String, Object>> prepareData() {
            Set<String> header = new HashSet<>();
            data = mdcsPredicted.entrySet().stream().map(e -> {
                Map<String, Object> r = new HashMap<>();

                r.put("dataset", dataset);
                r.put("ids", StringUtils.join(e.getKey().getMDsIDsSorted(), "_"));
                List<String> children = e.getValue().getChildren().stream().map(MDC::toString).sorted(String::compareTo)
                        .collect(Collectors.toList());
                r.put("children", String.join("|", children));

//                r.put("phase", e.getValue().getPhase());
                r.put("phase", "prediction");

                r.put("max_level", maxLevel.toString());
                r.put("level", e.getValue().getLevel().toString());

                r.put("prediction_model", predictionModel);
                r.put("prediction_model_parameters", predictionModelParameters);
                r.put("prediction_score", e.getValue().getScore().toString());

                ClassificationEvaluation ce = e.getValue().getClassificationEvaluation();

                r.put("precision_", ce.calculatePrecision().toString());
                r.put("recall", ce.calculateRecall().toString());
                r.put("f1", ce.calculateFMeasure(1.0).toString());

                r.put("f05", ce.calculateFMeasure(0.5).toString());
                r.put("f20", ce.calculateFMeasure(2.0).toString());

                r.put("auc_roc", -1.0);
                r.put("auc_pr", -1.0);

                r.put("tp", ce.getTp().toString());
                r.put("fn", ce.getFn().toString());
                r.put("fp", ce.getFp().toString());
                r.put("tn", ce.getTn().toString());

                r.put("is_execution_interrupted", isExecutionInterrupted);
                r.put("lattice_max_duration", maxDuration.toString());
                r.put("lattice_actual_duration", latticeExecutionDuration.toString());
                r.put("lattice_md_duration", e.getValue().getDurationToCreation().toString());
                r.put("total_execution_duration", totalExecutionDuration.toString());

                r.put("topk_position", e.getValue().getTopkPosition().toString());

                r.put("topk", topk.toString());
                r.put("enabled_expansion_phase", enabledExpansionPhase);
                r.put("features_in_regression", featuresInRegression);

                for (String feature : mdcFeatures) {
                    r.put(feature, e.getKey().getFields().get(feature).toString());
                }

                if (header.size() == 0) {
                    header.addAll(r.keySet());
                }

                return r;
            }).collect(Collectors.toList());

            this.header = new ArrayList<>(header);
            this.header.sort(String::compareTo);

            return data;
        }
    }

}
