package de.hpi.is.mdedup.prediction.selection;

import de.hpi.is.mdedup.selection.MDedupSelectionAlgorithm;
import de.hpi.is.mdedup.exploration.MDedupExplorationController;
import de.hpi.is.mdedup.selection.utils.OptimalMDCInfo;
import de.hpi.is.mdedup.utils.SimilaritiesPair;
import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.entities.MDC;

import java.util.ArrayList;
import java.util.Map;

public class MDedupPredictionAlgorithm extends MDedupSelectionAlgorithm {

    public MDedupPredictionAlgorithm(String dataset, Map<String, MD> mds, SelectionStrategy selectionStrategy,
                                     Integer maxLevel, String regressionModel,
                                     Map<String, Map<String, String>> records,
                                     Map<Integer, SimilaritiesPair> sims, Map<String, MDC> mdcs,
                                     MDedupExplorationController explorationController, Integer maxDuration,
                                     Map<String, Double> modelParameters, Double sampleMDs) {
        super(dataset, mds, selectionStrategy, maxLevel, maxDuration, Boolean.FALSE, new OracleOfFeatures(dataset, regressionModel, mds, records,
                sims, mdcs, explorationController, modelParameters), sampleMDs);
    }

    @Override
    public OracleOfFeatures getOracle() {
        return (OracleOfFeatures) oracle;
    }

    @Override
    public Map<MDC, OptimalMDCInfo> execute() {
        Map<MDC, OptimalMDCInfo> mdcs = super.execute();
        OracleOfFeatures of = (OracleOfFeatures) oracle;
        Map<String, Map<String, Double>> featuresOfMDCs = of.calculateFeaturesForMDCs(new ArrayList<>(mdcs.keySet()));

        mdcs.keySet().forEach(mdc -> {
            Map<String, String> fields = mdc.getFields();
            Map<String, Double> features = featuresOfMDCs.get(mdc.getId());
            features.forEach((key, value) -> fields.put(key, value.toString()));
        });

        return mdcs;
    }
}
