package de.hpi.is.mdedup.prediction.selection;

import de.hpi.is.mdedup.exploration.MDedupExplorationController;
import de.hpi.is.mdedup.prediction.mlmodels.MDedupRegression;
import de.hpi.is.mdedup.prediction.mlmodels.MDedupRegressionGaussianProcess;
import de.hpi.is.mdedup.prediction.mlmodels.MDedupRegressionLinear;
import de.hpi.is.mdedup.utils.*;
import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.entities.MDC;
import de.hpi.is.mdedup.utils.entities.MDCondition;
import org.javatuples.Triplet;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;


/**
 * This oracle predicts the score of an MDC based on the regression model provided, which is trained on other annotated
 * datasets.
 *
 */
public class OracleOfFeatures extends Oracle {

    Map<String, MD> mds;
    Map<String, Map<String, String>> records;
    Map<Integer, SimilaritiesPair> sims;
    Map<String, MDC> mdcs;

    MDedupExplorationController explorationController;

    MDedupRegression regressionModel;

    Map<String, Double> mdcsScores;
    Map<String, ClassificationEvaluation> mdcsEvaluations;

    Map<String, Map<String, Double>> mdcsWithFeaturesCurrent;

    public OracleOfFeatures(String dataset, String model, Map<String, MD> mds, Map<String, Map<String, String>> records,
                            Map<Integer, SimilaritiesPair> sims, Map<String, MDC> mdcs,
                            MDedupExplorationController explorationController, Map<String, Double> modelParameters) {
        this.mds = mds;
        this.records = records;
        this.sims = sims;
        this.mdcs = mdcs;
        this.explorationController = explorationController;

        switch (model) {
            case "linear":
                regressionModel = new MDedupRegressionLinear(dataset, mdcs);
                break;
            case "gaussian_process":
                regressionModel = new MDedupRegressionGaussianProcess(dataset, mdcs,
                        modelParameters.getOrDefault("sigma", null),
                        modelParameters.getOrDefault("lambda", null)
                );
                break;
        }
    }


    @Override
    public void batchCalculateScoresForMDCs(List<MDC> mdcList) {
        mdcsWithFeaturesCurrent = calculateFeaturesForMDCs(mdcList);

        mdcsScores = mdcsWithFeaturesCurrent.entrySet().parallelStream().collect(Collectors.toMap(
                Map.Entry::getKey,
                mdc -> {
                    Map<String, Double> featureValues = mdc.getValue();
                    Map<String, String> featureValuesStr = featureValues.entrySet().stream().collect(Collectors.toMap(
                            Map.Entry::getKey, e -> e.getValue().toString()
                    ));
                    Double score = regressionModel.execute(featureValuesStr);
                    return score;
                }
        ));
    }

    @Override
    public Double fetchScoreMDC(MDC mdc) {
        return mdcsScores.get(mdc.getId());
    }

    @Override
    public void batchCalculateEvaluationsForMDCs(List<MDC> mdcList) {
        MD.MDSide mdside = MD.MDSide.LHS;
        Set<String> mdsParticipating = new HashSet<>();
        mdcList.forEach(mdc -> mdsParticipating.addAll(mdc.getMDsIDs()));

        Set<MDCondition> columnMatches = new ConcurrentSkipListSet<>();
        mdsParticipating.parallelStream().forEach(mdID -> {
            columnMatches.addAll(mds.get(mdID).getMDSide(mdside));
        });

        List<Triplet<String, String, String>> classifications = sims.entrySet().parallelStream().map(pairSimilarities -> {
            Map<String, Boolean> mdcsValidities = isPairSatisfiedByMDC(mdcList, mdside, mdsParticipating, columnMatches,
                    pairSimilarities, mds);

            String pairClass = pairSimilarities.getValue().getPairClass().name().toLowerCase();
            List<Triplet<String, String, String>> classificationsPair = mdcsValidities.entrySet().stream().map(mdcValidity -> {
                String mdc = mdcValidity.getKey();
                Boolean valid = mdcValidity.getValue();

                if (pairClass.equals("dpl") && valid) {
                    return new Triplet<>(pairSimilarities.getValue().getId(), mdc, "tp") ;
                } else if (pairClass.equals("dpl") && !valid) {
                    return new Triplet<>(pairSimilarities.getValue().getId(), mdc, "fn") ;
                } else if (pairClass.equals("ndpl") && valid) {
                    return new Triplet<>(pairSimilarities.getValue().getId(), mdc, "fp") ;
                } else if (pairClass.equals("ndpl") && !valid) {
                    return new Triplet<>(pairSimilarities.getValue().getId(), mdc, "tn") ;
                } else {
                    return null;
                }
            }).collect(Collectors.toList());
            return classificationsPair;
        }).flatMap(Collection::stream).collect(Collectors.toList());

        mdcsEvaluations = new HashMap<>();
        for (Triplet<String, String, String> classification : classifications) {
            String pairID = classification.getValue0();
            String mdcID = classification.getValue1();
            String classificationLabel = classification.getValue2();
            ClassificationEvaluation ce = mdcsEvaluations.getOrDefault(mdcID, new ClassificationEvaluation(0,0,0,0));
            switch (classificationLabel) {
                case "tp":
                    ce.setTp(ce.getTp() + 1);
                    break;
                case "fn":
                    ce.setFn(ce.getFn() + 1);
                    break;
                case "fp":
                    ce.setFp(ce.getFp() + 1);
                    break;
                case "tn":
                    ce.setTn(ce.getTn() + 1);
                    break;
            }
            mdcsEvaluations.put(mdcID, ce);
        }
    }

    public Map<String, Map<String, Double>> calculateFeaturesForMDCs(List<MDC> mdcList) {
        this.mdcs = new HashMap<>();
        mdcList.forEach(mdc -> mdcs.put(mdc.getId(), mdc));
        explorationController.setMdcs(mdcs);
        Map<String, Map<String, Double>> featuresWithMDCsCurrent = explorationController.getMDCsWithFeatures();
        return mdcList.parallelStream().collect(Collectors.toMap(
                MDC::getId,
                mdc -> {
                    Map<String, Double> mdcsFeatures = new HashMap<>();
                    for (String feature: featuresWithMDCsCurrent.keySet()) {
                        mdcsFeatures.put(feature, featuresWithMDCsCurrent.get(feature).get(mdc.getId()));
                    }
                    return mdcsFeatures;
                }));
    }

    @Override
    public ClassificationEvaluation fetchEvaluationMDC(MDC mdc) {
        return mdcsEvaluations.get(mdc.toString());
    }


    public MDedupRegression getRegressionModel() {
        return regressionModel;
    }

    public MDedupExplorationController getExplorationController() {
        return explorationController;
    }

    public Map<String, Map<String, Double>> getMdcsWithFeaturesCurrent() {
        return mdcsWithFeaturesCurrent;
    }


}
