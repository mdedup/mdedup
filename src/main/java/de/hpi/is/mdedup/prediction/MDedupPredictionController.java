package de.hpi.is.mdedup.prediction;

import de.hpi.is.mdedup.MDedupController;
import de.hpi.is.mdedup.utils.classifiedpairs.MDCClassifiedPairsIO;
import de.hpi.is.mdedup.boosting.classification.utils.ClassificationDedupPairScored;
import de.hpi.is.mdedup.prediction.mlmodels.MDedupRegression;
import de.hpi.is.mdedup.prediction.selection.MDedupPredictionAlgorithm;
import de.hpi.is.mdedup.selection.MDedupSelectionAlgorithm;
import de.hpi.is.mdedup.exploration.MDedupExplorationController;
import de.hpi.is.mdedup.selection.OracleOfDedup;
import de.hpi.is.mdedup.selection.utils.OptimalMDCInfo;
import de.hpi.is.mdedup.utils.DedupPair;
import de.hpi.is.mdedup.utils.SimilaritiesPair;
import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.entities.MDC;
import org.javatuples.Pair;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;


/**
 * This class implements the MDC prediction step.
 *
 * 1. It creates the regression model that is used to predict the score of an MDC using the MDCs produced
 * by the rest annotated datasets.
 * 2. It calls the MDC Selection class using this regression model (known as ORACLE) to score the MDCs.
 */
public class MDedupPredictionController extends MDedupController {
    protected String queryMDCsExploration;
    protected String dataset;
    protected Integer maxLevel;
    protected Integer maxDuration;
    protected String regressionModel;
    protected String exportClassifiedPairsBy;

    protected MDedupPredictionIO mdedupio;
    protected MDedupExplorationController explorationController;
    protected MDCClassifiedPairsIO clpio;

    protected Map<String, MDC> mdcsExplorationSampleHyperparameterTuning;

    protected Integer topk;
    protected Boolean enabledExpansionPhase;
    protected String featuresInRegression;

    public MDedupPredictionController(String dbUser, String dbPassword, String dbDriver, String dbConnection,
                                      MDedupExplorationController explorationController,
                                      String dataset, Integer topk, Integer maxLevel, String regressionModel,
                                      String queryMDCsExploration, Integer maxDuration, String exportClassifiedPairsBy,
                                      Boolean enabledExpansionPhase, String featuresInRegression) {
        super(dbUser, dbPassword, dbDriver, dbConnection);
        this.explorationController = explorationController;
        this.dataset = dataset;

        this.maxLevel = maxLevel;
        this.maxDuration = maxDuration;
        this.regressionModel = regressionModel;
        this.exportClassifiedPairsBy = exportClassifiedPairsBy;

        this.topk = topk;
        this.enabledExpansionPhase = enabledExpansionPhase;
        this.featuresInRegression = featuresInRegression;

        mdedupio = new MDedupPredictionIO(dbDriver, dbConnection, dbUser, dbPassword);
        clpio = new MDCClassifiedPairsIO(dbDriver, dbConnection, dbUser, dbPassword);

        this.queryMDCsExploration = queryMDCsExploration;

        importSimilarities("select * from sims_" + dataset);
    }

    public void execute() {
        Instant start = Instant.now();

        // Annotated MDCs (features + F1) from the rest datasets.
        Map<String, MDC> mdcsExploration = explorationController.getMdexplio().importMDCsByPhase(queryMDCsExploration);

        // Configuring the parameter top k.
        MDedupSelectionAlgorithm.SelectionStrategy selectionStrategy = MDedupSelectionAlgorithm.SelectionStrategy.TOPK;
        selectionStrategy.setK(Integer.valueOf(topk));

        // Find the optimal parameters for the regression models. This is done using an 80:20 (train:test) split.
        MDedupRegression regression = findOptimalParameters(mdcsExploration, selectionStrategy);

        // Get the optimal parameters.
        Map<String, Double> optimalParameters = regression.getOptimalParameters();

        // Get the MDs of the currently tested dataset.
        List<String> mdIDs = new ArrayList<>(explorationController.getMds().keySet());
        Map<String, MD> mds = explorationController.getMds();
        mds.keySet().retainAll(mdIDs);

        // If we have too many MDs we perform sampling.
        Double sampleMDs = mds.size() > 1000 ? 0.1 : 1.0;

        MDedupPredictionAlgorithm predictionAlgorithm = new MDedupPredictionAlgorithm(
                dataset, mds, selectionStrategy, maxLevel, regressionModel, explorationController.getRecords(),
                explorationController.getSims(),
                mdcsExplorationSampleHyperparameterTuning, // <-----
                explorationController, maxDuration,
                optimalParameters, sampleMDs);

        // Get the predictions. Among them some MDC will have the highest score of course.
        Map<MDC, OptimalMDCInfo> predictions = predictionAlgorithm.execute();

        Instant finish = Instant.now();
        long totalExecutionDuration = Duration.between(start, finish).toMillis();

        // Export the MDCs.
        mdedupio.exportTable(
                new MDedupPredictionIO.PrepareStatementParametersPrediction(
                    dataset,
                    predictions,
                    maxLevel,
                    topk,
                    predictionAlgorithm.getExecutionInterrupted(),
                    maxDuration.longValue(),
                    regression.getOptimalParametersAsString(),
                    predictionAlgorithm.getExecutionDuration(),
                    totalExecutionDuration,
                    enabledExpansionPhase,
                    featuresInRegression
                )
        );

        // Export the pairs associated with the highest scored MDC.
        exportClassifiedPairs(predictions, mds);

        mdedupio.close();
        clpio.close();
    }

    public MDedupRegression findOptimalParameters(Map<String, MDC> mdcsExploration, MDedupSelectionAlgorithm.SelectionStrategy selectionStrategy) {
        // Get "explored" MDCs: MDCs with features (and the associated F-1 of course)
        mdcsExplorationSampleHyperparameterTuning = mdcsExploration.entrySet().stream().
                filter(e -> (e.getValue().getFields().get("phase").equals("exploration")))
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));

        /**
         * Find optimal parameters. The consutrction of the selectionAlgorithm here is done merely to access the
         * regression model with the necessary data already loaded.
         * **/
        MDedupPredictionAlgorithm selectionAlgorithm = new MDedupPredictionAlgorithm(
                dataset, explorationController.getMds(), selectionStrategy, maxLevel, regressionModel,
                explorationController.getRecords(), explorationController.getSims(),
                mdcsExplorationSampleHyperparameterTuning, explorationController, maxDuration, new HashMap<>(),
                null);

        // Get the best parameters for the regression model.
        selectionAlgorithm.getOracle().getRegressionModel().selectOptimalParameters();

        // Get the regression model already trained using the best parameters.
        return selectionAlgorithm.getOracle().getRegressionModel();
    }


    private void exportClassifiedPairs(Map<MDC, OptimalMDCInfo> predictions, Map<String, MD> mds) {
        predictions.entrySet().forEach(e -> e.getKey().getFields().put("prediction_score", e.getValue().getScore().toString()));

        Map<String, MDC> mdcsPrediction = predictions.keySet().stream().collect(Collectors.toMap(MDC::getId, m -> m));

        // Get highest-scored MDC.
        Map.Entry<String, MDC> mdcWithMaxScore = mdcsPrediction.entrySet().stream().max(
                Comparator.comparing(mdc -> Double.valueOf(mdc.getValue().getFields().get("prediction_score")))).get();

        OracleOfDedup oracle = new OracleOfDedup(dpl, ndpl, mds, sims);

        // Retrieve the pairs satisfied by this highest scored MDC.
        List<SimilaritiesPair> pairsSatisfiedByBestMDC = oracle.getPairsSatisfiedOfMDC(mdcWithMaxScore.getKey(),
                mds, mdcsPrediction, sims);
        Set<Pair<String, String>> pairsSatisfiedByBestMDCSet = pairsSatisfiedByBestMDC.stream()
                .map(p -> new Pair<>(p.getId1(), p.getId2())).collect(Collectors.toSet());

        List<ClassificationDedupPairScored> pairsSatisfiedByBestMDCMapped = sims.values().stream()
                .map(p -> new ClassificationDedupPairScored(p.getDataset(), p.getId(), p.getId1(), p.getId2(),
                        p.getPairClass(),
                        pairsSatisfiedByBestMDCSet.contains(new Pair<>(p.getId1(), p.getId2())) ? DedupPair.PairClass.DPL : DedupPair.PairClass.NDPL,
                        1.0, 1.0
                )).collect(Collectors.toList());

        // And export them.
        clpio.exportTable(new MDCClassifiedPairsIO.PrepareStatementParametersClassifiedPairs(
                dataset,
                pairsSatisfiedByBestMDCMapped,
                "mdedup_prediction",
                mdcWithMaxScore.getValue().generateID(),
                "",
                topk,
                enabledExpansionPhase,
                featuresInRegression
        ));

        mdedupio.close();
        clpio.close();
    }


}
