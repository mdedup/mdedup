package de.hpi.is.mdedup.prediction.mlmodels;

import de.hpi.is.mdedup.exploration.metricsgenerators.MDMetricsGenerator;
import de.hpi.is.mdedup.exploration.metricsgenerators.PairMetricsGenerator;
import de.hpi.is.mdedup.exploration.metricsgenerators.RecordMetricsGenerator;
import de.hpi.is.mdedup.utils.entities.MDC;
import smile.feature.Scaler;

import java.util.*;
import java.util.stream.Collectors;

public abstract class MDedupRegression {

    String dataset;
    Map<String, MDC> mdcs;
    List<String> features;
    List<String> mdcIDs;

    protected Scaler scaler;

    protected double[][] X, Xrest;
    protected double[] Y, Yrest;

    protected List<Map<String, Double>> parametersPool;
    protected Map<String, Double> optimalParameters;

    public MDedupRegression(String dataset, Map<String, MDC> mdcs) {
        this.dataset = dataset;
        this.mdcs = mdcs;

        features = new ArrayList<>();
        features.addAll(RecordMetricsGenerator.enumerateRecordMetrics());
        features.addAll(PairMetricsGenerator.enumeratePairMetrics());
        features.addAll(MDMetricsGenerator.enumerateMDMetrics());
        features.sort(String::compareTo);

        System.out.println(features);

        prepareForInterDatasetTesting();

        optimalParameters = new HashMap<>();
    }

    public abstract Map<String, Double> selectOptimalParameters();

    public abstract Double execute(Map<String, String> mdcWithFeatures);

    public Map<String, Double> getOptimalParameters() {
        return optimalParameters;
    }

    public String getOptimalParametersAsString() {
        List<String> parametersList = optimalParameters.entrySet().stream().map(e -> e.getKey() + "@" + e.getValue())
                .collect(Collectors.toList());
        return String.join("#", parametersList);
    }

    protected void prepareForInterDatasetTesting() {
        mdcIDs = new ArrayList<>(mdcs.keySet());
        Collections.sort(mdcIDs, Comparator.comparing(Integer::valueOf));

        int cntDatasetMDCs = mdcs.values().parallelStream().mapToInt(x -> x.getFields().get("dataset")
                .equalsIgnoreCase(dataset) ? 1 : 0).sum();

        X = new double[cntDatasetMDCs][features.size()];
        Xrest = new double[mdcIDs.size() - cntDatasetMDCs][features.size()];
        Y = new double[cntDatasetMDCs];
        Yrest = new double[mdcIDs.size() - cntDatasetMDCs];

        int cnt = 0, cntRest = 0;
        for (int i = 0; i < mdcIDs.size(); ++i) {
            String mdcID = mdcIDs.get(i);
            String mdcDataset = mdcs.get(mdcID).getFields().get("dataset");

            double[][] curX = mdcDataset.equals(dataset) ? X : Xrest;
            double[] curY = mdcDataset.equals(dataset) ? Y : Yrest;
            int curCnt = mdcDataset.equals(dataset) ? cnt : cntRest;

            curX[curCnt] = convertMDCWithFeaturesToDoubleArray(mdcs.get(mdcID).getFields());
            curY[curCnt] = Double.valueOf(mdcs.get(mdcID).getFields().get("f05"));//.get("f1"));

            if (mdcDataset.equalsIgnoreCase(dataset)) {
                cnt++;
            } else {
                cntRest++;
            }
        }

        double[][] Xall = new double[mdcIDs.size()][features.size()];
        for (int i = 0; i < mdcIDs.size(); ++i) {
            for (int j = 0; j< features.size(); ++j) {
                double[][] curX = i < cntDatasetMDCs ? X : Xrest;
                int index = i < cntDatasetMDCs ? i : i - cntDatasetMDCs;
                Xall[i][j] = curX[index][j];
            }
        }
        scaler = new Scaler();
        scaler.learn(Xall);
        X = scaler.transform(X);
        Xrest = scaler.transform(Xrest);
    }

    protected double[] convertMDCWithFeaturesToDoubleArray(Map<String, String> mdcWithFeatures) {
        double[] xMDC = new double[features.size()];
        for (int j = 0; j < features.size(); ++j) {
            String feature = features.get(j);
            String v = mdcWithFeatures.get(feature);

            if (v == null || Double.isInfinite(Double.valueOf(v)) || Double.isNaN(Double.valueOf(v))) {
                xMDC[j] = 0.0;
            } else {
                xMDC[j] = Double.valueOf(v);
            }
        }
        return xMDC;
    }


    public abstract smile.regression.Regression<double[]> getModel();

    public List<String> getMdcIDs() {
        return mdcIDs;
    }

    public double[][] getX() {
        return X;
    }

    public double[][] getXrest() {
        return Xrest;
    }

    public double[] getY() {
        return Y;
    }

    public double[] getYrest() {
        return Yrest;
    }
}
