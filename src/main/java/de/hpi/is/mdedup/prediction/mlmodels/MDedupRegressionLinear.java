package de.hpi.is.mdedup.prediction.mlmodels;

import de.hpi.is.mdedup.utils.entities.MDC;
import smile.regression.OLS;
import smile.regression.Regression;

import java.util.Map;

public class MDedupRegressionLinear extends MDedupRegression {
    OLS regr;
    public MDedupRegressionLinear(String dataset, Map<String, MDC> mdcs) {
        super(dataset, mdcs);
        Xrest = scaler.transform(Xrest);
        regr = new OLS(Xrest, Yrest, true);
    }

    @Override
    public Map<String, Double> selectOptimalParameters() {
        return null;
    }

    @Override
    public Double execute(Map<String, String> mdcWithFeatures) {
        double[] x = convertMDCWithFeaturesToDoubleArray(mdcWithFeatures);
        x = scaler.transform(x);
        double y = regr.predict(x);
        return y;
    }

    @Override
    public Regression<double[]> getModel() {
        return regr;
    }
}
