package de.hpi.is.mdedup.prediction.mlmodels;

import de.hpi.is.mdedup.boosting.classification.utils.SampleDataset;
import de.hpi.is.mdedup.utils.entities.MDC;
import org.javatuples.Pair;
import smile.math.kernel.GaussianKernel;
import smile.regression.GaussianProcessRegression;
import smile.regression.Regression;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class MDedupRegressionGaussianProcess extends MDedupRegression {
    GaussianProcessRegression<double[]> gpr;

    Double bestSigma = null;
    Double bestLambda = null;

    public MDedupRegressionGaussianProcess(String dataset, Map<String, MDC> mdcs,
                                           Double bestSigma, Double bestLambda) {
        super(dataset, mdcs);

        // https://www.cs.cmu.edu/~aarti/Class/10701/readings/Luxburg06_TR.pdf - sigma = 1
        if (bestSigma != null && bestLambda != null) {
            gpr = new GaussianProcessRegression<>(Xrest, Yrest, new GaussianKernel(bestSigma), bestLambda);
        } else {
            gpr = new GaussianProcessRegression<>(Xrest, Yrest, new GaussianKernel(1.0), 0.01);
        }
    }

    protected void populateParametersPool() {
        parametersPool = new ArrayList<>();
        // USE STEP afterclassification (see filter in for loop of parameters)
        int sigmaBegin = -3, sigmaEnd = 7, lambdaBegin = -5, lambdaEnd = 5;

        List<Double> gaussianKernelSigmas = IntStream.range(sigmaBegin, sigmaEnd + 1)
                .filter(i -> i % 2 != 0) // Step two
                .mapToDouble(i -> Math.pow(2.0, i)).boxed() // Powers of two
                .collect(Collectors.toList());  // Kernel width
        List<Double> gaussianProcessLambdas = IntStream.range(lambdaBegin, lambdaEnd + 1)
                .filter(i -> i % 2 != 0) // Step two
                .mapToDouble(i -> Math.pow(2.0, i)).boxed() // Powers of two
                .collect(Collectors.toList()); // Regularization term

        for (Double sigma: gaussianKernelSigmas) {
            for (Double lambda: gaussianProcessLambdas) {
                Map<String, Double> setOfParameters = new HashMap<>();
                setOfParameters.put("sigma", sigma);
                setOfParameters.put("lambda", lambda);
                parametersPool.add(setOfParameters);
            }
        }
    }

    public Map<String, Double> selectOptimalParameters() {
        populateParametersPool();

        SampleDataset sd = new SampleDataset(Xrest, null, null);
        Map<String, int[]> setSplits = sd.randomUniformSampleTrainValidationTest(0.8, 0.0, 0.2);
        double[][] Xtrain = smile.math.Math.slice(Xrest, setSplits.get("train"));
        double[] Ytrain = smile.math.Math.slice(Yrest, setSplits.get("train"));
        double[][] Xtest = smile.math.Math.slice(Xrest, setSplits.get("test"));
        double[] Ytest = smile.math.Math.slice(Yrest, setSplits.get("test"));

        Map<String, Double> parametersToSumMSE = new ConcurrentHashMap<>();

        if (parametersPool.size() > 1) {
            List<Pair<Double, Double>> parameters = new ArrayList<>();
            parametersPool.forEach(params -> {
                parameters.add(new Pair<>(params.get("sigma"), params.get("lambda")));
            });

            System.out.println("Calculating optimal parameters");
            parameters.parallelStream().forEach(params -> {
                Double sigma = params.getValue0();
                Double lambda = params.getValue1();

                double sumMSE = getRMSEForParameters(sigma, lambda, Xtrain, Ytrain, Xtest, Ytest);
                parametersToSumMSE.put(sigma + "_" + lambda, sumMSE);
                System.out.println("finished " + sigma + "_" + lambda + " --> " + sumMSE);
            });
            System.out.println("Finished calculating optimal parameters");
        } else {
            Double sigma = parametersPool.get(0).get("sigma");
            Double lambda = parametersPool.get(0).get("lambda");
            parametersToSumMSE.put(sigma + "_" + lambda, (double)Xrest.length);
        }

        Double bestRMSE = Double.MAX_VALUE;
        String bestParameters = null;
        for (Map.Entry<String, Double> e: parametersToSumMSE.entrySet()) {
            Double sumRMSE = Math.sqrt(e.getValue()/Xrest.length);
            if (sumRMSE < bestRMSE) {
                bestParameters = e.getKey();
                bestRMSE = sumRMSE;
            }
        }
        String[] parametersAsToks = bestParameters.split("_");
        bestSigma = Double.valueOf(parametersAsToks[0]);
        bestLambda = Double.valueOf(parametersAsToks[1]);

        System.out.println("Best parameters: sigma -> " + bestSigma + " and lambda -> " + bestLambda);

        optimalParameters.put("sigma", bestSigma);
        optimalParameters.put("lambda", bestLambda);

        return optimalParameters;
    }

    private double getRMSEForParameters(double sigma, double lambda, double[][] Xtrain, double[] Ytrain, double[][] Xtest,
                                        double[] Ytest) {
        double sumMSE = 0.0;

        GaussianProcessRegression<double[]> gpr = new GaussianProcessRegression<>(Xtrain, Ytrain, new GaussianKernel(sigma), lambda);

        for (int i = 0 ; i < Xtest.length; ++i) {
            double[] x = Xtest[i];
            double y = Ytest[i];
            double pred = gpr.predict(x);
            sumMSE += Math.pow(y-pred, 2.0);
        }
        return sumMSE;
    }


    @Override
    public Double execute(Map<String, String> mdcWithFeatures) {
        double[] x = convertMDCWithFeaturesToDoubleArray(mdcWithFeatures);
        x = scaler.transform(x);
        double y = gpr.predict(x);
        return y;
    }

    @Override
    public Regression<double[]> getModel() {
        return gpr;
    }
}
