package de.hpi.is.mdedup.expansion;

import de.hpi.is.mdedup.MDedupController;
import de.hpi.is.mdedup.selection.MDedupSelectionIO;
import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.SimilaritiesPair;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * This class orchestrates the expansion of MDCs produced during the selection step.
 */
public class MDedupExpandController extends MDedupController {

    private final String dataset;
    private final String queryHymd;
    private final Map<String, Double> sampleSizeByStrategy;

    protected Integer topk;
    protected Boolean enabledExpansionPhase;
    protected String featuresInRegression;

    private MDedupSelectionIO mdedupioSelection;
    private MDedupExpansionIO mdedupioExpansion;

    public MDedupExpandController(String dbUser, String dbPassword, String dbDriver, String dbConnection,
                                  String dataset, String queryHymd, Map<String, Double> sampleSizeByStrategy,
                                  Integer topk, Boolean enabledExpansionPhase, String featuresInRegression) {
        super(dbUser, dbPassword, dbDriver, dbConnection);
        this.dataset = dataset;
        this.queryHymd = queryHymd;
        this.sampleSizeByStrategy = sampleSizeByStrategy;

        this.topk = topk;
        this.enabledExpansionPhase = enabledExpansionPhase;
        this.featuresInRegression = featuresInRegression;

        mdedupioSelection = new MDedupSelectionIO(dbDriver, dbConnection, dbUser, dbPassword);
        mdedupioExpansion = new MDedupExpansionIO(dbDriver, dbConnection, dbUser, dbPassword);
    }

    public void execute() {
        // Get the available MDs for the dataset we want to expand its MDCs.
        Map<String, MD> mds = mdedupioSelection.importMDs(queryHymd);
        if (mds.size() == 1) {
            System.out.println("Only 1 MD was provided, there is nothing to expand");
            System.exit(-1);
        }

        Instant start = Instant.now();
        List<Integer> mdIDs = mds.keySet().stream().mapToInt(Integer::valueOf).boxed().collect(Collectors.toList());

        // Retrieve the MDCs selected in the previous step.
        List<List<Integer>> mdcsSelected = mdedupioSelection.importMDCsSelection(dataset);

        // Upper limit for our expansions.
        Integer maxLevel = mdcsSelected.parallelStream().mapToInt(List::size).max().getAsInt();

        System.out.println("Read data complete");
        importSimilarities("select * from sims_" + dataset);
        MDedupExpandAlgorithm expandAlgorithm = new MDedupExpandAlgorithm(dataset, dpl, ndpl, mds, sims, maxLevel);

        // Retrieve expanded MDCs
        List<MDedupExpandAlgorithm.MDCExpandResult> expansions = expandAlgorithm.expand(sampleSizeByStrategy, mdIDs, mdcsSelected);

        Instant finish = Instant.now();
        long totalExecutionDuration = Duration.between(start, finish).toMillis();

        // Export
        System.out.println("Exporting results");
        mdedupioExpansion.resetTable(dataset, "gaussian_neighborhood");
        mdedupioExpansion.resetTable(dataset, "uniform_random");

        // Export MDCs.
        mdedupioExpansion.exportTable(new MDedupExpansionIO.PrepareStatementParametersExpansion(
                dataset, expansions, totalExecutionDuration, topk, enabledExpansionPhase, featuresInRegression));

        mdedupioExpansion.close();
        mdedupioSelection.close();
    }

}
