package de.hpi.is.mdedup.expansion;

import de.hpi.is.mdedup.selection.OracleOfDedup;
import de.hpi.is.mdedup.utils.ClassificationEvaluation;
import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.SimilaritiesPair;
import de.hpi.is.mdedup.utils.entities.MDC;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

public class MDedupExpandAlgorithm {

    protected String dataset;
    protected OracleOfDedup oracle;

    protected Integer maxLevel;
    protected Integer maxLevelGiven;

    public MDedupExpandAlgorithm(String dataset, ArrayList<Integer> dpl, ArrayList<Integer> ndpl,
                                 Map<String, MD> mds, Map<Integer, SimilaritiesPair> sims, Integer maxLevel) {
        this.dataset = dataset;
        this.oracle = new OracleOfDedup(dpl, ndpl, mds, sims);
        this.maxLevel = maxLevel;
        this.maxLevelGiven = maxLevel;
        if (maxLevel > mds.size() || maxLevel == -1) {
            this.maxLevel = mds.size();
        }
    }

    public static class MDCExpandResult {
        List<String> mds;
        ClassificationEvaluation classificationEvaluation;
        String expansionStrategy;
        String maxLevel;

        public MDCExpandResult(List<String> mds, ClassificationEvaluation classificationEvaluation, String expansionStrategy, String maxLevel) {
            this.mds = mds;
            this.classificationEvaluation = classificationEvaluation;
            this.expansionStrategy = expansionStrategy;
            this.maxLevel = maxLevel;
        }

        public List<String> getMds() {
            return mds;
        }

        public ClassificationEvaluation getClassificationEvaluation() {
            return classificationEvaluation;
        }

        public String getExpansionStrategy() {
            return expansionStrategy;
        }

        public String getMaxLevel() {
            return maxLevel;
        }
    }

    public List<MDCExpandResult> expand(
            Map<String, Double> sampleSizeByStrategy, List<Integer> mdIDsList, List<List<Integer>> mdCmbList) {

        // Keep MDs in a set for fast existence lookup
        HashSet<String> mdCmbsSet = new HashSet<>();
        for (List<Integer> mdCmb: mdCmbList) {
            mdCmb.sort(Integer::compareTo);
            List<String> mdCmbStrList = new ArrayList<>();
            mdCmb.forEach(x -> mdCmbStrList.add(x.toString()));
            mdCmbsSet.add(StringUtils.join(mdCmbStrList, "_"));
        }

        Integer possibleMDCombinations = (int) Math.pow(2.0, mdIDsList.size());
        possibleMDCombinations--; // We don't want the empty set.
        possibleMDCombinations -= mdCmbsSet.size(); // Remove the combinations already included.

        HashSet<String> mdCmbsSetGaussianNeighborhood = new HashSet<>();
        HashSet<String> mdCmbsSetRandomUniform = new HashSet<>();
//        if (mdIDsList.size() > 1 && maxLevel > 1) {
        if (mdIDsList.size() > 1) {
            if (sampleSizeByStrategy.containsKey("gaussian_neighborhood")) {
                Integer mdCmbsNumToReach = (int) (mdCmbsSet.size() * sampleSizeByStrategy.get("gaussian_neighborhood"));
                // If our expectations are too many.
                if (mdCmbsNumToReach > possibleMDCombinations) {
                    mdCmbsNumToReach = possibleMDCombinations;
                }
                mdCmbsSetGaussianNeighborhood = expandGaussianNeighborhood(mdIDsList, mdCmbsSet, mdCmbsNumToReach);
            }
            if (sampleSizeByStrategy.containsKey("uniform_random")) {
                Integer mdCmbsNumToReach = (int) (mdCmbsSet.size() * sampleSizeByStrategy.get("uniform_random"));
                // If our expectations are too many.
                if (mdCmbsNumToReach > possibleMDCombinations) {
                    mdCmbsNumToReach = possibleMDCombinations;
                }
                HashSet<String> mdCmbsSetWithGaussianNeighborhood = new HashSet<>();
                mdCmbsSetWithGaussianNeighborhood.addAll(mdCmbsSet);
                mdCmbsSetWithGaussianNeighborhood.addAll(mdCmbsSetGaussianNeighborhood);
                mdCmbsSetRandomUniform = expandRandomUniform(mdIDsList, mdCmbsNumToReach, mdCmbsSetWithGaussianNeighborhood);
            }
        }

        // Classify them - Gaussian Neighborhood
        oracle.batchCalculateEvaluationsForMDCs(mdCmbsSetGaussianNeighborhood.stream().map(x -> {
            Set<String> mdIDs = new HashSet<>(Arrays.asList(x.split("_")));
            return new MDC(mdIDs);
        }).collect(Collectors.toList()));

        List<MDCExpandResult> resultsGaussianNeighborhood = mdCmbsSetGaussianNeighborhood.stream().map(mdCmb -> {
            List<String> mdIDs = Arrays.stream(mdCmb.split("_")).map(String::valueOf).collect(Collectors.toList());
            ClassificationEvaluation classificationEvaluation = oracle.fetchEvaluationMDC(new MDC(new HashSet<>(mdIDs)));
            return new MDCExpandResult(mdIDs, classificationEvaluation, "gaussian_neighborhood", maxLevelGiven.toString());
        }).filter(mdc -> mdc.getClassificationEvaluation().calculateFMeasure(1.0) > 0.0).collect(Collectors.toList());
        System.out.println("Gaussian Neighborhood expansion - from: " + mdCmbsSet.size() +  " to: " + resultsGaussianNeighborhood.size() + " results.");

        // Classify them - Random Uniform
        oracle.batchCalculateEvaluationsForMDCs(mdCmbsSetRandomUniform.stream().map(x -> {
            Set<String> mdIDs = new HashSet<>(Arrays.asList(x.split("_")));
            return new MDC(mdIDs);
        }).collect(Collectors.toList()));

        List<MDCExpandResult> resultsRandomUniform = mdCmbsSetRandomUniform.stream().map(mdCmb -> {
            List<String> mdIDs = Arrays.stream(mdCmb.split("_")).map(String::valueOf).collect(Collectors.toList());
            ClassificationEvaluation classificationEvaluation = oracle.fetchEvaluationMDC(new MDC(new HashSet<>(mdIDs)));
            return new MDCExpandResult(mdIDs, classificationEvaluation, "uniform_random", maxLevelGiven.toString());
        }).filter(mdc -> mdc.getClassificationEvaluation().calculateFMeasure(1.0) > 0.0).collect(Collectors.toList());
        System.out.println("Random uniform expansion - from: " + mdCmbsSet.size() +  " to: " + resultsRandomUniform.size() + " results.");

        List<MDCExpandResult> results = new ArrayList<>();
        results.addAll(resultsGaussianNeighborhood);
        results.addAll(resultsRandomUniform);

        return results;
    }

    private HashSet<String> expandRandomUniform(List<Integer> mdIDsList, Integer mdCmbsNumToReach, HashSet<String> mdCmbsSet) {
        System.out.println("Expanding randomly");
        HashSet<String> mdCmbsSetRandom = new HashSet<>();
        mdCmbsSetRandom.addAll(mdCmbsSet);
        Integer maxLevelFound = mdCmbsSet.stream().mapToInt(s -> s.split("_").length).max().getAsInt();
        Random r = new Random(0L);
        int i = 0;
        while ((mdCmbsSetRandom.size() - mdCmbsSet.size()) < mdCmbsNumToReach) {
            if (i == 10000000) { // If 10m iterations were not enough to find new MDCMBs just stop searching more.
                break;
            }
            if (++i % 100 == 0) {
                System.out.println("i: " + i + " mdcmbs: " + (mdCmbsSetRandom.size()-mdCmbsSet.size()) + " out of " + mdCmbsNumToReach);
            }
            List<Integer> mds = new ArrayList<>(mdIDsList);
            Collections.shuffle(mds, r);

            int mdcSize = 1 + (maxLevelFound > 1 ? r.nextInt(maxLevelFound-1) : 0);

            if (mdcSize == 0 || mdcSize > maxLevelFound) {
                continue;
            }
            List<Integer> mdcInt = mds.subList(0, mdcSize);
            mdcInt.sort(Integer::compareTo);
            String mdc = StringUtils.join(mdcInt, "_");
            mdCmbsSetRandom.add(mdc);
        }
        mdCmbsSetRandom.removeAll(mdCmbsSet);
        return mdCmbsSetRandom;
    }


    private HashSet<String> expandGaussianNeighborhood(List<Integer> mdIDsList, HashSet<String> mdCmbsSet, Integer mdCmbsNumToReach) {
        System.out.println("Expanding on neighborhood");
        HashSet<String> mdcmbsExpanded = new HashSet<>();
        mdcmbsExpanded.addAll(mdCmbsSet);
        Set<String> mdIDsSet = mdIDsList.stream().map(String::valueOf).collect(Collectors.toSet());
        List<String> mdcmbsList = new ArrayList<>(mdCmbsSet);
        Random r = new Random(0L);

        int i = 0;
        while ((mdcmbsExpanded.size() - mdCmbsSet.size()) < mdCmbsNumToReach) {
            if (i == 10000000) { // If 10m iterations were not enough to find new MDCMBs just stop searching more.
                break;
            }
            if (++i % 100 == 0) {
                System.out.println("i: " + i + " mds: " + (mdcmbsExpanded.size() - mdCmbsSet.size()) + " out of " + mdCmbsNumToReach);
            }

            // Selected MDC
            String mdcmb = mdcmbsList.get(r.nextInt(mdcmbsList.size()));
            List<String> mds = new ArrayList<>(Arrays.asList(mdcmb.split("_")));

            // Since actually touching the actual params inside Math.max are almost impossible, we added this extra "+1"
            Double targetMdcmbSizeGenerated = r.nextGaussian() * (Math.max(maxLevel - mds.size(), -(mds.size() - 1)) + 1);
            Integer targetMdcmbSize = null;
            if (Math.abs(targetMdcmbSizeGenerated) < 0.1) {
                targetMdcmbSize = 0;
            } else if (Math.abs(targetMdcmbSizeGenerated) < 1.0){
                targetMdcmbSize = (int) Math.ceil(Math.abs(targetMdcmbSizeGenerated)) * (targetMdcmbSizeGenerated < 0? -1: 1);
            } else {
                targetMdcmbSize = (int) Math.round(targetMdcmbSizeGenerated);
            }
            if (targetMdcmbSize > (maxLevel - mds.size())) {
                targetMdcmbSize = maxLevel - mds.size();
            } else if (targetMdcmbSize <= -mds.size()) {
                targetMdcmbSize = -(mds.size() - 1);
            }

            if (targetMdcmbSize > 0) { // Adding
                String mdcmbExpanded = addMDs(mdIDsSet, r, mds, targetMdcmbSize);
                mdcmbsExpanded.add(mdcmbExpanded);
            } else if (targetMdcmbSize < 0) { // Removing
                String mdcmbExpanded = removeMDs(r, mds, targetMdcmbSize);
                mdcmbsExpanded.add(mdcmbExpanded);
            } else {
                double range = mds.size() - 1.0;
                Integer changeMDsSize = (int) ((r.nextGaussian() * (range/2)) + (range/2));
                changeMDsSize = Math.min(changeMDsSize, (int) range);
                changeMDsSize = Math.max(changeMDsSize, 1);

                // Remove a random number
                String mdcmbExpandedRemoved = removeMDs(r, new ArrayList<>(mds), -changeMDsSize);
                List<String> mdsExpandedRemoved = new ArrayList<>(Arrays.asList(mdcmbExpandedRemoved.split("_")));
                // ... and then add
                String mdcmbExpandedAdded = addMDs(mdIDsSet, r, new ArrayList<>(mdsExpandedRemoved), changeMDsSize);
                mdcmbsExpanded.add(mdcmbExpandedAdded);
                continue;
            }
        }
        mdcmbsExpanded.removeAll(mdCmbsSet);
        return mdcmbsExpanded;
    }

    private String removeMDs(Random r, List<String> mds, Integer targetMdcmbSize) {
        // Remaining MDs
        Collections.shuffle(mds, r);
        mds = mds.subList(0, mds.size() - Math.abs(targetMdcmbSize));

        // This can happen if mds had elements and now has nothing. The sort would fail if it tried to sort
        // the empty string.
        mds.remove("");

        mds.sort(Comparator.comparing(Integer::valueOf));
        return String.join("_", mds);
    }

    private String addMDs(Set<String> mdIDsSet, Random r, List<String> mds, Integer targetMdcmbSize) {
        // Remaining MDs
        HashSet<String> remainingMDsSet = new HashSet<>(mdIDsSet);
        remainingMDsSet.removeAll(mds);
        List<String> remainingMDsList = new ArrayList<>(remainingMDsSet);
        Collections.shuffle(remainingMDsList, r);
        mds.addAll(remainingMDsList.subList(0, targetMdcmbSize));

        // This can happen if mds did not have elements and now have. The sort would fail if it tried to sort
        // the empty string.
        mds.remove("");

        mds.sort(Comparator.comparing(Integer::valueOf));
        return String.join("_", mds);
    }
}
