package de.hpi.is.mdedup.expansion;

import de.hpi.is.mdedup.selection.MDedupSelectionIO;
import de.hpi.is.mdedup.utils.ClassificationEvaluation;
import de.hpi.is.mdedup.utils.entities.MDC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MDedupExpansionIO extends MDedupSelectionIO {
    public MDedupExpansionIO(String dbDriver, String dbConnection, String dbUser, String dbPassword) {
        super(dbDriver, dbConnection, dbUser, dbPassword);
    }

    public MDedupExpansionIO(Connection conn) {
        super(conn);
    }

    public void resetTable(String dataset, String phase) {
        String sql = "DELETE FROM mdedup_mdcs WHERE phase='"+phase+"'";
        if (dataset != null) {
            sql += " and dataset='" + dataset + "';";
        }
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.execute();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<List<Integer>> importMDCsExpansion(String dataset) {
        List<List<Integer>> mdcsNeighborhood = importMDCsByPhase(dataset, "gaussian_neighborhood");
        List<List<Integer>> mdcsRandom = importMDCsByPhase(dataset, "uniform_random");
        List<List<Integer>> mdcs = new ArrayList<>();
        mdcs.addAll(mdcsNeighborhood);
        mdcs.addAll(mdcsRandom);
        return mdcs;
    }

    public static class PrepareStatementParametersExpansion extends PrepareStatementParameters {
        protected List<MDedupExpandAlgorithm.MDCExpandResult> expansions;

        protected Long totalExecutionDuration;

        protected List<Map<String, Object>> data;

        protected Integer topk;
        protected Boolean enabledExpansionPhase;
        protected String featuresInRegression;

        public PrepareStatementParametersExpansion(String dataset, List<MDedupExpandAlgorithm.MDCExpandResult> expansions,
                                                   Long totalExecutionDuration,
                                                   Integer topk,
                                                   Boolean enabledExpansionPhase,
                                                   String featuresInRegression) {
            super(dataset, "mdedup_mdcs");
            this.expansions = expansions;
            this.totalExecutionDuration = totalExecutionDuration;
            this.topk = topk;
            this.enabledExpansionPhase = enabledExpansionPhase;
            this.featuresInRegression = featuresInRegression;
        }

        public List<Map<String, Object>> prepareData() {
            Set<String> header = new HashSet<>();
            data = expansions.stream().map(e -> {
                Map<String, Object> r = new HashMap<>();

                MDC mdc = new MDC(e.mds);
                r.put("dataset", dataset);
                r.put("ids", mdc.getId());
                r.put("phase", "expansion");
                r.put("expansion_phase", e.getExpansionStrategy());

                ClassificationEvaluation ce = e.getClassificationEvaluation();

                r.put("precision_", ce.calculatePrecision());
                r.put("recall", ce.calculateRecall());
                r.put("f1", ce.calculateFMeasure(1.0));

                r.put("f05", ce.calculateFMeasure(0.5));
                r.put("f20", ce.calculateFMeasure(2.0));

                r.put("tp", ce.getTp());
                r.put("fn", ce.getFn());
                r.put("fp", ce.getFp());
                r.put("tn", ce.getTn());

                r.put("total_execution_duration", totalExecutionDuration);

                r.put("topk", topk);
                r.put("enabled_expansion_phase", enabledExpansionPhase);
                r.put("features_in_regression", featuresInRegression);

                if (header.size() == 0) {
                    header.addAll(r.keySet());
                }

                return r;
            }).collect(Collectors.toList());

            this.header = new ArrayList<>(header);
            this.header.sort(String::compareTo);

            return data;
        }
    }




}
