package de.hpi.is.mdedup.exploration.metricsgenerators;

import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.SimilaritiesPair;
import de.hpi.is.mdedup.utils.entities.MDC;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class MetricsGenerator {
    Map<String, MD> mds;
    Map<String, Map<String, String>> data;
    Map<Integer, SimilaritiesPair> sims;
    Map<String, MDC> mdcs;

    public MetricsGenerator() {
    }

    public MetricsGenerator(Map<String, MD> mds, Map<String, Map<String, String>> data,
                            Map<Integer, SimilaritiesPair> sims, Map<String, MDC> mdcs) {
        this.mds = mds;
        this.data = data;
        this.sims = sims;
        this.mdcs = mdcs;
    }

    public abstract Map<String, Map<String, Double>> getMetrics();
    public abstract Set<String> enumerateMetrics();
}
