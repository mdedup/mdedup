package de.hpi.is.mdedup.exploration.metricsgenerators;

import de.hpi.is.mdedup.utils.ClassificationEvaluation;
import de.hpi.is.mdedup.utils.DedupPair;
import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.entities.MDC;
import de.hpi.is.mdedup.utils.entities.MDCondition;
import de.hpi.is.mdedup.utils.SimilaritiesPair;
import org.javatuples.Pair;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

public class PairMetricsGenerator extends MetricsGenerator {

    protected Map<String, Double> supportNormalizedLHS;
    protected Map<String, Double> supportNormalizedRHS;
    protected Map<String, Double> supportNormalizedBOTH;
    protected Map<String, Double> confidenceNormalized;

    protected Map<String, ClassificationEvaluation> approximationClassificationResultForMDCs;

    public PairMetricsGenerator(Map<String, MD> mds, Map<String, Map<String, String>> data,
                                Map<Integer, SimilaritiesPair> sims, Map<String, MDC> mdcs) {
        super(mds, data, sims, mdcs);
    }

    public PairMetricsGenerator() {
    }

    public Map<String, Map<String, Double>> getMetrics() {
        Map<String, Map<String, Double>> metrics = new HashMap<>();

        metrics.put("support", getSupportNormalized());
        metrics.put("confidence", getConfidenceNormalized());
        metrics.put("lift", getLiftNormalized());
        metrics.put("conviction", getConvictionNormalized());

        return metrics;
    }

    public static Set<String> enumeratePairMetrics() {
        return new PairMetricsGenerator().enumerateMetrics();
    }

    @Override
    public Set<String> enumerateMetrics() {
        HashSet<String> metricsNames = new HashSet<>();

        metricsNames.add("support");
        metricsNames.add("confidence");
        metricsNames.add("lift");
        metricsNames.add("conviction");

        return metricsNames;
    }

    /**
     *
     * Support metric
     */
    public Map<String, Double> getSupportNormalized() {
        this.supportNormalizedLHS = getSupportNormalized(MD.MDSide.LHS);
        return this.supportNormalizedLHS;
    }

    protected Map<String, Double> getSupportNormalized(MD.MDSide mdside) {
        Map<String, Integer> support = getSupport(mdside);

        HashMap<String, Double> supportNormalized = new HashMap<>();
        support.forEach((mdcID, supportValue) -> supportNormalized.put(mdcID, (double) supportValue / sims.size()));

        return supportNormalized;
    }

    protected Map<String, Integer> getSupport(MD.MDSide mdside) {
        ConcurrentHashMap<String, Integer> support = new ConcurrentHashMap<>();
        mdcs.keySet().forEach(mdcID -> support.put(mdcID, 0));

        /* Step 1. Get Column Matches (MD conditions) */
        Set<MDCondition> columnMatches = new ConcurrentSkipListSet<>();
        mds.entrySet().parallelStream().forEach(md -> {
            columnMatches.addAll(md.getValue().getMDSide(mdside));
        });

        sims.entrySet().parallelStream().forEach(pairSimilarities -> {
            Map<String, Boolean> mdcsValidities = getMDCsValiditiesForPair(this.mdcs, this.mds, mdside, columnMatches, pairSimilarities.getValue());

            mdcsValidities.forEach((mdcID, valid) -> {
                if (valid) {
                    support.put(mdcID, support.get(mdcID) + 1);
                }
            });

        });

        return support;
    }


    /**
     *
     * Confidence metric
     */
    public Map<String, Double> getConfidenceNormalized() {
        if (this.supportNormalizedLHS == null) {
            this.supportNormalizedLHS = getSupportNormalized(MD.MDSide.LHS);
        }
        Map<String, Double> supportNormalizedLHS = this.supportNormalizedLHS;
        this.supportNormalizedBOTH = getSupportNormalized(MD.MDSide.BOTH);
        Map<String, Double> supportNormalizedLHSRHS = supportNormalizedBOTH;


        HashMap<String, Double> confidenceNormalized = new HashMap<>();
        mdcs.forEach((mdcID, mdIDs) -> {
            confidenceNormalized.put(mdcID, supportNormalizedLHSRHS.getOrDefault(mdcID, 0.0) /
                    supportNormalizedLHS.getOrDefault(mdcID, 1.0));
        });
        this.confidenceNormalized = confidenceNormalized;
        return confidenceNormalized;
    }

    /**
     *
     * Lift metric
     */
    public Map<String, Double> getLiftNormalized() {
        if (this.supportNormalizedLHS == null) {
            this.supportNormalizedLHS = getSupportNormalized(MD.MDSide.LHS);
        }
        if (this.supportNormalizedRHS == null) {
            this.supportNormalizedRHS = getSupportNormalized(MD.MDSide.RHS);
        }
        if (this.supportNormalizedBOTH == null) {
            this.supportNormalizedBOTH = getSupportNormalized(MD.MDSide.BOTH);
        }
        Map<String, Double> supportNormalizedLHS = this.supportNormalizedLHS;
        Map<String, Double> supportNormalizedRHS = this.supportNormalizedRHS;
        Map<String, Double> supportNormalizedLHSRHS = supportNormalizedBOTH;


        HashMap<String, Double> liftNormalized = new HashMap<>();
        mdcs.forEach((mdcID, mdIDs) -> {

            liftNormalized.put(mdcID, supportNormalizedLHSRHS.getOrDefault(mdcID, 0.0) /
                    (supportNormalizedLHS.getOrDefault(mdcID, 1.0) * supportNormalizedRHS.getOrDefault(mdcID, 1.0)));
        });
        return liftNormalized;
    }


    /**
     *
     * Conviction metric
     */
    public Map<String, Double> getConvictionNormalized() {
        if (this.confidenceNormalized == null) {
            this.confidenceNormalized = getConfidenceNormalized();
        }
        if (this.supportNormalizedRHS == null) {
            this.supportNormalizedRHS = getSupportNormalized(MD.MDSide.RHS);
        }
        Map<String, Double> supportNormalizedRHS = this.supportNormalizedRHS;

        HashMap<String, Double> convictionNormalized = new HashMap<>();
        mdcs.forEach((mdcID, mdIDs) -> {
            double suppY = supportNormalizedRHS.get(mdcID);
            double confXY = confidenceNormalized.get(mdcID);

            Double conviction = (1.0 - suppY) / (1.0 - confXY);
            if (Double.isInfinite(conviction) || Double.isNaN(conviction)) {
                conviction = Double.MAX_VALUE;
            }
            convictionNormalized.put(mdcID, conviction);
        });
        return convictionNormalized;
    }

    public Map<String, Double> getPrecisionApproximation() {
        if (approximationClassificationResultForMDCs == null) {
            approximationClassificationResultForMDCs = approximateMetrics();
        }
        Map<String, Double> mdcToPrecision = approximationClassificationResultForMDCs.entrySet().parallelStream().collect(Collectors.toMap(
                e -> e.getKey(),
                e -> e.getValue().calculatePrecision()
        ));
        return mdcToPrecision;
    };

    public Map<String, Double> getRecallApproximation() {
        if (approximationClassificationResultForMDCs == null) {
            approximationClassificationResultForMDCs = approximateMetrics();
        }
        Map<String, Double> mdcToRecall = approximationClassificationResultForMDCs.entrySet().parallelStream().collect(Collectors.toMap(
                e -> e.getKey(),
                e -> e.getValue().calculateRecall()
        ));
        return mdcToRecall;
    }

    public Map<String, Double> getFMeasureApproximation() {
        if (approximationClassificationResultForMDCs == null) {
            approximationClassificationResultForMDCs = approximateMetrics();
        }
        Map<String, Double> mdcToFMeasure = approximationClassificationResultForMDCs.entrySet().parallelStream().collect(Collectors.toMap(
                e -> e.getKey(),
                e -> e.getValue().calculateFMeasure(1.0)
        ));
        return mdcToFMeasure;
    }

    protected Map<String, ClassificationEvaluation> approximateMetrics () {
        List<SimilaritiesPair> approximateGS = approximateGS(this.sims);

        /* Step 1. Get Column Matches (MD conditions) */
        Set<MDCondition> columnMatches = new ConcurrentSkipListSet<>();
        mds.entrySet().parallelStream().forEach(md -> {
            columnMatches.addAll(md.getValue().getMDSide(MD.MDSide.LHS));
        });

        Queue<Pair<String, String>> approximateMetricsToBeSummed = new ConcurrentLinkedQueue<>();

        approximateGS.parallelStream().forEach((pairSimilarities -> {
            Map<String, Boolean> mdcsValidities = getMDCsValiditiesForPair(this.mdcs, this.mds, MD.MDSide.LHS, columnMatches, pairSimilarities);
//            int tp = 0, fp = 0, fn = 0, tn = 0;
            for (Map.Entry<String, Boolean> entry : mdcsValidities.entrySet()) {
                String mdcID = entry.getKey();
                Boolean valid = entry.getValue();
                if (valid && pairSimilarities.getPairClass() == DedupPair.PairClass.DPL) {
//                    ++tp;
                    approximateMetricsToBeSummed.add(new Pair<>(mdcID, "TP"));
                } else if (valid && pairSimilarities.getPairClass() == DedupPair.PairClass.NDPL) {
//                    ++fp;
                    approximateMetricsToBeSummed.add(new Pair<>(mdcID, "FP"));
                } else if (!valid && pairSimilarities.getPairClass() == DedupPair.PairClass.DPL) {
//                    ++fn;
                    approximateMetricsToBeSummed.add(new Pair<>(mdcID, "FN"));
                } else if (!valid && pairSimilarities.getPairClass() == DedupPair.PairClass.NDPL) {
//                    ++tn;
                    approximateMetricsToBeSummed.add(new Pair<>(mdcID, "TN"));
                }
            }
        }));

        Map<String, Integer> mdcsTP = new HashMap<>();
        Map<String, Integer> mdcsFP = new HashMap<>();
        Map<String, Integer> mdcsFN = new HashMap<>();
        Map<String, Integer> mdcsTN = new HashMap<>();

        for (Pair<String, String> p : approximateMetricsToBeSummed) {
            String mdcID = p.getValue0();
            switch (p.getValue1()) {
                case "TP":
                    mdcsTP.put(mdcID, mdcsTP.getOrDefault(mdcID, 0)+1);
                    break;
                case "FP":
                    mdcsFP.put(mdcID, mdcsFP.getOrDefault(mdcID, 0)+1);
                    break;
                case "FN":
                    mdcsFN.put(mdcID, mdcsFN.getOrDefault(mdcID, 0)+1);
                    break;
                case "TN":
                    mdcsTN.put(mdcID, mdcsTN.getOrDefault(mdcID, 0)+1);
                    break;
            }
        }

        Map<String, ClassificationEvaluation> approximationClassificationResultForMDCs = new HashMap<>();
        for (MDC mdc : mdcs.values()) {
            int tp = mdcsTP.getOrDefault(mdc.getId(), 0);
            int fp = mdcsFP.getOrDefault(mdc.getId(), 0);
            int fn = mdcsFN.getOrDefault(mdc.getId(), 0);
            int tn = mdcsTN.getOrDefault(mdc.getId(), 0);

            ClassificationEvaluation ce = new ClassificationEvaluation(tp, fp, tn, fn);
            approximationClassificationResultForMDCs.put(mdc.getId(), ce);
        }

        return approximationClassificationResultForMDCs;
    }

    // Assume top5% as DPLs and bottom5% as NDPLs
    public static List<SimilaritiesPair> approximateGS(Map<Integer, SimilaritiesPair> pairs) {
        List<SimilaritiesPair> sims = new ArrayList<>();
        List<SimilaritiesPair> finalSims = sims;
        pairs.values().forEach(sp -> finalSims.add(sp.clone()));

//        Random r = new Random(0L);
//        Collections.shuffle(sims, r);
//        sims = sims.subList(0, sims.size()/10);

        sims.sort(Comparator.comparing(o -> -1 * o.getSims().get("total_mean")));

        List<SimilaritiesPair> simsDPL = sims.subList(0, (int) (sims.size()*(5.0/100.0)));
        if (simsDPL.size() == 0) {
            simsDPL = sims.subList(0, 1);
        }
        simsDPL.forEach(sp -> sp.setPairClass(DedupPair.PairClass.DPL));
//        List<SimilaritiesPair> simsNDPL = sims.subList((int) (sims.size()*(95.0/100.0)), sims.size());
        List<SimilaritiesPair> simsNDPL = sims.subList((int) (sims.size()*(92.0/100.0)), sims.size());
        if (simsNDPL.size() == 0) {
            simsNDPL = sims.subList(sims.size()-1, sims.size());
        }
        simsNDPL.forEach(sp -> sp.setPairClass(DedupPair.PairClass.NDPL));
        List<SimilaritiesPair> simsDPLandNDPL = new ArrayList<>();
        simsDPLandNDPL.addAll(simsDPL);
        simsDPLandNDPL.addAll(simsNDPL);

        return simsDPLandNDPL;
    }

    private Map<String, Boolean> getMDCsValiditiesForPair(Map<String, MDC> mdcs, Map<String, MD> mds, MD.MDSide mdside, Set<MDCondition> columnMatches, SimilaritiesPair pairSimilarities) {
        /* Step 2. Calculate validity of column matches */
        Map<String, Boolean> mdcndValidity = new HashMap<>();
        columnMatches.forEach(mdcnd -> {
            Boolean valid = Boolean.TRUE;
            Map<String, Double> pairSims = pairSimilarities.getSims();
//            String attrMsr = mdcnd.getAttribute() + "_" + mdcnd.getMeasure();
            String attrMsr = mdcnd.getAttribute() + "-" + mdcnd.getMeasure();
            if (pairSims.containsKey(attrMsr)) {
                if (pairSims.get(attrMsr) < mdcnd.getThreshold()) {
                    valid = Boolean.FALSE;
                }
            }
            mdcndValidity.put(mdcnd.toString(), valid);
        });

        /* Step 3. Calculate validity of MDs */
        Map<String, Boolean> mdValidity = new HashMap<>();
        mds.values().forEach(md -> {
            Boolean valid = Boolean.TRUE;
            for (MDCondition mdcnd : md.getMDSide(mdside)) {
                if (!mdcndValidity.get(mdcnd.toString())) {
                    valid = Boolean.FALSE;
                    break;
                }
            }
            mdValidity.put(md.getEncodingBySide(mdside), valid);
        });

        Map<String, Boolean> mdcValidity = new HashMap<>();
        mdcs.forEach((mdcID, mdc) -> {
            Boolean valid = Boolean.FALSE;
            for (String mdID : mdc.getMDsIDs()) {
                if (mdValidity.get(mds.get(mdID).getEncodingBySide(mdside))) {
                    valid = Boolean.TRUE;
                    break;
                }
            }
            mdcValidity.put(mdcID, valid);
        });
        return mdcValidity;
    }


}
