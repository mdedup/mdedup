package de.hpi.is.mdedup.exploration;

import de.hpi.is.mdedup.MDedupController;
import de.hpi.is.mdedup.utils.classifiedpairs.MDCClassifiedPairsIO;
import de.hpi.is.mdedup.boosting.classification.utils.ClassificationDedupPairScored;
import de.hpi.is.mdedup.exploration.metricsgenerators.MDMetricsGenerator;
import de.hpi.is.mdedup.exploration.metricsgenerators.PairMetricsGenerator;
import de.hpi.is.mdedup.exploration.metricsgenerators.RecordMetricsGenerator;
import de.hpi.is.mdedup.selection.OracleOfDedup;
import de.hpi.is.mdedup.utils.DedupPair;
import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.SimilaritiesPair;
import de.hpi.is.mdedup.utils.entities.MDC;
import org.javatuples.Pair;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Exploration is used to generate features for MDCs of the training pipeline. This is done for MDCs produced by the
 * selection and expansion phases.
 *
 * The application pipeline reuses parts of this package to generate the features for the MDCs it explores on-the-fly,
 * storing them afterwards.
 */
public class MDedupExplorationController extends MDedupController {

    private final String dataset;
    protected Map<String, MD> mds;
    protected Map<String, Map<String, String>> records;
    protected Map<String, MDC> mdcs;

    MDedupExplorationIO mdexplio;

    RecordMetricsGenerator gnrRecords;
    PairMetricsGenerator gnrPairs;
    MDMetricsGenerator gnrMDs;

    public MDedupExplorationController(String dbUser, String dbPassword, String dbDriver, String dbConnection,
                                       String dataset, MDedupExplorationIO mdexplio,
                                       Map<String, Map<String, String>> records,
                                       Map<String, MD> mds, Map<String, MDC> mdcs, String querySims) {
        super(dbUser, dbPassword, dbDriver, dbConnection);

        this.dataset = dataset;
        this.mdexplio = mdexplio;
        this.records = records;
        this.mds = mds;

        // Given the MDCs of selection and expansion.
        this.mdcs = mdcs;

        importSimilarities(querySims);
    }

    public void execute() {
        Instant start = Instant.now();
        // Retrieve the MDCs with their features.
        Map<String, Map<String, Double>> mdcsWithFeatures = getMDCsWithFeatures();
        Instant finish = Instant.now();
        long totalExecutionDuration = Duration.between(start, finish).toMillis();

        // Export them.
        mdexplio.exportTable(new MDedupExplorationIO.PrepareStatementParametersExploration(
                dataset,
                mdcsWithFeatures,
                mdcs,
                totalExecutionDuration
        ));
        mdexplio.close();
    }

    public Map<String, Map<String, Double>> getMDCsWithFeatures() {
        gnrMDs = new MDMetricsGenerator(mds, records, sims, mdcs);
        gnrPairs = new PairMetricsGenerator(mds, records, sims, mdcs);
        gnrRecords = new RecordMetricsGenerator(mds, records, sims, mdcs);

        Map<String, Map<String, Double>> mdcsWithFeatures = new HashMap<>();
        mdcsWithFeatures.putAll(gnrMDs.getMetrics());
        mdcsWithFeatures.putAll(gnrPairs.getMetrics());
        mdcsWithFeatures.putAll(gnrRecords.getMetrics());

        return mdcsWithFeatures;
    }

    public static Set<String> getMDCsFeatures() {
        Set<String> features = new HashSet<>();
        features.addAll(new MDMetricsGenerator().enumerateMetrics());
        features.addAll(new PairMetricsGenerator().enumerateMetrics());
        features.addAll(new RecordMetricsGenerator().enumerateMetrics());

        return features;
    }

    public Map<String, MD> getMds() {
        return mds;
    }

    public Map<String, Map<String, String>> getRecords() {
        return records;
    }


    public Map<String, MDC> getMdcs() {
        return mdcs;
    }


    public MDedupExplorationIO getMdexplio() {
        return mdexplio;
    }

    public void setMds(Map<String, MD> mds) {
        this.mds = mds;
    }

    public void setRecords(Map<String, Map<String, String>> records) {
        this.records = records;
    }


    public void setMdcs(Map<String, MDC> mdcs) {
        this.mdcs = mdcs;
    }

    public Map<Integer, SimilaritiesPair> getSims() {
        return sims;
    }

    public void setSims(Map<Integer, SimilaritiesPair> sims) {
        this.sims = sims;
    }

    public static void main(String[] args) {
        System.out.println("Printing features");
        List<String> features = new ArrayList<>(getMDCsFeatures());
        features.sort(String::compareTo);
        System.out.println(features.toString());
    }
}
