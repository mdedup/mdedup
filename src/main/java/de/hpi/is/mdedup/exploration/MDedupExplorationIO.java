package de.hpi.is.mdedup.exploration;

import de.hpi.is.mdedup.MDedupIO;
import de.hpi.is.mdedup.selection.MDedupSelectionIO;
import de.hpi.is.mdedup.selection.utils.OptimalMDCInfo;
import de.hpi.is.mdedup.utils.ClassificationEvaluation;
import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.SimilaritiesPair;
import de.hpi.is.mdedup.utils.entities.MDC;
import org.apache.commons.lang3.StringUtils;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MDedupExplorationIO extends MDedupIO {

    public MDedupExplorationIO(String dbDriver, String dbConnection, String dbUser, String dbPassword) {
        super(dbDriver, dbConnection, dbUser, dbPassword);
    }

    public MDedupExplorationIO(Connection conn) {
        super(conn);
    }

    public static class PrepareStatementParametersExploration extends PrepareStatementParameters {
        protected Map<String, Map<String, Double>> mdcsWithFeatures;
        protected Map<String, MDC> mdcs;

        protected Long totalExecutionDuration;

        protected List<Map<String, Object>> data;

        public PrepareStatementParametersExploration(String dataset, Map<String, Map<String, Double>> mdcsWithFeatures,
                                                    Map<String, MDC> mdcs, Long totalExecutionDuration) {
            super(dataset, "mdedup_mdcs");
            this.mdcsWithFeatures = mdcsWithFeatures;
            this.mdcs = mdcs;
            this.totalExecutionDuration = totalExecutionDuration;
        }

        public List<Map<String, Object>> prepareData() {
            Set<String> header = new HashSet<>();
            data = mdcs.entrySet().stream().map(e -> {
                Map<String, Object> r = new HashMap<>();

                MDC mdc = e.getValue();
                Map<String, Double> mdcFeatures = mdcsWithFeatures.keySet().stream().collect(Collectors.toMap(
                        f -> f,
                        f -> mdcsWithFeatures.get(f).get(e.getKey())
                ));

                r.put("phase", "exploration");

                List<String> fieldsToCopy = Arrays.asList(
                    "ids", "dataset", "children",
                    "max_level", "level", "topk", "topk_position", "prediction_model", "prediction_model_parameters",
                    "precision_", "recall", "f1", "f05", "f20", "auc_roc", "auc_pr", "tp", "fn", "fp", "tn",
                    "expansion_phase", "enabled_expansion_phase", "features_in_regression"
                );

                fieldsToCopy.forEach(f -> r.put(f, mdc.getFields().get(f)));

                List<String> fieldsToNull = Arrays.asList(
                        "lattice_max_duration", "lattice_actual_duration", "lattice_md_duration"
                );
                fieldsToNull.forEach(f -> r.put(f, 0));
                r.put("is_execution_interrupted", false);
                r.put("total_execution_duration", totalExecutionDuration.toString());

                for (String feature : mdcFeatures.keySet()) {
                    r.put(feature, mdcFeatures.get(feature).toString());
                }

                if (header.size() == 0) {
                    header.addAll(r.keySet());
                }

                return r;
            }).collect(Collectors.toList());

            this.header = new ArrayList<>(header);
            this.header.sort(String::compareTo);

            return data;
        }
    }

}
