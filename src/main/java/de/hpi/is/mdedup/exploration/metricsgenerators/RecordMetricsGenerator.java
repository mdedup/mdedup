package de.hpi.is.mdedup.exploration.metricsgenerators;

import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.SimilaritiesPair;
import de.hpi.is.mdedup.utils.entities.MDC;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class RecordMetricsGenerator extends MetricsGenerator {
    MDMetricsGenerator gMD;

    public RecordMetricsGenerator(Map<String, MD> mds, Map<String, Map<String, String>> data,
                                  Map<Integer, SimilaritiesPair> sims, Map<String, MDC> mdcs) {
        super(mds, data, sims, mdcs);
        gMD = new MDMetricsGenerator(mds, data, sims, mdcs);
    }

    public RecordMetricsGenerator() {
    }

    public Map<String, Map<String, Double>> getMetrics() {
        Map<String, Map<String, Double>> metrics = new HashMap<>();

        metrics.put("completeness", getCompleteness());
        metrics.put("uniqueness", getUniqueness());

        return metrics;
    }

    public static Set<String> enumerateRecordMetrics() {
        return new RecordMetricsGenerator().enumerateMetrics();
    }

    @Override
    public Set<String> enumerateMetrics() {
        HashSet<String> metricsNames = new HashSet<>();

        metricsNames.add("completeness");
        metricsNames.add("uniqueness");

        return metricsNames;
    }

    /**
     *
     * Cardinality metric
     *
     * @return
     */
    public Map<String, Double> getCompleteness() {
        ConcurrentHashMap<String, Double> completeness = new ConcurrentHashMap<>();
        mdcs.keySet().parallelStream().forEach(mdcID -> {
            List<List<String>> attributesOfMDs = gMD.getPartOfMDC(mdcID, MDMetricsGenerator.MDPart.ATTRIBUTE);
            Set<String> attributesSet = new HashSet<>();
            attributesOfMDs.forEach(attributesSet::addAll);

            int counter = 0;
            records:for (Map.Entry<String, Map<String, String>> r: data.entrySet()) {
                for (String attr : attributesSet) {
                    if (   (r.getValue().containsKey(attr) && r.getValue().get(attr) == null) ||
                            r.getValue().getOrDefault(attr, "").equals("")) {
                        continue records;
                    }
                }
                counter += 1;
            }

            completeness.put(mdcID, (double) counter / data.size());
        });
        return completeness;
    }

    /**
     *
     * Uniqueness
     *
     * @return
     */
    public Map<String, Double> getUniqueness() {
        ConcurrentHashMap<String, Double> uniqueness = new ConcurrentHashMap<>();
        mdcs.keySet().parallelStream().forEach(mdcID -> {
            List<List<String>> attributesOfMDs = gMD.getPartOfMDC(mdcID, MDMetricsGenerator.MDPart.ATTRIBUTE);
            Set<String> attributesSet = new HashSet<>();
            attributesOfMDs.forEach(attributesSet::addAll);
            List<String> attributes = new ArrayList<>(attributesSet);
            Collections.sort(attributes);

            HashSet<String> valueCombinations = new HashSet<>();
            records:for (Map.Entry<String, Map<String, String>> r: data.entrySet()) {
                StringBuilder sb = new StringBuilder();
                for (String attribute : attributes) {
                    sb.append(r.getValue().getOrDefault(attribute, ""));
                }
                valueCombinations.add(sb.toString());
            }
            uniqueness.put(mdcID, (double) valueCombinations.size() / data.size());
        });
        return uniqueness;
    }

}
