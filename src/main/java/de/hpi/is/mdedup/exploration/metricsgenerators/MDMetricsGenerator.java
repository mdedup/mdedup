package de.hpi.is.mdedup.exploration.metricsgenerators;

import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.entities.MDC;
import de.hpi.is.mdedup.utils.entities.MDCondition;
import de.hpi.is.mdedup.utils.SimilaritiesPair;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class MDMetricsGenerator extends MetricsGenerator {

    public enum MDPart {
        COLUMN_MATCH, ATTRIBUTE, THRESHOLD, SIMILARITY_MEASURE
    }

    public enum SetOperation {
        INTERSECTION, UNION, JACCARD,
        MIN, MEDIAN, MAX, STDEV
    }

    public MDMetricsGenerator(Map<String, MD> mds, Map<String, Map<String, String>> data,
                              Map<Integer, SimilaritiesPair> sims, Map<String, MDC> mdcs) {
        super(mds, data, sims, mdcs);
    }

    public Map<String, Map<String, Double>> getMetrics() {
        Map<String, Map<String, Double>> metrics = new HashMap<>();

        metrics.putAll(getThresholdMDCStats());
        metrics.put("cardinality", getCardinalityOfMDC());
        for (MDPart part: MDPart.values()) {
            for (SetOperation so : SetOperation.values()) {
                metrics.put("set_size_" + part.toString() + "_" + so.toString(),getSetSizeStatOfMDCs(part, so));
            }
        }

        return metrics;
    }

    public MDMetricsGenerator() {
    }

    public static Set<String> enumerateMDMetrics() {
        return new MDMetricsGenerator().enumerateMetrics();
    }

    @Override
    public Set<String> enumerateMetrics() {
        HashSet<String> metricsNames = new HashSet<>();

        metricsNames.add("cardinality");
        metricsNames.add("thresholds_min");
        metricsNames.add("thresholds_max");
        metricsNames.add("thresholds_median");

        for (MDPart part: MDPart.values()) {
            for (SetOperation so : SetOperation.values()) {
                metricsNames.add("set_size_" + part.toString() + "_" + so.toString());
            }
        }

        return metricsNames;
    }

    /**
     *
     * Set size metrics.
     *
     * @param part
     * @param stat
     * @return
     */
    public Map<String, Double> getSetSizeStatOfMDCs(MDPart part, SetOperation stat) {
        ConcurrentHashMap<String, Double> statPerMDC = new ConcurrentHashMap<>();
        mdcs.keySet().parallelStream().forEach(mdcID -> {
            statPerMDC.put(mdcID, getSetSizeStatOfMDC(mdcID, part, stat));
        });
        return statPerMDC;
    }

    private Double getSetSizeStatOfMDC(String mdcID, MDPart part, SetOperation stat) {
        List<List<String>> partsOfMDs = getPartOfMDC(mdcID, part);

        switch (stat) {
            case UNION:
                Set<String> union = new HashSet<>();
                for (List<String> partsOfMD : partsOfMDs) {
                    union.addAll(partsOfMD);
                }
                return (double) union.size();
            case INTERSECTION:
                Set<String> intersection = new HashSet<>(partsOfMDs.get(0));
                for (int i = 1; i < partsOfMDs.size(); ++i) {
                    intersection.retainAll(partsOfMDs.get(i));
                }
                return (double) intersection.size();
            case JACCARD:
                Double intersectionValue = getSetSizeStatOfMDC(mdcID, part, SetOperation.INTERSECTION);
                Double unionValue = getSetSizeStatOfMDC(mdcID, part, SetOperation.UNION);
                return intersectionValue / unionValue;
            case MIN:
                List<Integer> sizesOfParts = partsOfMDs.stream().mapToInt(List::size).boxed()
                        .collect(Collectors.toList());
                return (double) Collections.min(sizesOfParts);
            case MAX:
                sizesOfParts = partsOfMDs.stream().mapToInt(List::size).boxed()
                        .collect(Collectors.toList());
                return (double) Collections.max(sizesOfParts);
            case MEDIAN:
                sizesOfParts = partsOfMDs.stream().mapToInt(List::size).boxed()
                        .collect(Collectors.toList());
                Collections.sort(sizesOfParts);

                double medianValue;
                int middle = sizesOfParts.size()/2;
                if (sizesOfParts.size()%2 == 1)
                    medianValue = (double) sizesOfParts.get(middle);
                else
                    medianValue = (sizesOfParts.get(middle-1) + sizesOfParts.get(middle)) / 2.0;

                return medianValue;
            case STDEV:
                sizesOfParts = partsOfMDs.stream().mapToInt(List::size).boxed()
                        .collect(Collectors.toList());
                SummaryStatistics ss = new SummaryStatistics();
                sizesOfParts.forEach(ss::addValue);

                return ss.getStandardDeviation();
            default:
                return null;
        }
    }

    /**
     *
     * Threshold metrics
     *
     * @return
     */
    public Map<String, Map<String, Double>> getThresholdMDCStats() {
        ConcurrentHashMap<String, Map<String, Double>> thresholdStatsPerMDC = new ConcurrentHashMap<>();
        mdcs.keySet().parallelStream().forEach(mdcID -> {
            List<List<String>> partsOfMDs = getPartOfMDC(mdcID, MDPart.THRESHOLD);
            List<Double> thresholds = new ArrayList<>();
            partsOfMDs.forEach(p -> p.forEach(v -> thresholds.add(Double.valueOf(v))));

            Map<String, Double> thresholdStats = new HashMap<>();
            thresholdStats.put("min", Collections.min(thresholds));
            thresholdStats.put("max", Collections.max(thresholds));

            Collections.sort(thresholds);
            int middle = thresholds.size()/2;
            double medianValue;
            if (thresholds.size()%2 == 1)
                medianValue = thresholds.get(middle);
            else
                medianValue = (thresholds.get(middle-1) + thresholds.get(middle)) / 2.0;
            thresholdStats.put("median", medianValue);

            thresholdStatsPerMDC.put(mdcID, thresholdStats);
        });

        /* Invert Map... <MDC, <Metric, value>> --> <Metric, <MDC, value>> */
        Map<String, Map<String, Double>> results = new HashMap<>();
        results.put("thresholds_min", new HashMap<>());
        results.put("thresholds_max", new HashMap<>());
        results.put("thresholds_median", new HashMap<>());
        for (String mdcID: thresholdStatsPerMDC.keySet()) {
            results.get("thresholds_min").put(mdcID, thresholdStatsPerMDC.get(mdcID).get("min"));
            results.get("thresholds_max").put(mdcID, thresholdStatsPerMDC.get(mdcID).get("max"));
            results.get("thresholds_median").put(mdcID, thresholdStatsPerMDC.get(mdcID).get("median"));
        }
        return results;
    }


    protected List<List<String>> getPartOfMDC(String mdcID, MDPart part) {
        List<List<String>> partsOfMDs = new ArrayList<>();
        for (String mdID: mdcs.get(mdcID).getMDsIDs()) {
            List<String> parts = new ArrayList<>();
            MD md = mds.get(mdID);
            for (MDCondition cnd : md.getLhs()) {
                switch (part) {
                    case ATTRIBUTE:
                        parts.add(cnd.getAttribute());
                        break;
                    case THRESHOLD:
                        parts.add(cnd.getThreshold().toString());
                        break;
                    case SIMILARITY_MEASURE:
                        parts.add(cnd.getMeasure());
                        break;
                    case COLUMN_MATCH:
                        parts.add(cnd.toString());
                        break;
                }
            }
            partsOfMDs.add(parts);
        }
        return partsOfMDs;
    }


    /**
     *
     * Cardinality metric
     *
     * @return
     */
    public Map<String, Double> getCardinalityOfMDC() {
        ConcurrentHashMap<String, Double> cardinality = new ConcurrentHashMap<>();
        mdcs.keySet().parallelStream().forEach(mdcID -> cardinality.put(mdcID, (double) mdcs.get(mdcID).getMDsIDs().size()));
        return cardinality;
    }

}

