package de.hpi.is.mdedup;

import de.hpi.is.mdedup.boosting.classification.utils.ClassificationDedupPairScored;
import de.hpi.is.mdedup.exploration.MDedupExplorationController;
import de.hpi.is.mdedup.utils.DedupPair;
import de.hpi.is.mdedup.utils.SimilaritiesPair;
import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.entities.MDC;
import me.tongfei.progressbar.ProgressBar;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * A template that is used across different phases to import and export data. Some basic methods are implemented
 *  to be extended or re-used from child classes.
 */
public class MDedupIO {

    protected Connection conn;

    protected void resetTable() throws SQLException {

    }
    protected void dropTable() throws SQLException {

    }

    public MDedupIO(String dbDriver, String dbConnection, String dbUser, String dbPassword) {
        try {
            Class.forName(dbDriver);
            conn = DriverManager.getConnection(dbConnection, dbUser, dbPassword);

            conn.setAutoCommit(false);
        }
        catch (Exception e)
        {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }

    public MDedupIO(Connection conn) {
        this.conn = conn;
    }

    public void close() {
        try {
            if (conn != null) {
                conn.close();
                conn = null;
            }
        } catch (Exception e) {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }

    public static Map<String, Map<String, String>> importTable(Connection conn, String sql, List<String> columns) {
        Map<String, Map<String, String>> records = new HashMap<>();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery(sql);
            ResultSetMetaData rsmd = rs.getMetaData();

            if (columns == null) {
                columns = IntStream.range(1, rsmd.getColumnCount() + 1).mapToObj(i -> {
                    try {
                        return rsmd.getColumnName(i);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    return null;
                }).filter(Objects::nonNull).collect(Collectors.toList());
            }

            while (rs.next()) {
                String id = rs.getString("id");

                Map<String, String> record = columns.stream().collect(HashMap::new, (m,c) ->
                {
                    try {
                        m.put(c, rs.getString(c));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }, HashMap::putAll);
                records.put(id, record);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return records;
    }

    public Map<String, Map<String, String>> importTable(String sql) {
        return importTable(conn, sql, null);
    }

    public void exportTable(PrepareStatementParameters psp) {
        List<Map<String, Object>> data = psp.prepareData();
        if (data.size() == 0) {
            return;
        }
        final int BATCH_SIZE = 100;

        try {
            createTable();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Create the MySQL insert preparedStatement.
        try {
            List<String> header = psp.getHeader();
            // The MySQL insert statement.
            System.out.println("Preparing SQL statement");
            String query = " insert into " + psp.getTableName() +
                    " (" + String.join(", ", header) + ")"
                    + " values (" + header.stream().map(c -> "?")
                    .collect(Collectors.joining(", "))+")";
            PreparedStatement stmt = conn.prepareStatement(query);
            System.out.println("Prepared SQL statement");

            try (ProgressBar pb = new ProgressBar("Export progress", data.size())) { // name, initial max
                for (int i = 0; i < data.size(); ++i) {
                    pb.step();
                    Map<String, Object> statementParams = data.get(i);

                    int j = 0;
                    for (String h : header) {
                        stmt.setObject(++j, statementParams.getOrDefault(h, null));
                    }
                    stmt.addBatch();
                    if (i % BATCH_SIZE == BATCH_SIZE - 1) {
                        try {
                            stmt.executeBatch();
                            conn.commit();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
                stmt.executeBatch();
                conn.commit();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void createTable() throws SQLException {
        List<String> features = new ArrayList<>(MDedupExplorationController.getMDCsFeatures());
        Collections.sort(features);

        List<String> featuresWithType = new ArrayList<>();
        for (String feature : features) {
            featuresWithType.add(feature + " " + "VARCHAR(100)");
        }

        String sql = "CREATE TABLE IF NOT EXISTS `mdedup_mdcs` (" +
                "`id` int(11) NOT NULL AUTO_INCREMENT," +
                "`dataset` varchar(45) DEFAULT NULL," +
                "`ids` longtext," +
                "`children` longtext," +

                "`phase` varchar(45) DEFAULT NULL," +

                "`max_level` int(20) DEFAULT NULL," +
                "`level` int(40) DEFAULT NULL," +

//                "`topk` int(11) DEFAULT NULL," +
                "`topk_position` int(11) DEFAULT NULL," +

                "`topk` int(11) DEFAULT NULL," +
                "`enabled_expansion_phase` boolean DEFAULT NULL," +
                "`features_in_regression` longtext DEFAULT NULL," +

                "`expansion_phase` VARCHAR(40)," +

                "`prediction_model` VARCHAR(40)," +
                "`prediction_model_parameters` VARCHAR(40)," +
                "`prediction_score` DOUBLE," +

                "`precision_` double DEFAULT NULL," +
                "`recall` double DEFAULT NULL," +
                "`f1` double DEFAULT NULL," +

                "`f05` double DEFAULT NULL," +
                "`f20` double DEFAULT NULL," +

                "`auc_roc` double DEFAULT NULL," +
                "`auc_pr` double DEFAULT NULL," +

                "`tp` int(40) DEFAULT NULL," +
                "`fn` int(40) DEFAULT NULL," +
                "`fp` int(40) DEFAULT NULL," +
                "`tn` int(40) DEFAULT NULL," +

                "`is_execution_interrupted` BOOLEAN DEFAULT NULL," +
                "`lattice_max_duration` bigint(100) DEFAULT NULL," +
                "`lattice_actual_duration` bigint(100) DEFAULT NULL," +
                "`lattice_md_duration` bigint(100) DEFAULT NULL," +
                "`total_execution_duration` bigint(100) DEFAULT NULL," +

                String.join(", ", featuresWithType) +
                ", PRIMARY KEY (`id`)" +
                ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";


        System.out.println(sql);
        Statement stmt = conn.createStatement();
        stmt.setQueryTimeout(20);
        stmt.execute(sql);
        conn.commit();
    }


    public Map<Integer, SimilaritiesPair> importSimilarities(String querySims) {
        Map<String, Map<String, String>> simsTotalPlain = importTable(querySims);

        List<String> mainAttributes = new ArrayList<>(Arrays.asList("dataset", "id", "id1", "id2", "class"));
        return simsTotalPlain.entrySet().stream().collect(Collectors.toMap(
                e -> Integer.valueOf(e.getKey()),
                e -> {
                    Map<String, String> m = new HashMap<>(e.getValue());
                    Map<String, Double> similarities = new HashMap<>();
                    m.keySet().removeAll(mainAttributes);
                    m.forEach((key, value) -> similarities.put(key, Double.valueOf(value)));
                    return new SimilaritiesPair(
                            e.getValue().get("dataset"),
                            e.getValue().get("id"),
                            e.getValue().get("id1"),
                            e.getValue().get("id2"),
                            DedupPair.PairClass.valueOf(e.getValue().get("class").toUpperCase()),
                            similarities
                    );
                }
        ));
    }

    public Map<Integer, ClassificationDedupPairScored> importClassificationPairsScored(String query) {
        Map<String, Map<String, String>> simsTotalPlain = importTable(query);

//        List<String> mainAttributes = new ArrayList<>(Arrays.asList("dataset", "id", "id1", "id2", "pair_class", "classified_as", "score"));
        return simsTotalPlain.entrySet().stream().collect(Collectors.toMap(
                e -> Integer.valueOf(e.getKey()),
                e -> {
                    Map<String, String> m = e.getValue();
                    return new ClassificationDedupPairScored(
                            m.get("dataset"),
                            m.get("pair_id"),
                            m.get("pair_id1"),
                            m.get("pair_id2"),
                            DedupPair.PairClass.valueOf(m.get("pair_class")),
                            DedupPair.PairClass.valueOf(m.get("pair_classified_class")),
                            Double.valueOf(m.get("classification_score")),
                            Double.valueOf(m.get("similarity_score"))
                    );
                }
        ));
    }

    public Map<String, Map<String, String>> importMDsPlain(String queryHyMD) {
        return importTable(conn, queryHyMD, new ArrayList<>(Arrays.asList("id", "dataset", "lhs", "rhs", "support")));
    }

    public Map<String, MD> importMDs(String queryHyMD) {
        Map<String, Map<String, String>> mdsPlain = importMDsPlain(queryHyMD);

        Map<String, MD> mds = mdsPlain.entrySet().stream().collect(Collectors.toMap(
                Map.Entry::getKey,
                e -> new MD(
                        e.getValue().get("id"),
                        e.getValue().get("dataset"),
                        e.getValue().get("lhs"),
                        e.getValue().get("rhs"))
        ));
        return mds;
    }

    public List<List<Integer>> importMDCsByPhase(String dataset, String phase) {
        Map<String, Map<String, String>> mdcMDidsListPlain = importTable(
                "select * from mdedup.mdedup_mdcs where dataset=\""+dataset+"\" and phase=\""+phase+"\";");

        return mdcMDidsListPlain.values().stream().map(m ->
                Arrays.stream(m.get("ids").split("_")).mapToInt(Integer::valueOf).boxed().collect(Collectors.toList())
        ).collect(Collectors.toList());
    }

    public Map<String, MDC> importMDCsByPhase(String queryMDCs) {
        Map<String, Map<String, String>> data = importTable(conn, queryMDCs, null);

        Map<String, MDC> mdcs = new HashMap<>();
        for (String id: data.keySet()) {
            Map<String, String> mdcData = data.get(id);
            MDC mdc = new MDC(mdcData);
            mdcs.put(mdcData.get("id"), mdc);
        }
        return mdcs;
    }


    public static abstract class PrepareStatementParameters {

        protected String dataset;
        protected String tableName;
        protected List<String> mdcFeatures;
        protected List<String> header;

        public PrepareStatementParameters(String dataset, String tableName) {
            this.dataset = dataset;
            this.tableName = tableName;

            mdcFeatures = new ArrayList<>(MDedupExplorationController.getMDCsFeatures());
            mdcFeatures.sort(String::compareTo);
        }

        public abstract List<Map<String, Object>> prepareData();

        public String getDataset() {
            return dataset;
        }

        public String getTableName() {
            return tableName;
        }

        public List<String> getHeader() {
            return header;
        }
    }

}
