package de.hpi.is.mdedup.boosting.classification.utils;

import de.hpi.is.mdedup.utils.DedupPair;

import java.util.Comparator;

public class ClassificationDedupPairScored extends ClassificationDedupPair {

    protected Double classificationScore;
    protected Double similarityScore;


    public ClassificationDedupPairScored(String dataset, String id, String id1, String id2, PairClass pairClass,
                                         PairClass classifiedAs, Double classificationScore, Double similarityScore) {
        super(dataset, id, id1, id2, pairClass, classifiedAs);
        this.classificationScore = classificationScore;
        this.similarityScore = similarityScore;
    }

    @Override
    public int compareTo(DedupPair o) {
        if (!(o instanceof ClassificationDedupPair)) {
            return 0;
        } else {
            Comparator<de.hpi.is.mdedup.boosting.classification.utils.ClassificationDedupPairScored> cmp = Comparator
                    .comparing(de.hpi.is.mdedup.boosting.classification.utils.ClassificationDedupPairScored::getDataset)
                    .thenComparing(de.hpi.is.mdedup.boosting.classification.utils.ClassificationDedupPairScored::getClassificationScore)
                    .thenComparing(de.hpi.is.mdedup.boosting.classification.utils.ClassificationDedupPairScored::getSimilarityScore)
                    .thenComparing(de.hpi.is.mdedup.boosting.classification.utils.ClassificationDedupPairScored::getId1)
                    .thenComparing(de.hpi.is.mdedup.boosting.classification.utils.ClassificationDedupPairScored::getId2)
                    .thenComparing(de.hpi.is.mdedup.boosting.classification.utils.ClassificationDedupPairScored::getPairClass)
                    .thenComparing(de.hpi.is.mdedup.boosting.classification.utils.ClassificationDedupPairScored::getClassifiedAs);

            return cmp.compare(this, (de.hpi.is.mdedup.boosting.classification.utils.ClassificationDedupPairScored) o);
        }
    }

    public Double getClassificationScore() {
        return classificationScore;
    }

    public Double getSimilarityScore() {
        return similarityScore;
    }
}
