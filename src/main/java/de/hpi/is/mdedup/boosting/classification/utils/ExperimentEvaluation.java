package de.hpi.is.mdedup.boosting.classification.utils;

import de.hpi.is.mdedup.utils.ClassificationEvaluation;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.util.Map;
import java.util.TreeMap;

public class ExperimentEvaluation {

    protected ClassificationEvaluation classificationEvaluation = null;
    protected ClassificationEvaluation.ClassificationEvaluationExtended classificationEvaluationExtended = null;
    protected HardwareInfo hardwareInfo = null;
    protected Long executionTimeClassification = -1L;
    protected Long executionTimeClassificationSearchSpace = -1L;
    protected Integer parametersSearchSpace = -1;

    public ExperimentEvaluation(ClassificationEvaluation classificationEvaluation,
                                ClassificationEvaluation.ClassificationEvaluationExtended classificationEvaluationExtended,
                                Long executionTimeClassification) {

        this.classificationEvaluation = classificationEvaluation;
        this.classificationEvaluationExtended = classificationEvaluationExtended;
        this.executionTimeClassification = executionTimeClassification;
        this.hardwareInfo = new HardwareInfo();
        hardwareInfo.retrieve();
    }

    public Map<String, Double> toMap() {
        Map<String, Double> m = new TreeMap<>();

        if (classificationEvaluation != null) {
            m.putAll(classificationEvaluation.toMap());
        }
        if (classificationEvaluationExtended != null) {
            m.putAll(classificationEvaluationExtended.toMap());
        }

        m.put("executionTimeClassification", executionTimeClassification.doubleValue());

        m.putAll(hardwareInfo.toMap());

        return m;
    }


    public ClassificationEvaluation getClassificationEvaluation() {
        if (classificationEvaluationExtended != null) {
            return classificationEvaluationExtended;
        } else {
            return classificationEvaluation;
        }
    }

    public ClassificationEvaluation.ClassificationEvaluationExtended getClassificationEvaluationExtended() {
        return classificationEvaluationExtended;
    }

    public void setExecutionTimeClassification(Long executionTimeClassification) {
        this.executionTimeClassification = executionTimeClassification;
    }

    public HardwareInfo getHardwareInfo() {
        return hardwareInfo;
    }

    public Long getExecutionTimeClassification() {
        return executionTimeClassification;
    }

    public static class HardwareInfo {
        protected static OperatingSystemMXBean systemMonitor = null;

        protected Integer availableProcessors = -1;

        protected Long totalMemory = -1L;
        protected Long maxMemory = -1L;
        protected Long usedMemory = -1L;
        protected Long freeMemory = -1L;

        protected Double systemCPULoad = -1.0;

        public HardwareInfo() {
            systemMonitor = (com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        }

        public void retrieve() {
            availableProcessors = Runtime.getRuntime().availableProcessors();
            systemCPULoad = systemMonitor.getSystemLoadAverage();

            maxMemory = (Runtime.getRuntime().maxMemory() / 1024) / 1024;
            totalMemory = (Runtime.getRuntime().totalMemory() / 1024) / 1024;
            freeMemory = (Runtime.getRuntime().freeMemory() / 1024) / 1024;
            usedMemory = totalMemory - freeMemory;
        }

        public Map<String, Double> toMap() {
            Map<String, Double> m = new TreeMap<>();

            m.put("availableProcessors", availableProcessors.doubleValue());
            m.put("totalMemory", totalMemory.doubleValue());
            m.put("maxMemory", maxMemory.doubleValue());
            m.put("usedMemory", usedMemory.doubleValue());
            m.put("freeMemory", freeMemory.doubleValue());
            m.put("systemCPULoad", systemCPULoad);

            return m;
        }
    }

    @Override
    public String toString() {
        return classificationEvaluation.calculateFMeasure(1.0).toString();
    }

    public Long getExecutionTimeClassificationSearchSpace() {
        return executionTimeClassificationSearchSpace;
    }

    public void setExecutionTimeClassificationSearchSpace(Long executionTimeClassificationSearchSpace) {
        this.executionTimeClassificationSearchSpace = executionTimeClassificationSearchSpace;
    }

    public Integer getParametersSearchSpace() {
        return parametersSearchSpace;
    }

    public void setParametersSearchSpace(Integer parametersSearchSpace) {
        this.parametersSearchSpace = parametersSearchSpace;
    }
}
