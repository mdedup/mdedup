package de.hpi.is.mdedup.boosting.classification.utils;

import smile.math.Math;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This is an modification of the CrossValidation class found in the SMILE package.
 * I wanted the permutation to be done with the same seed, so that we don't get inconsistencies across executions.
 *
 * Moreover, I wanted the splits to be stratified. This means that every split should have an equal number of labels
 * or classes.
 */
public class StratifiedCrossValidationStandardSeedPermutation {
    /**
     * The number of rounds of cross validation.
     */
    public final int k;
    /**
     * The index of training instances.
     */
    public final int[][] train;
    /**
     * The index of testing instances.
     */
    public final int[][] test;

    /**
     * Constructor.
     //     * @param n the number of samples.
     * @param k the number of rounds of cross validation.
     */
    public StratifiedCrossValidationStandardSeedPermutation(int[] Y, int k) {
        int n = Y.length;

        if (k < 0 || k > n) {
            throw new IllegalArgumentException("Invalid number of CV rounds: " + k);
        }

        this.k = k;

        Map<Integer, List<Integer>> labelToIndices = new HashMap<>();
        IntStream.range(0, Y.length)
                .forEach(i -> {
                    Integer label = Y[i];
                    List<Integer> l = labelToIndices.getOrDefault(label, new ArrayList<>());
                    l.add(i);
                    labelToIndices.put(label, l);
                });

        Math.setSeed(0L); // Seed for permutations
        Random r = new Random(0L);

        train = new int[k][];
        test = new int[k][];

        for (int i = 0; i < k; i++) {
            Map<Integer, Integer> start = new HashMap<>();
            Map<Integer, Integer> end = new HashMap<>();

            int trainInstances = 0;
            int testInstances = 0;
            for (Map.Entry<Integer, List<Integer>> e : labelToIndices.entrySet()) {
                Integer label = e.getKey();
                List<Integer> indices = e.getValue();
                int chunk = indices.size() / k;
                start.put(label, chunk * i);
                end.put(label, chunk * (i + 1));
                if (i == k-1) end.put(label, indices.size());

                if (k == 1) {
                    int chunks = indices.size()/10;
                    int trainSize = chunks * 5;
                    int testSize = chunks * 5;

                    if (trainSize == 0) {
                        if (indices.size() == 1) {
                            trainSize = 1;
                            testSize = 0;
                        } else {
                            trainSize = indices.size() - 1;
                            testSize = 1;
                        }
                    }
                    if (testSize > 0) {
                        start.put(label, 0);
                        end.put(label, testSize);
                    } else {
                        start.put(label, -1);
                        end.put(label, -1);
                    }
                    trainInstances += trainSize;
                    testInstances += testSize;
                }
//                if ((start.get(label) + 1) == end.get(label)) {
//                    start.put(label, -1);
//                    end.put(label, -1);
//
//                    trainInstances += 1;
////                    testInstances += 0;
//                }
                else {
                    trainInstances += indices.size() - (end.get(label) - start.get(label));
                    testInstances += end.get(label) - start.get(label);
                }
            }

            train[i] = new int[trainInstances];
            test[i] = new int[testInstances];

            int iTrain = 0;
            int iTest = 0;
            for (Integer label : labelToIndices.keySet()) {
                List<Integer> indices = labelToIndices.get(label);
                for (int j = 0; j < indices.size(); ++j) {
                    if (j >= start.get(label) && j < end.get(label)) {
                        test[i][iTest++] = indices.get(j);
                    } else {
                        train[i][iTrain++] = indices.get(j);
                    }
                }
            }

            // Since the classes are one after the other on train and test, we'll shuffle the indices.
            shuffleArray(train[i], r);
            shuffleArray(test[i], r);

//            // Train has to have at least 1 instance of every class. Worst case swap train and test sets.
//            if (trainInstances == 0) {
//                int[] tmp = train[i];
//                train[i] = test[i];
//                test[i] = tmp;
//            }
        }
    }

    // https://stackoverflow.com/questions/1519736/random-shuffling-of-an-array
    // Implementing Fisher–Yates shuffle
    private void shuffleArray(int[] ar, Random r)
    {
        for (int i = ar.length - 1; i > 0; i--)
        {
            int index = r.nextInt(i + 1);
            // Simple swap
            int a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

    /**
     * Alternatively, we could shuffle using Java 8 methods.
     * @param array
     * @param r: Random generator.
     * @return
     */
    private int[] shuffleArraySimple(int[] array, Random r)
    {
        List<Integer> list = Arrays.stream(array).boxed().collect(Collectors.toList());
        Collections.shuffle(list, r);
        return list.stream().mapToInt(i->i).toArray();
    }

    public static void main(String[] args) {
        int[] l = new int[80];
        for (int i = 0; i < 20; ++i) {
            l[i] = 0;
        }
        for (int i = 20; i < 40; ++i) {
            l[i] = 1;
        }
        for (int i = 40; i < 80; ++i) {
            l[i] = 2;
        }
//        shuffleArray(l, new Random(0));
        System.out.println(Arrays.toString(l));
        StratifiedCrossValidationStandardSeedPermutation scv = new StratifiedCrossValidationStandardSeedPermutation(l, 10);

        System.out.println("hi");
        for (int fold = 0; fold < scv.k; ++fold) {
            Map<Integer, Integer> counters = new HashMap<>();
            int[] train = scv.train[fold];
            int[] test = scv.test[fold];

            for (int iTrain = 0; iTrain < train.length; ++iTrain) {
                Integer label = l[train[iTrain]];
                counters.put(label, counters.getOrDefault(label, 0) + 1);
            }
            System.out.println("Train");
            System.out.println(counters.toString());
            counters.clear();
            for (int iTest = 0; iTest < test.length; ++iTest) {
                Integer label = l[test[iTest]];
                counters.put(label, counters.getOrDefault(label, 0) + 1);
            }
            System.out.println("Test");
            System.out.println(counters.toString());
        }
    }
}
