package de.hpi.is.mdedup.boosting;

import de.hpi.is.mdedup.MDedupController;
import de.hpi.is.mdedup.utils.classifiedpairs.MDCClassifiedPairsIO;
import de.hpi.is.mdedup.boosting.classification.Classifier;
import de.hpi.is.mdedup.boosting.classification.utils.ClassificationDedupPair;
import de.hpi.is.mdedup.boosting.classification.utils.ClassificationDedupPairScored;
import de.hpi.is.mdedup.boosting.classification.utils.ExperimentEvaluation;
import de.hpi.is.mdedup.utils.DedupPair;
import de.hpi.is.mdedup.utils.SimilaritiesPair;
import org.javatuples.Pair;
import org.javatuples.Triplet;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

/**
 *  This class implements the boosting step, which is the last step of the application process.
 *  It loads the pairs produced by the highest scored MDC, trains, and then tests a binary classifier, which in our
 *  case is Support Vector Machines.
 */
public class BoostingController extends MDedupController {

    protected String dataset;
    protected List<String> restDatasets;

    protected List<String> attributes;

    protected Classifier.ClassifierType classificationAlgorithm;

    protected String sqlGoldStandardSimilarities;
    protected String sqlMDedupPredictions;

    protected BoostingIO cio;
    protected MDCClassifiedPairsIO clpio;

    protected String description;

    protected Integer topk;
    protected Boolean enabledExpansionPhase;
    protected String featuresInRegression;

    public BoostingController(String dbUser, String dbPassword, String dbDriver, String dbConnection,
                              String dataset, List<String> restDatasets, List<String> attributes,
                              Classifier.ClassifierType classificationAlgorithm, String sqlGoldStandardSimilarities,
                              String sqlMDedupPredictions, String description, Integer topk,
                              Boolean enabledExpansionPhase, String featuresInRegression) {
        super(dbUser, dbPassword, dbDriver, dbConnection);
        this.dataset = dataset;
        this.restDatasets = restDatasets;
        this.attributes = attributes;
        this.classificationAlgorithm = classificationAlgorithm;

        this.sqlGoldStandardSimilarities = sqlGoldStandardSimilarities;
        this.sqlMDedupPredictions = sqlMDedupPredictions;
        this.description = description;

        this.topk = topk;
        this.enabledExpansionPhase = enabledExpansionPhase;
        this.featuresInRegression = featuresInRegression;

        cio = new BoostingIO(dbDriver, dbConnection, dbUser, dbPassword);
        clpio = new MDCClassifiedPairsIO(dbDriver, dbConnection, dbUser, dbPassword);

        importSimilarities(sqlGoldStandardSimilarities);
    }

    public void execute() {
        if (!sqlMDedupPredictions.equals("null")) {
            /* This is the main purpose of this class and acts as a boosting step that trains SVMs on top of the results
                produced by the highest scored MDC.
             */
            trainOnMDC();
        }
        else {
            /* This execution uses the gold standard of the tested dataset and only served as a baseline for us */
            trainOnGoldStandard();
        }
    }

    protected void trainOnMDC() {
        // Load the pairs as were classified by the highest scored MDC
        Map<Integer, ClassificationDedupPairScored> mdedupPredictions = cio.importClassificationPairsScored(sqlMDedupPredictions);

        // Keep only the duplicate pairs.
        Map<Integer, ClassificationDedupPairScored> pairsMDedupDPL = mdedupPredictions.entrySet().stream()
                .filter(dp -> dp.getValue().getClassifiedAs().equals(DedupPair.PairClass.DPL)).collect(Collectors.toMap(
                        Map.Entry::getKey, Map.Entry::getValue));

        // Keep only the non-duplicate pairs.
        Map<Integer, ClassificationDedupPairScored> pairsMDedupNDPL = mdedupPredictions.entrySet().stream()
                .filter(dp -> dp.getValue().getClassifiedAs().equals(DedupPair.PairClass.NDPL)).collect(Collectors.toMap(
                        Map.Entry::getKey, Map.Entry::getValue));

        // Convert into sets
        Set<Pair<String, String>> pairsMDedupDPLSet = pairsMDedupDPL.values().stream().map(dp -> new Pair<>(dp.getId1(), dp.getId2())).collect(Collectors.toSet());
        Set<Pair<String, String>> pairsMDedupNDPLSet = pairsMDedupNDPL.values().stream().map(dp -> new Pair<>(dp.getId1(), dp.getId2())).collect(Collectors.toSet());

        // {<id1, id2> -> similarities}
        Map<Pair<String, String>, SimilaritiesPair> pairToSim = sims.values().stream().collect(Collectors.toMap(
                sp -> new Pair<>(sp.getId1(),sp.getId2()),
                sp -> sp
        ));

        // Similarities as lists
        List<SimilaritiesPair> simsMDedupDPL = pairsMDedupDPLSet.stream().map(pairToSim::get).map(SimilaritiesPair::clone)
                .collect(Collectors.toList());
        simsMDedupDPL.forEach(sp -> sp.setPairClass(DedupPair.PairClass.DPL));
        List<SimilaritiesPair> simsMDedupNDPL = pairsMDedupNDPLSet.stream().map(pairToSim::get).map(SimilaritiesPair::clone)
                .collect(Collectors.toList());
        simsMDedupNDPL.forEach(sp -> sp.setPairClass(DedupPair.PairClass.NDPL));

        // In case the classes are imbalanced, undersample non-duplicates.
        if (simsMDedupDPL.size()*10 < simsMDedupNDPL.size()) {
            Collections.sort(simsMDedupNDPL, comparing(o -> o.getSims().get("total_mean")));
            simsMDedupNDPL = simsMDedupNDPL.subList(0, (int) (simsMDedupNDPL.size()*0.9));

            // If it's still more than 10 times the DPL, then randomly restrict it among the remaining ones.
            if (simsMDedupDPL.size()*10 < simsMDedupNDPL.size()) {
                Random r = new Random(1);
                Collections.shuffle(simsMDedupNDPL, r);
                simsMDedupNDPL = simsMDedupNDPL.subList(0, simsMDedupDPL.size()*10);
            }
        }

        // Gather all similarities.
        List<SimilaritiesPair> simsDPLandNDPL = new ArrayList<>();
        simsDPLandNDPL.addAll(simsMDedupDPL);
        simsDPLandNDPL.addAll(simsMDedupNDPL);

        // If similarities are too many, undersample them.
        if (sims.size() > 100000) {
            Random r = new Random(0L);
            Collections.shuffle(simsDPLandNDPL, r);
            simsDPLandNDPL = simsDPLandNDPL.subList(0, (int)(simsDPLandNDPL.size()*0.1));
        }

        Instant start = Instant.now();

        // Keep the attribute-similarity pairs. As they were produced by the MD Discovery tool.
        List<String> pairAttributes = new ArrayList<>(simsDPLandNDPL.get(0).getSims().keySet());
        Classifier classifier = Classifier.selectClassifier(classificationAlgorithm, 10, pairAttributes,
                simsDPLandNDPL, Classifier.SplitType.CROSS_VALIDATION);

        // We return the combinations of <parameter, score>.
        Map<String, ExperimentEvaluation> searchSpace = classifier.searchClassificationSpace();
        List<Map.Entry<String, ExperimentEvaluation>> searchSpaceList = new ArrayList<>(searchSpace.entrySet());

        /* We need some determinism of executions across different machines. Therefore to get the best F1 we also
           secondarily sort against the alphabetic order of the MDCs. */
        Collections.sort(searchSpaceList, (a, b) -> {

            double f1a = a.getValue().getClassificationEvaluation().calculateFMeasure(1.0);
            double f1b = b.getValue().getClassificationEvaluation().calculateFMeasure(1.0);

            int x = Double.compare(f1a, f1b);

            if (x != 0) {
                return x;
            }

            x = a.getValue().getClassificationEvaluation().calculatePrecision()
                    .compareTo(b.getValue().getClassificationEvaluation().calculatePrecision());

            if (x != 0) {
                return x;
            }

            x = a.getKey().compareTo(b.getKey());
            return x;

        });

        // We get the best parameters.
        Map.Entry<String, ExperimentEvaluation> bestParameters = searchSpaceList.get(searchSpaceList.size() - 1);

        Instant finishSearchSpace = Instant.now();

        long classificationSearchSpaceExecutionDuration = Duration.between(start, finishSearchSpace).toMillis();

        Triplet<double[][], int[], DedupPair[]> XYpairs = classifier.convertToDoubleArray(new ArrayList<>(sims.values()), classifier.getAttributesSMILE());

        // Using the best parameters, we now obtain the evaluation results on the full list of pairs.
        ExperimentEvaluation ee = classifier.execute(bestParameters.getKey(), true, XYpairs, null);

        Instant finishClassification = Instant.now();

        long classificationExecutionDuration = Duration.between(finishSearchSpace, finishClassification).toMillis();
        long classificationTotalExecutionDuration = Duration.between(start, finishClassification).toMillis();

        // Export results.
        cio.exportTable(new BoostingIO.PrepareStatementParametersCompetitors(
                dataset,
                ee,
                classificationAlgorithm,
                bestParameters.getKey(),
                "trained_on_mdedup" + description,
                Boolean.FALSE, // <---
                -1L,
                classificationExecutionDuration,
                classificationSearchSpaceExecutionDuration,
                classificationTotalExecutionDuration,
                topk,
                enabledExpansionPhase,
                featuresInRegression
        ));

        clpio.close();
        cio.close();
    }

    /**
     * This method is only used to compare our results with a binary classifier that is trained on top of similarity
     * measures. As discussed in the paper this is incomparable to our approach, but serves as a sanity and quality
     * check of our results.
     */
    protected void trainOnGoldStandard() {
        List<SimilaritiesPair> simsSampled = new ArrayList<>(sims.values());
        if (sims.size() > 100000) {
            Random r = new Random(0L);
            Collections.shuffle(simsSampled, r);
            simsSampled = simsSampled.subList(0, (int)(simsSampled.size()*0.1));
        }

        Instant start = Instant.now();
        Map.Entry<Integer, SimilaritiesPair> someEntry = new ArrayList<>(sims.entrySet()).get(0);
        List<String> pairAttributes = new ArrayList<>(someEntry.getValue().getSims().keySet());
        Classifier classifier = Classifier.selectClassifier(classificationAlgorithm, -1, pairAttributes, simsSampled, Classifier.SplitType.TRAIN_VALIDATION_TEST);

        Map<String, ExperimentEvaluation> searchSpace = classifier.searchClassificationSpace();
        Instant finishSearchSpace = Instant.now();
        long classificationSearchSpaceExecutionDuration = Duration.between(start, finishSearchSpace).toMillis();

        /* Executing classifier */
        Map.Entry<String, ExperimentEvaluation> bestParameters = searchSpace.entrySet().stream()
                .max(comparing(o -> o.getValue().getClassificationEvaluation().calculateFMeasure(1.0))).get();

        classifier = Classifier.selectClassifier(classificationAlgorithm, -1, pairAttributes, new ArrayList<>(sims.values()), Classifier.SplitType.TRAIN_VALIDATION_TEST);
        ExperimentEvaluation ee = classifier.execute(bestParameters.getKey(), true, null, Classifier.TrainingValidationTestType.TRAIN_VALIDATION_TO_TEST);

        Instant finishClassification = Instant.now();
        long classificationExecutionDuration = Duration.between(finishSearchSpace, finishClassification).toMillis();

        long classificationTotalExecutionDuration = Duration.between(start, finishClassification).toMillis();

        cio.exportTable(new BoostingIO.PrepareStatementParametersCompetitors(
                dataset,
                ee,
                classificationAlgorithm,
                bestParameters.getKey(),
                "trained_on_gs" + description,
                Boolean.FALSE, // <---
                -1L,
                classificationExecutionDuration,
                classificationSearchSpaceExecutionDuration,
                classificationTotalExecutionDuration,
                topk,
                enabledExpansionPhase,
                featuresInRegression
        ));

        List<ClassificationDedupPairScored> classifiedPairs = getClassificationPairs(new ArrayList<>(sims.values()), ee);

        clpio.exportTable(new MDCClassifiedPairsIO.PrepareStatementParametersClassifiedPairs(
                dataset,
                classifiedPairs,
                classificationAlgorithm.name(),
                bestParameters.getKey(),
                "trained_on_gs" + description,
                topk,
                enabledExpansionPhase,
                featuresInRegression
        ));

        clpio.close();
        cio.close();
    }

    private List<ClassificationDedupPairScored> getClassificationPairs(List<SimilaritiesPair> datasetPairs, ExperimentEvaluation er) {
        Map<Pair<String, String>, SimilaritiesPair> pairToSimilarities = new HashMap<>();
        for (SimilaritiesPair sp: datasetPairs) {
            pairToSimilarities.put(new Pair<>(sp.getId1(), sp.getId2()), sp);
        }
        List<ClassificationDedupPairScored> classifiedPairs = new ArrayList<>();

        List<List<ClassificationDedupPair>> classifiedLists = Arrays.asList(
                er.getClassificationEvaluationExtended().getTpList(),
                er.getClassificationEvaluationExtended().getFpList(),
                er.getClassificationEvaluationExtended().getFnList(),
                er.getClassificationEvaluationExtended().getTnList()
        );

        for (List<ClassificationDedupPair> pairsClassifiedType: classifiedLists) {
            for (ClassificationDedupPair cdp : pairsClassifiedType) {
                classifiedPairs.add(new ClassificationDedupPairScored(
                        cdp.getDataset(), cdp.getId(), cdp.getId1(), cdp.getId2(), cdp.getPairClass(),
                        cdp.getClassifiedAs(),
                        -1.0,
                        pairToSimilarities.get(new Pair<>(cdp.getId1(), cdp.getId2())).getSims().get("total_mean")));
            }
        }

        Collections.sort(classifiedPairs, comparing(ClassificationDedupPairScored::getClassificationScore)
                .thenComparing(ClassificationDedupPairScored::getSimilarityScore)
                .reversed());
        return classifiedPairs;
    }


}
