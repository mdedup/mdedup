package de.hpi.is.mdedup.boosting;

import de.hpi.is.mdedup.MDedupIO;
import de.hpi.is.mdedup.boosting.classification.Classifier;
import de.hpi.is.mdedup.boosting.classification.utils.ExperimentEvaluation;
import de.hpi.is.mdedup.exploration.MDedupExplorationController;
import de.hpi.is.mdedup.utils.ClassificationEvaluation;

import java.sql.*;
import java.util.*;

public class BoostingIO extends MDedupIO {

    public BoostingIO(String dbDriver, String dbConnection, String dbUser, String dbPassword) {
        super(dbDriver, dbConnection, dbUser, dbPassword);
    }

    public void createTable() throws SQLException {
        List<String> features = new ArrayList<>(MDedupExplorationController.getMDCsFeatures());
        Collections.sort(features);

        List<String> featuresWithType = new ArrayList<>();
        for (String feature : features) {
            featuresWithType.add(feature + " " + "VARCHAR(100)");
        }

        String sql = "CREATE TABLE IF NOT EXISTS `mdedup_boosting` (" +
            "`id` int(11) NOT NULL AUTO_INCREMENT," +
            "`dataset` varchar(45) DEFAULT NULL," +

            "`classification_algorithm` varchar(45) DEFAULT NULL," +
            "`classification_best_parameters` varchar(100) DEFAULT NULL," +
            "`description` varchar(100) DEFAULT NULL," +

            "`topk` int(11) DEFAULT NULL," +
            "`enabled_expansion_phase` boolean DEFAULT NULL," +
            "`features_in_regression` longtext DEFAULT NULL," +
            
            "`precision_` double DEFAULT NULL," +
            "`recall` double DEFAULT NULL," +
            "`f1` double DEFAULT NULL," +
            
            "`f05` double DEFAULT NULL," +
            "`f20` double DEFAULT NULL," +
            
            "`tp` int(11) DEFAULT NULL," +
            "`fn` int(11) DEFAULT NULL," +
            "`fp` int(11) DEFAULT NULL," +
            "`tn` int(11) DEFAULT NULL," +

            "`auc_roc` double DEFAULT NULL," +
            "`auc_pr` double DEFAULT NULL," +

            "`is_execution_interrupted` BOOLEAN DEFAULT NULL," +
            "`classification_max_duration` bigint(100) DEFAULT NULL," +
            "`classification_total_duration` bigint(100) DEFAULT NULL," +
            "`classificatin_search_space_duration` bigint(100) DEFAULT NULL," +
            "`classificatin_execution_duration` bigint(100) DEFAULT NULL," +

            " PRIMARY KEY (`id`)" +
            ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";

        System.out.println(sql);
        Statement stmt = conn.createStatement();
        stmt.setQueryTimeout(20);
        stmt.execute(sql);
        conn.commit();
    }


    public static class PrepareStatementParametersCompetitors extends PrepareStatementParameters {
        protected ExperimentEvaluation experiment;

        protected Classifier.ClassifierType classificationAlgorithm;
        protected String bestClassificationParameters;
        protected String description;

        protected Boolean isExecutionInterrupted;
        protected Long classificationMaxDuration;
        protected Long classificationExecutionDuration;
        protected Long classificationSearchSpaceDuration;
        protected Long classificationTotalDuration;

        protected Integer topk;
        protected Boolean enabledExpansionPhase;
        protected String featuresInRegression;

        protected List<Map<String, Object>> data;

        public PrepareStatementParametersCompetitors(String dataset, ExperimentEvaluation experiment,
                                                     Classifier.ClassifierType classificationAlgorithm,
                                                     String bestClassificationParameters, String description,
                                                     Boolean isExecutionInterrupted,
                                                     Long classificationMaxDuration,
                                                     Long classificationExecutionDuration,
                                                     Long classificationSearchSpaceDuration,
                                                     Long classificationTotalDuration,
                                                     Integer topk,
                                                     Boolean enabledExpansionPhase,
                                                     String featuresInRegression) {
            super(dataset, "mdedup_boosting");
            this.experiment = experiment;
            this.classificationAlgorithm = classificationAlgorithm;
            this.bestClassificationParameters = bestClassificationParameters;
            this.description = description;
            this.isExecutionInterrupted = isExecutionInterrupted;
            this.classificationMaxDuration = classificationMaxDuration;
            this.classificationExecutionDuration = classificationExecutionDuration;
            this.classificationSearchSpaceDuration = classificationSearchSpaceDuration;
            this.classificationTotalDuration = classificationTotalDuration;
            this.topk = topk;
            this.enabledExpansionPhase = enabledExpansionPhase;
            this.featuresInRegression = featuresInRegression;
        }

        public List<Map<String, Object>> prepareData() {
            Set<String> header = new HashSet<>();
            Map<String, Object> r = new HashMap<>();

            r.put("dataset", dataset);

            r.put("classification_algorithm", classificationAlgorithm.name());
            r.put("classification_best_parameters", bestClassificationParameters);
            r.put("description", description);

            ClassificationEvaluation ce = experiment.getClassificationEvaluation();

            r.put("precision_", ce.calculatePrecision());
            r.put("recall", ce.calculateRecall());
            r.put("f1", ce.calculateFMeasure(1.0));

            r.put("f05", ce.calculateFMeasure(0.5));
            r.put("f20", ce.calculateFMeasure(2.0));

            r.put("tp", ce.getTp());
            r.put("fn", ce.getFn());
            r.put("fp", ce.getFp());
            r.put("tn", ce.getTn());

            r.put("auc_roc", -1.0);
            r.put("auc_pr", -1.0);

            r.put("is_execution_interrupted", isExecutionInterrupted);
            r.put("classification_max_duration", classificationMaxDuration);
            r.put("classification_total_duration", classificationTotalDuration);
            r.put("classificatin_search_space_duration", classificationSearchSpaceDuration);
            r.put("classificatin_execution_duration", classificationExecutionDuration);

            r.put("topk", topk);
            r.put("enabled_expansion_phase", enabledExpansionPhase);
            r.put("features_in_regression", featuresInRegression);

            header.addAll(r.keySet());

            this.header = new ArrayList<>(header);
            this.header.sort(String::compareTo);

            data = new ArrayList<>(Arrays.asList(r));

            return data;
        }
    }


}
