package de.hpi.is.mdedup.boosting.classification.classifiers;

import de.hpi.is.mdedup.boosting.classification.Classifier;
import de.hpi.is.mdedup.boosting.classification.classifiers.smile.SVM;
import de.hpi.is.mdedup.utils.DedupPair;
import org.javatuples.Pair;
import smile.data.Attribute;
import smile.math.kernel.GaussianKernel;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SupportVectorMachines extends Classifier {

    public SupportVectorMachines(Pair<double[][], int[]> XY, DedupPair[] pairs, Attribute[] attributesSMILE,
                                 Integer folds, SplitType splitType) {
        super(XY, pairs, attributesSMILE, folds, splitType);
    }

    /**
     * https://www.csie.ntu.edu.tw/~cjlin/papers/guide/guide.pdf
     *
     * "We recommend a grid search on C and gamma... cross-validation..."
     *
     *  Loose grid search on C = 2^−5, 2^−3, ..., 2^15 and γ = 2^−15, 2^−13, ..., 2^3.
     *  Since gamma = 0.5/sigma^2  --> sigma = sqrt(0.5/gamma) and thus: sigma = 2^-2, 2^0, ...,2^7.
     *  Since we do a step by 2 search (loose search), we will start from -3, thus: sigma = 2^-3, 2^0, ...,2^7
     * @return
     */
    @Override
    public List<String> getHyperparameters() {
        List<String> hyperparameters = new ArrayList<>();

        // USE STEP afterclassification (see filter in for loop of parameters)
//        int Cbegin = -5, Cend = 15, gammaBegin = -15, gammaEnd = 3;
        int Cbegin = -5, Cend = 15, sigmaBegin = -3, sigmaEnd = 7;

        List<Double> valuesC = IntStream.range(Cbegin, Cend + 1)
//                .filter(i -> i % 2 != 0) // Step two
                .mapToDouble(i -> Math.pow(2.0, i)).boxed() // Powers of two
                .collect(Collectors.toList());
//
        List<Double> valuesSigma = IntStream.range(sigmaBegin, sigmaEnd + 1)
//                .filter(i -> i % 2 != 0) // Step two
                .mapToDouble(i -> Math.pow(2.0, i)).boxed() // Powers of two
                .collect(Collectors.toList());

        for (Double C : valuesC) {
//            for (Double gamma : valuesGamma) {
            for (Double sigma : valuesSigma) {
                hyperparameters.add("C@" + C + "#" + "sigma@" + sigma);
            }
        }

        return hyperparameters;
    }

    @Override
    public SVM<double[]> getClassifierTrained(Map<String, String> parameters, double[][] X, int[] Y) {
        Double C = Double.valueOf(parameters.get("C"));
        Double sigma = Double.valueOf(parameters.get("sigma"));
        SVM<double[]> svm = new SVM<>(new GaussianKernel(sigma), C);
        svm.learn(X, Y);
        svm.finish();
        return svm;
    }

//    public static Double gammaToSigma(Double gamma) {
//        return Math.sqrt(0.5 / gamma); // this.gamma = 0.5 / (sigma * sigma); --> sigma = sqrt(0.5 / gamma)
//    }
}
