package de.hpi.is.mdedup.boosting.classification;

import de.hpi.is.mdedup.boosting.classification.classifiers.SupportVectorMachines;
import de.hpi.is.mdedup.boosting.classification.utils.*;
import de.hpi.is.mdedup.boosting.classification.classifiers.smile.SVM;
import de.hpi.is.mdedup.utils.ClassificationEvaluation;
import de.hpi.is.mdedup.utils.DedupPair;
import de.hpi.is.mdedup.utils.SimilaritiesPair;
import org.apache.commons.lang3.ArrayUtils;
import org.javatuples.Pair;
import org.javatuples.Triplet;
import smile.data.Attribute;
import smile.data.NumericAttribute;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

public abstract class Classifier {
    protected Pair<double[][], int[]> XY;
    protected DedupPair[] pairs;
    protected Integer folds;
    protected Attribute[] attributesSMILE;

    protected SplitType splitType;

    protected CrossValidationStandardSeedPermutation cv = null;

    protected Map<Integer, double[][]> foldToXtrain = null, foldToXtest = null;
    protected Map<Integer, int[]> foldToYtrain = null, foldToYtest = null;

    protected double[][] Xtrain, Xvalidation, Xtest;
    protected int[] Ytrain, Yvalidation, Ytest;

    public abstract List<String> getHyperparameters();
//    public abstract smile.classification.Classifier<double[]> getClassifierTrained(Map<String, String> parameters, double[][] X, int[] Y);
    public abstract SVM<double[]> getClassifierTrained(Map<String, String> parameters, double[][] X, int[] Y);

    public static enum ClassifierType {
        THRESHOLD, RANDOM_FOREST, SUPPORT_VECTOR_MACHINES, LOGISTIC_REGRESSION, NAIVE_BAYES, K_NEAREST_NEIGHBORS
    }

    public static enum SplitType {
        CROSS_VALIDATION, TRAIN_VALIDATION_TEST
    }

    public static enum TrainingValidationTestType {
        TRAIN_TO_VALIDATION, TRAIN_VALIDATION_TO_TEST
    }

    public Classifier(Pair<double[][], int[]> XY, DedupPair[] pairs, Attribute[] attributesSMILE, Integer folds, SplitType splitType) {
        this.XY = XY;
        this.pairs = pairs;
        this.folds = folds;
        this.attributesSMILE = attributesSMILE;
        this.splitType = splitType;

        if (splitType == SplitType.CROSS_VALIDATION) {
            cv = new CrossValidationStandardSeedPermutation(XY.getValue1(), folds);
            foldToXtrain = new HashMap<>();
            foldToXtest = new HashMap<>();
            foldToYtrain = new HashMap<>();
            foldToYtest = new HashMap<>();

            for (int k = 0; k < folds; ++k) {
                // https://github.com/haifengl/smile
                double[][] Xtrain = smile.math.Math.slice(XY.getValue0(), cv.train[k]);
                double[][] Xtest = smile.math.Math.slice(XY.getValue0(), cv.test[k]);
                int[] Ytrain = smile.math.Math.slice(XY.getValue1(), cv.train[k]);
                int[] Ytest = smile.math.Math.slice(XY.getValue1(), cv.test[k]);

                foldToXtrain.put(k, Xtrain);
                foldToXtest.put(k, Xtest);
                foldToYtrain.put(k, Ytrain);
                foldToYtest.put(k, Ytest);
            }
        }
        else if (splitType == SplitType.TRAIN_VALIDATION_TEST) {
            SampleDataset sd = new SampleDataset(XY.getValue0(), XY.getValue1(), pairs);
//            Map<String, int[]> setSplits = sd.stratifiedSampleTrainValidationTest(0.64, 0.16, 0.2);
            Map<String, int[]> setSplits = sd.stratifiedSampleTrainValidationTest(0.60, 0.20, 0.20);
            Xtrain = smile.math.Math.slice(XY.getValue0(), setSplits.get("train"));
            Ytrain = smile.math.Math.slice(XY.getValue1(), setSplits.get("train"));
            Xvalidation = smile.math.Math.slice(XY.getValue0(), setSplits.get("validation"));
            Yvalidation = smile.math.Math.slice(XY.getValue1(), setSplits.get("validation"));
            Xtest = smile.math.Math.slice(XY.getValue0(), setSplits.get("test"));
            Ytest = smile.math.Math.slice(XY.getValue1(), setSplits.get("test"));
        }
    }

    public ExperimentEvaluation execute(String parameters, Boolean withPairs, Triplet<double[][], int[], DedupPair[]>
            XYtestPairs, TrainingValidationTestType trainingValidationTestType){
        Map<String, String> m = new HashMap<>();
        if (!parameters.trim().equals("")) {
            String[] parts = parameters.split("#");
            for (String part: parts) {
                String[] kv = part.split("@");
                m.put(kv[0], kv[1]);
            }
        }
        return execute(m, withPairs, XYtestPairs, trainingValidationTestType);
    }

    public ExperimentEvaluation execute(Map<String, String> parameters, Boolean withPairs,
                                        Triplet<double[][], int[], DedupPair[]> XYtestPairs,
                                        TrainingValidationTestType trainingValidationTestType) {
        long start = System.nanoTime();
        int tp = 0, fp = 0, fn = 0, tn = 0;
        ArrayList<ClassificationDedupPair>
                tpList = new ArrayList<>(),
                fpList = new ArrayList<>(),
                fnList = new ArrayList<>(),
                tnList = new ArrayList<>();

        if (XYtestPairs == null) {
//            StratifiedCrossValidationStandardSeedPermutation cv = new StratifiedCrossValidationStandardSeedPermutation(XY.getValue1(), folds);
            if (splitType == SplitType.CROSS_VALIDATION) {
                for (int k = 0; k < folds; ++k) {
                    // https://github.com/haifengl/smile
                    double[][] Xtrain = foldToXtrain.get(k); // smile.math.Math.slice(XY.getValue0(), cv.train[k]);
                    double[][] Xtest = foldToXtest.get(k); // smile.math.Math.slice(XY.getValue0(), cv.test[k]);
                    int[] Ytrain = foldToYtrain.get(k); // smile.math.Math.slice(XY.getValue1(), cv.train[k]);
                    int[] Ytest = foldToYtest.get(k); // smile.math.Math.slice(XY.getValue1(), cv.test[k]);

                    smile.classification.Classifier<double[]> classifier = getClassifierTrained(parameters, Xtrain, Ytrain);
                    int[] predictions = classifier.predict(Xtest);
                    classifier = null;
                    if (withPairs) {
//                        classificationParametersWithPairs(tpList, fpList, fnList, tnList, cv.test[k], Xtest, Ytest, predictions, pairs);
//                        classificationParametersWithPairs(tpList, fpList, fnList, tnList, XtestToUse, YtestToUse, predictions, pairs);
                        classificationParametersWithPairs(tpList, fpList, fnList, tnList, Xtest, Ytest, predictions, pairs);

                    } else {
                        for (int i = 0; i < Ytest.length; ++i) {
                            int pred = predictions[i];
                            int y = Ytest[i];

                            if (y == DedupPair.PairClass.DPL.ordinal() && pred == DedupPair.PairClass.DPL.ordinal())
                                ++tp;
                            else if(y == DedupPair.PairClass.DPL.ordinal() && pred == DedupPair.PairClass.NDPL.ordinal())
                                ++fn;
                            else if(y == DedupPair.PairClass.NDPL.ordinal() && pred == DedupPair.PairClass.DPL.ordinal())
                                ++fp;
                            else if(y == DedupPair.PairClass.NDPL.ordinal() && pred == DedupPair.PairClass.NDPL.ordinal())
                                ++tn;
                        }
                    }
                    System.gc();
                    System.runFinalization();
                }
            } else if (splitType == SplitType.TRAIN_VALIDATION_TEST) {
                double[][] XtrainToUse = null, XtestToUse = null;
                int[] YtrainToUse = null, YtestToUse = null;
                switch (trainingValidationTestType) {
                    case TRAIN_TO_VALIDATION:
                        XtrainToUse = Xtrain;
                        XtestToUse = Xvalidation;
                        YtrainToUse = Ytrain;
                        YtestToUse = Yvalidation;
                        break;
                    case TRAIN_VALIDATION_TO_TEST:
                        XtrainToUse = ArrayUtils.addAll(Xtrain, Xvalidation);
                        XtestToUse = Xtest;
                        YtrainToUse = ArrayUtils.addAll(Ytrain, Yvalidation);
                        YtestToUse = Ytest;
                        break;
                }
                smile.classification.Classifier<double[]> classifier = getClassifierTrained(parameters, XtrainToUse, YtrainToUse);
                int[] predictions = classifier.predict(XtestToUse);
                classifier = null;
                if (withPairs) {
                    classificationParametersWithPairs(tpList, fpList, fnList, tnList, XtestToUse, YtestToUse, predictions, pairs);
                } else {
                    for (int i = 0; i < YtestToUse.length; ++i) {
                        int pred = predictions[i];
                        int y = YtestToUse[i];

                        if (y == DedupPair.PairClass.DPL.ordinal() && pred == DedupPair.PairClass.DPL.ordinal())
                            ++tp;
                        else if(y == DedupPair.PairClass.DPL.ordinal() && pred == DedupPair.PairClass.NDPL.ordinal())
                            ++fn;
                        else if(y == DedupPair.PairClass.NDPL.ordinal() && pred == DedupPair.PairClass.DPL.ordinal())
                            ++fp;
                        else if(y == DedupPair.PairClass.NDPL.ordinal() && pred == DedupPair.PairClass.NDPL.ordinal())
                            ++tn;
                    }
                }
                System.gc();
                System.runFinalization();
            }

            if (withPairs) {
                return new ExperimentEvaluation(null,
                        new ClassificationEvaluation.ClassificationEvaluationExtended(tpList, fpList, tnList, fnList),
                        System.nanoTime() - start);
            } else {
                return new ExperimentEvaluation(new ClassificationEvaluation(tp, fp, tn, fn), null,
                        System.nanoTime() - start);
            }
        } else {
            double[][] Xtrain = this.XY.getValue0();
            double[][] Xtest = XYtestPairs.getValue0();
            int[] Ytrain = this.XY.getValue1();
            int[] Ytest = XYtestPairs.getValue1();

            smile.classification.Classifier<double[]> classifier = getClassifierTrained(parameters, Xtrain, Ytrain);

//            int[] ints = IntStream.range(0, Xtest.length).toArray();
            int[] predictions = classifier.predict(Xtest);
            classificationParametersWithPairs(tpList, fpList, fnList, tnList, Xtest, Ytest, predictions, XYtestPairs.getValue2());

            classifier = null;
            System.gc();
            System.runFinalization();

            return new ExperimentEvaluation(null,
                    new ClassificationEvaluation.ClassificationEvaluationExtended(tpList, fpList, tnList, fnList),
                    System.nanoTime() - start);
        }
    }

//    public ExperimentEvaluation execute(Map<String, String> parameters) {
//        return execute(parameters, false, null);
//    }

    public static Attribute[] extractAttributesSMILE(List<String> similarityAttributes) {
        List<String> attributeSimilarities = similarityAttributes.stream().filter(k ->
                k.endsWith("-Levenshtein") || k.endsWith("-Equal") || k.endsWith("-Jaccard"))
                .collect(Collectors.toList());
        Attribute[] attributesSMILE = new Attribute[attributeSimilarities.size()];
        for (int i = 0; i < attributeSimilarities.size(); ++i) {
            attributesSMILE[i] = new NumericAttribute(attributeSimilarities.get(i)); // StringAttribute
        }
        return attributesSMILE;
    }


    private void classificationParametersWithPairs(ArrayList<ClassificationDedupPair> tp,
                                                     ArrayList<ClassificationDedupPair> fp,
                                                     ArrayList<ClassificationDedupPair> fn,
                                                     ArrayList<ClassificationDedupPair> tn,
                                                     double[][] xtest, int[] ytest,
                                                     int[] predictions,
                                                     DedupPair[] pairs) {
        for (int i = 0; i < xtest.length; ++i) {
            int pred = predictions[i];
            int y = ytest[i];
            DedupPair dp = pairs[i];
            if (y == DedupPair.PairClass.DPL.ordinal() && pred == DedupPair.PairClass.DPL.ordinal()) {
                tp.add(new ClassificationDedupPair(dp.getDataset(), dp.getId(), dp.getId1(), dp.getId2(), dp.getPairClass(), DedupPair.PairClass.DPL));
            } else if (y == DedupPair.PairClass.DPL.ordinal() && pred == DedupPair.PairClass.NDPL.ordinal()) {
                fn.add(new ClassificationDedupPair(dp.getDataset(), dp.getId(), dp.getId1(), dp.getId2(), dp.getPairClass(), DedupPair.PairClass.NDPL));
            } else if (y == DedupPair.PairClass.NDPL.ordinal() && pred == DedupPair.PairClass.NDPL.ordinal()) {
                tn.add(new ClassificationDedupPair(dp.getDataset(), dp.getId(), dp.getId1(), dp.getId2(), dp.getPairClass(), DedupPair.PairClass.NDPL));
            } else if (y == DedupPair.PairClass.NDPL.ordinal() && pred == DedupPair.PairClass.DPL.ordinal()) {
                fp.add(new ClassificationDedupPair(dp.getDataset(), dp.getId(), dp.getId1(), dp.getId2(), dp.getPairClass(), DedupPair.PairClass.DPL));
            }
        }
    }


    public static Classifier selectClassifier(ClassifierType classificationAlgorithm, Attribute[] attributesSMILE,
                                              Triplet<double[][], int[], DedupPair[]> XYpairs, Integer folds, SplitType splitType) {

//        if (classificationAlgorithm == ClassifierType.THRESHOLD) {
//            return new ThresholdClassifier(new Pair<>(XYpairs.getValue0(), XYpairs.getValue1()), XYpairs.getValue2(), 10, attributesSMILE);
//        } else if (classificationAlgorithm == ClassifierType.SUPPORT_VECTOR_MACHINES) {
            return new SupportVectorMachines(new Pair<>(XYpairs.getValue0(), XYpairs.getValue1()),
                    XYpairs.getValue2(), attributesSMILE, folds, splitType);
//        } else if (classificationAlgorithm == ClassifierType.RANDOM_FOREST) {
//            return new RandomForestClassifier(new Pair<>(XYpairs.getValue0(), XYpairs.getValue1()), XYpairs.getValue2(), 10, attributesSMILE);
//        } else if (classificationAlgorithm == ClassifierType.K_NEAREST_NEIGHBORS) {
//            return new KNNClassifier(new Pair<>(XYpairs.getValue0(), XYpairs.getValue1()), XYpairs.getValue2(), 10, attributesSMILE);
//        } else if (classificationAlgorithm == ClassifierType.LOGISTIC_REGRESSION) {
//            return new LogisticRegressionClassifier(new Pair<>(XYpairs.getValue0(), XYpairs.getValue1()), XYpairs.getValue2(), 10, attributesSMILE);
//        } else if (classificationAlgorithm == ClassifierType.NAIVE_BAYES) {
//            return new NaiveBayesianClassifier(new Pair<>(XYpairs.getValue0(), XYpairs.getValue1()), XYpairs.getValue2(), 10, attributesSMILE);
//        }
//        return null;
    }

    public static Classifier selectClassifier(ClassifierType classificationAlgorithm, Integer folds, List<String> pairAttributes,
                                              List<SimilaritiesPair> pairs, SplitType splitType) {
        Attribute[] attributesSMILE = extractAttributesSMILE(pairAttributes);
        Triplet<double[][], int[], DedupPair[]> XYpairs = convertToDoubleArray(pairs, attributesSMILE);
        return selectClassifier(classificationAlgorithm, attributesSMILE, XYpairs, folds, splitType);
    }


    public static Triplet<double[][], int[], DedupPair[]> convertToDoubleArray(List<SimilaritiesPair> pairsSimilarities,
                                                                        Attribute[] attributesSMILE) {

        double[][] X = new double[pairsSimilarities.size()][attributesSMILE.length];
        int[] Y = new int[pairsSimilarities.size()];
        DedupPair[] pairs = new DedupPair[pairsSimilarities.size()];

        List<DedupPair> keys = new ArrayList<>();
        pairsSimilarities.forEach(x -> {
            keys.add(new DedupPair(x.getDataset(), x.getId(), x.getId1(), x.getId2(), x.getPairClass()));
        });

        for (int i = 0; i < pairsSimilarities.size(); i++) {
            SimilaritiesPair sp = pairsSimilarities.get(i);
            X[i] = new double[attributesSMILE.length];
            DedupPair p = keys.get(i);
            Map<String, Double> similarities = sp.getSims();
            for (int j = 0; j < attributesSMILE.length; ++j) {
                Double sim = similarities.get(attributesSMILE[j].getName());
                X[i][j] = sim;
            }
            Y[i] = p.getPairClass().ordinal();
            pairs[i] = p.clone();
        }

        return new Triplet<>(X, Y, pairs);
    }

    public Map<String, ExperimentEvaluation> searchClassificationSpace() {
        List<String> hyperparameters = getHyperparameters();

        // Controlling number of threads
//        ForkJoinPool forkJoinPool = new ForkJoinPool(4);
        ForkJoinPool forkJoinPool = new ForkJoinPool(4);
        try {
            Map<String, ExperimentEvaluation> result = forkJoinPool.submit(() ->
                    //parallel task here, for example
                    hyperparameters.parallelStream().map(parameters -> {
                        System.out.println("Hyperparameter tuning, started executing: " + parameters);
                        ExperimentEvaluation er = execute(parameters, false, null, TrainingValidationTestType.TRAIN_TO_VALIDATION);
                        System.out.println("Hyperparameter tuning, finished executing: " + parameters);
                        return new Pair<>(parameters, er);
                    }).collect(Collectors.toMap(
                            Pair::getValue0,
                            Pair::getValue1
                    ))).get();
            return result;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
//        return hyperparameters.parallelStream().map(parameters -> {
//            System.out.println("Hyperparameter tuning, started executing: " + parameters);
//            ExperimentEvaluation er = execute(parameters, false, null);
//            System.out.println("Hyperparameter tuning, finished executing: " + parameters);
//            return new Pair<>(parameters, er);
//        }).collect(Collectors.toMap(
//                Pair::getValue0,
//                Pair::getValue1
//        ));
    }

    public Attribute[] getAttributesSMILE() {
        return attributesSMILE;
    }

    public Pair<double[][], int[]> getXY() {
        return XY;
    }

    public void setXY(Pair<double[][], int[]> XY) {
        this.XY = XY;
    }

    public DedupPair[] getPairs() {
        return pairs;
    }

    public void setPairs(DedupPair[] pairs) {
        this.pairs = pairs;
    }

//    public Integer getFolds() {
//        return folds;
//    }
//
//    public void setFolds(Integer folds) {
//        this.folds = folds;
//    }
}
