package de.hpi.is.mdedup.utils;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class SimpleArgsParser {

    final static Logger logger = Logger.getLogger(SimpleArgsParser.class);

    public static Map<String, String> parse(String[] args) {
        Map<String, String> m = new HashMap<>();
        if (args.length == 0) {
            System.out.println("No arguments provided. Bye!");
            System.exit(-1);
        }
        for(String arg: args) {
            String[] toks = arg.split("==");
            m.put(toks[0], toks[1]);
        }
        return m;
    }
}
