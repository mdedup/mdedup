package de.hpi.is.mdedup.utils;

import java.util.HashSet;

public class ClassificationSets {
    HashSet<Integer> tp;
    HashSet<Integer> fp;
    HashSet<Integer> fn;
    HashSet<Integer> tn;

    public ClassificationSets(HashSet<Integer> tp, HashSet<Integer> fp, HashSet<Integer> fn, HashSet<Integer> tn) {
        this.tp = tp;
        this.fp = fp;
        this.fn = fn;
        this.tn = tn;
    }

    public HashSet<Integer> getTp() {
        return tp;
    }

    public HashSet<Integer> getFp() {
        return fp;
    }

    public HashSet<Integer> getFn() {
        return fn;
    }

    public HashSet<Integer> getTn() {
        return tn;
    }
}
