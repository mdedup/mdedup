package de.hpi.is.mdedup.utils.entities;

import it.unimi.dsi.fastutil.Hash;

import java.util.*;

public class MDC implements Comparable<MDC>{
    protected String id;
    protected Set<String> mdsIDs;
    protected Map<String, String> fields;

    public MDC(Map<String, String> fields) {
        this.fields = fields;
        this.mdsIDs = new HashSet<>(Arrays.asList(fields.get("ids").split("_")));
        this.id = generateID();
    }

    public MDC(Set<String> mdsIDs) {
        this.mdsIDs = mdsIDs;
        this.id = generateID();
        this.fields = new HashMap<>();
    }

    public MDC(List<String> mdsIDs) {
        this(new HashSet<>(mdsIDs));
    }

    public MDC(String id, Set<String> mdsIDs) {
        this.id = id;
        this.mdsIDs = mdsIDs;
        this.fields = new HashMap<>();
    }

    public String getId() {
        return id;
    }

    public Set<String> getMDsIDs() {
        return mdsIDs;
    }

    public List<String> getMDsIDsSorted() {
        List<String> ids = new ArrayList<>(mdsIDs);
        ids.sort(Comparator.comparing(Integer::valueOf));
        return ids;
    }

    public String generateID() {
        return String.join("_", getMDsIDsSorted());
    }

    @Override
    public int compareTo(MDC o) {
        Comparator<MDC> cmp = Comparator.comparing(MDC::getId)
                .thenComparing((o1, o2) -> {
                    Set<String> md1 = o1.mdsIDs;
                    Set<String> md2 = o2.mdsIDs;

                    Integer x = md1.containsAll(md2) && md2.containsAll(md1) ? 0 : 1;
                    if (x != 0) {
                        return Integer.compare(md1.size(), md2.size());
                    }
                    return x;
                });
        return cmp.compare(this, o);
    }

    protected MDC clone()  {
        return new MDC(id, new HashSet<>(mdsIDs));
    }

    @Override
    public int hashCode() {
        return mdsIDs.hashCode();
    }

    @Override
    public String toString() {
        return generateID();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof MDC)) {
            return false;
        }
        MDC other = (MDC) o;
        return mdsIDs.containsAll(other.mdsIDs) && other.mdsIDs.containsAll(mdsIDs);
    }

    public Map<String, String> getFields() {
        return fields;
    }

    public void setFields(Map<String, String> fields) {
        this.fields = fields;
    }
}
