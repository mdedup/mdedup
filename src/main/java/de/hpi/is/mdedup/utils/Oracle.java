package de.hpi.is.mdedup.utils;

import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.entities.MDC;
import de.hpi.is.mdedup.utils.entities.MDCondition;
import org.javatuples.Triplet;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

public abstract class Oracle {

    public abstract void batchCalculateScoresForMDCs(List<MDC> mdcList);
    public abstract Double fetchScoreMDC(MDC mdc);

    public abstract void batchCalculateEvaluationsForMDCs(List<MDC> mdcList);
    public abstract ClassificationEvaluation fetchEvaluationMDC(MDC mdc);

    public static List<SimilaritiesPair> getPairsSatisfiedOfMDC(String mdcID, Map<String, MD> mds, Map<String, MDC> mdcs,
                                                         Map<Integer, SimilaritiesPair> sims) {
        List<SimilaritiesPair> pairsSatisfiedByMDC = new ArrayList<>();

        MDC mdc = mdcs.get(mdcID);
        Set<String> mdsParticipating = new HashSet<>();
        mdsParticipating.addAll(mdc.getMDsIDs());

        Set<MDCondition> columnMatches = new ConcurrentSkipListSet<>();
        mdsParticipating.parallelStream().forEach(mdID -> {
            columnMatches.addAll(mds.get(mdID).getMDSide(MD.MDSide.LHS));
        });

        List<Triplet<SimilaritiesPair, String, String>> classifications = sims.entrySet().parallelStream().map(pairSimilarities -> {
            Map<String, Boolean> mdcsValidities = isPairSatisfiedByMDC(Arrays.asList(mdc), MD.MDSide.LHS,
                    mdsParticipating, columnMatches,
                    pairSimilarities, mds);

            String pairClass = pairSimilarities.getValue().getPairClass().name().toLowerCase();
            Boolean valid = mdcsValidities.get(mdc.getId());

            if (pairClass.equals("dpl") && valid) {
                return Collections.singletonList(new Triplet<>(pairSimilarities.getValue(), mdcID, "tp"));
            } else if (pairClass.equals("dpl") && !valid) {
                return Collections.singletonList(new Triplet<>(pairSimilarities.getValue(), mdcID, "fn"));
            } else if (pairClass.equals("ndpl") && valid) {
                return Collections.singletonList(new Triplet<>(pairSimilarities.getValue(), mdcID, "fp"));
            } else if (pairClass.equals("ndpl") && !valid) {
                return Collections.singletonList(new Triplet<>(pairSimilarities.getValue(), mdcID, "tn"));
            } else {
                return null;
            }
        }).filter(Objects::nonNull).flatMap(Collection::stream).collect(Collectors.toList());

        for (Triplet<SimilaritiesPair, String, String> classification : classifications) {
            SimilaritiesPair sp = classification.getValue0();
            String classificationLabel = classification.getValue2();
            switch (classificationLabel) {
                case "tp":
                case "fp":
                    //TODO add to list
                    pairsSatisfiedByMDC.add(sp);
                    break;
                case "fn":
                case "tn":
                    // nothing
                    break;
            }
        }

        return pairsSatisfiedByMDC;
    }

    protected static Map<String, Boolean> isPairSatisfiedByMDC(List<MDC> mdcList, MD.MDSide mdside,
                                                             Set<String> mdsParticipating,
                                                             Set<MDCondition> columnMatches,
                                                             Map.Entry<Integer, SimilaritiesPair> pairSimilarities,
                                                             Map<String, MD> mds) {
        /* Step 2. Calculate validity of column matches */
        Map<String, Boolean> mdcndValidity = new HashMap<>();
        columnMatches.forEach(mdcnd -> {
            Boolean valid = Boolean.TRUE;
            Map<String, Double> pairSims = pairSimilarities.getValue().getSims();
            String attrMsr = mdcnd.getAttribute() + "-" + mdcnd.getMeasure();
            if (pairSims.containsKey(attrMsr)) {
                if (pairSims.get(attrMsr) < mdcnd.getThreshold()) {
                    valid = Boolean.FALSE;
                }
            }
            mdcndValidity.put(mdcnd.toString(), valid);
        });

        /* Step 3. Calculate validity of MDs */
        Map<String, Boolean> mdValidity = new HashMap<>();
        mdsParticipating.forEach(mdID -> {
            Boolean valid = Boolean.TRUE;
            MD md = mds.get(mdID);
            for (MDCondition mdcnd : md.getMDSide(mdside)) {
                if (!mdcndValidity.get(mdcnd.toString())) {
                    valid = Boolean.FALSE;
                    break;
                }
            }
            mdValidity.put(md.getEncodingBySide(mdside), valid);
        });

        Map<String, Boolean> mdcValidity = new HashMap<>();
        mdcList.forEach(mdc -> {
            Boolean valid = Boolean.FALSE;
            for (String mdID : mdc.getMDsIDs()) {
                if (mdValidity.get(mds.get(mdID).getEncodingBySide(mdside))) {
                    valid = Boolean.TRUE;
                    break;
                }
            }
            mdcValidity.put(mdc.getId(), valid);
        });

        return mdcValidity;
    }
}
