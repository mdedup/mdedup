package de.hpi.is.mdedup.utils.classifiedpairs;

import de.hpi.is.mdedup.MDedupIO;
import de.hpi.is.mdedup.boosting.classification.utils.ClassificationDedupPairScored;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Collectors;

public class MDCClassifiedPairsIO extends MDedupIO {

    public MDCClassifiedPairsIO(String dbDriver, String dbConnection, String dbUser, String dbPassword) {
        super(dbDriver, dbConnection, dbUser, dbPassword);
    }

    public MDCClassifiedPairsIO(Connection conn) {
        super(conn);
    }

    public void createTable() throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS `mdedup_classificationpairs` (" +
            "    `id` int(11) NOT NULL AUTO_INCREMENT," +
            "    `dataset` varchar(45) DEFAULT NULL," +
            "    `classification_algorithm` varchar(45) DEFAULT NULL," +
            "    `classification_best_parameters` longtext DEFAULT NULL," +
            "    `description` varchar(45) DEFAULT NULL," +
            "    `classification_score` double DEFAULT NULL," +
            "    `similarity_score` double DEFAULT NULL," +
            "    `pair_id` varchar(45) DEFAULT NULL," +
            "    `pair_id1` varchar(45) DEFAULT NULL," +
            "    `pair_id2` varchar(45) DEFAULT NULL," +
            "    `pair_class` varchar(45) DEFAULT NULL," +
            "    `pair_classified_class` varchar(45) DEFAULT NULL," +
            "    `pair_classification_label` varchar(45) DEFAULT NULL," +

            "`topk` int(11) DEFAULT NULL," +
            "`enabled_expansion_phase` boolean DEFAULT NULL," +
            "`features_in_regression` longtext DEFAULT NULL," +

            "    PRIMARY KEY (`id`)" +
            "  ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";
        System.out.println(sql);
        Statement stmt = conn.createStatement();
        stmt.setQueryTimeout(20);
        stmt.execute(sql);
        conn.commit();
    }

    public static class PrepareStatementParametersClassifiedPairs extends PrepareStatementParameters {
        protected List<ClassificationDedupPairScored> pairsScored;

        protected String classificationAlgorithm;
        protected String bestClassificationParameters;
        protected String description;

        protected List<Map<String, Object>> data;

        protected Integer topk;
        protected Boolean enabledExpansionPhase;
        protected String featuresInRegression;

        public PrepareStatementParametersClassifiedPairs(String dataset, List<ClassificationDedupPairScored> pairsScored,
                                                     String classificationAlgorithm,
                                                     String bestClassificationParameters, String description,
                                                     Integer topk, Boolean enabledExpansionPhase,
                                                     String featuresInRegression) {
            super(dataset, "mdedup_classificationpairs");
            this.pairsScored = pairsScored;
            this.classificationAlgorithm = classificationAlgorithm;
            this.bestClassificationParameters = bestClassificationParameters;
            this.description = description;

            this.topk = topk;
            this.enabledExpansionPhase = enabledExpansionPhase;
            this.featuresInRegression = featuresInRegression;
        }

        public List<Map<String, Object>> prepareData() {
            Set<String> header = new HashSet<>();
            data = pairsScored.stream().map(p -> {
                Map<String, Object> r = new HashMap<>();

                r.put("dataset", dataset);
                r.put("classification_algorithm", classificationAlgorithm);
                r.put("classification_best_parameters", bestClassificationParameters);
                r.put("description", description);
                r.put("classification_score", p.getClassificationScore().toString());
                r.put("similarity_score", p.getSimilarityScore().toString());
                r.put("pair_id", p.getId());
                r.put("pair_id1", p.getId1());
                r.put("pair_id2", p.getId2());
                r.put("pair_class", p.getPairClass().name());
                r.put("pair_classified_class", p.getClassifiedAs().name());
                r.put("pair_classification_label", p.getClassificationLabel().name());

                r.put("topk", topk);
                r.put("enabled_expansion_phase", enabledExpansionPhase);
                r.put("features_in_regression", featuresInRegression);

                if (header.size() == 0) {
                    header.addAll(r.keySet());
                }

                return r;
            }).collect(Collectors.toList());

            this.header = new ArrayList<>(header);
            this.header.sort(String::compareTo);

            return data;
        }
    }

}
