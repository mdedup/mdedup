package de.hpi.is.mdedup.utils.entities;

import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

public class MD implements Comparable<MD>{
    protected String id;
    protected String dataset;

    protected String lhsEncoded;
    protected List<MDCondition> lhs;

    protected String rhsEncoded;
    protected List<MDCondition> rhs;

    @Override
    public int compareTo(MD o) {
        Comparator<MD> mdComparator =
//                Comparator.comparing(MDCondition::getMdID)
                Comparator.comparing(MD::getDataset)
//                .thenComparing(MDCondition::getAttribute)
                        .thenComparing(MD::getLhsEncoded)
                        .thenComparing(MD::getRhsEncoded);
        return mdComparator.compare(this, o);
    }

    public static enum MDSide {
        LHS, RHS, BOTH
    }

    public static enum MDMergeStrategyAttributesMeasures {
        INTERSECTION, THRESHOLD_BASED, UNION;
    }
    public static MDMergeStrategyAttributesMeasures mdMergeAttrMsr = MDMergeStrategyAttributesMeasures.UNION;
    public static enum MDMergeStrategyThresholds {
        MIN, MAX, MEDIAN;
    }
    public static MDMergeStrategyThresholds mdMergeThr = MDMergeStrategyThresholds.MAX;

    public MD(String id, String dataset, String lhsEncoded, String rhsEncoded) {
        this.id = id;
        this.dataset = dataset;

        this.lhsEncoded = lhsEncoded;
        this.rhsEncoded = rhsEncoded;

        this.lhs = decode(id, lhsEncoded);
        this.rhs = decode(id, rhsEncoded);
    }

    public MD(String id, String dataset, List<MDCondition> lhs, List<MDCondition> rhs) {
        this.id = id;
        this.dataset = dataset;

        this.lhs = lhs;
        this.rhs = rhs;

        this.lhsEncoded = encode(lhs);
        this.rhsEncoded = encode(rhs);
    }


//    private static List<MDCondition> mergeConditions(String mergedID, int numberOriginalMDs, List<MDCondition> mdcList) {
//        // GroupBy: attribute, measure
//        HashMap<Pair<String, String>, List<Double>> am2thresholds = new HashMap<>();
//        for (MDCondition mdc: mdcList) {
//            Pair<String, String> am = new Pair<>(mdc.attribute, mdc.measure);
//            List<Double> thresholds = am2thresholds.containsKey(am) ? am2thresholds.get(am) : new ArrayList<>();
//            thresholds.add(mdc.threshold);
//            am2thresholds.put(am, thresholds);
//        }
//
//        List<MDCondition> finalMDCList = new ArrayList<>();
//        for (Map.Entry<Pair<String, String>, List<Double>> e: am2thresholds.entrySet()) {
//            switch (mdMergeAttrMsr) {
//                case UNION: // Any co-occurrence
//                    break;
//                case INTERSECTION:
//                    if (e.getValue().size() < numberOriginalMDs) { // Anything less than max co-occurrence
//                        continue;
//                    }
//                    break;
//                case THRESHOLD_BASED:
//                    // TODO
//                    throw new NotImplementedException("Option not implemented yet");
////                    break;
//            }
//            Double threshold = -1.0;
//
//            switch (mdMergeThr) {
//                case MIN:
//                    threshold = Collections.min(e.getValue());
//                    break;
//                case MAX:
//                    threshold = Collections.max(e.getValue());
//                    break;
//                case MEDIAN:
//                    List<Double> thresholds = e.getValue();
//                    Collections.sort(thresholds);
//                    threshold = thresholds.get(thresholds.size()/afterclassification);
//                    break;
//            }
//
//            MDCondition mergedMDC = new MDCondition(mergedID, e.getKey().getValue0(), e.getKey().getValue1(), threshold);
//            finalMDCList.add(mergedMDC);
//        }
//
//        return finalMDCList;
//    }

    public MD clone() {
        return new MD(id, dataset, lhsEncoded, rhsEncoded);
    }

    //    private Set<MDCondition> decode(String s) {
    private List<MDCondition> decode(String mdID, String s) {
        List<String> parts = Arrays.asList(s.split("#"));
        List<MDCondition> mdParts = parts.stream().map(part -> new MDCondition(mdID, part)).collect(Collectors.toList());
        return mdParts;
    }

    private String encode(List<MDCondition> l) {
        return StringUtils.join(l.stream().map(MDCondition::toString).collect(Collectors.toList()), "#");
    }

    @Override
    public String toString() {
        return lhsEncoded + "|" + rhsEncoded;
    }

    public String getId() {
        return id;
    }

    public String getDataset() {
        return dataset;
    }

    public String getLhsEncoded() {
        return lhsEncoded;
    }

    public List<MDCondition> getLhs() {
        return lhs;
    }

    public String getRhsEncoded() {
        return rhsEncoded;
    }

    public List<MDCondition> getRhs() {
        return rhs;
    }

    public List<MDCondition> getLhsRhs() {
        List<MDCondition> l = new ArrayList<>();
        l.addAll(lhs);
        l.addAll(rhs);
        return l;
    }

    public List<MDCondition> getMDSide(MDSide side) {
        switch (side) {
            case LHS:
                return getLhs();
            case RHS:
                return getRhs();
            case BOTH:
                return getLhsRhs();
        }
        return null;
    }

    public String getEncodingBySide(MDSide side) {
        switch (side) {
            case LHS:
                return getLhsEncoded();
            case RHS:
                return getRhsEncoded();
            case BOTH:
                return toString();
        }
        return null;
    }
}
