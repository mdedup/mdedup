package de.hpi.is.mdedup.utils.entities;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class MDCondition implements Comparable<MDCondition>{
    private String mdID;
    private String attribute;
    private String measure;
    private Double threshold;

    public MDCondition(String mdID, String encodedRepresentation) {
        this.mdID = mdID;
        Map<String, String> m = decode(encodedRepresentation);
        attribute = m.get("attribute");
        measure = m.get("measure");
        threshold = Double.valueOf(m.get("threshold"));
    }

    public MDCondition(String mdID, String attribute, String measure, Double threshold) {
        this.mdID = mdID;
        this.attribute = attribute;
        this.measure = measure;
        this.threshold = threshold;
    }

    private Map<String, String> decode(String s) {
        String[] parts = s.split("-");
        Map<String, String> m = new HashMap<>();
        m.put("attribute", parts[0]);
        m.put("measure", parts[1]);
        m.put("threshold", parts[2]);
        return m;
    }

    private String encode(String attribute, String measure, Double threshold) {
        return StringUtils.join(Arrays.asList(attribute, measure, threshold.toString()), "-");
    }

    @Override
    public String toString() {
        return encode(attribute, measure, threshold);
    }

    public String getAttribute() {
        return attribute;
    }

    public String getMeasure() {
        return measure;
    }

    public Double getThreshold() {
        return threshold;
    }

    @Override
    public int compareTo(MDCondition o) {
        Comparator<MDCondition> mdcndComparator =
//                Comparator.comparing(MDCondition::getMdID)
                Comparator.comparing(MDCondition::getAttribute)
//                .thenComparing(MDCondition::getAttribute)
                .thenComparing(MDCondition::getMeasure)
                .thenComparing(MDCondition::getThreshold);
        return mdcndComparator.compare(this, o);
//        int cmp = 0;
//
//        cmp = attribute.compareTo(o.mdID);
//        if (cmp != 0) {
//            return cmp;
//        }
//
//        cmp = attribute.compareTo(o.attribute);
//        if (cmp != 0) {
//            return cmp;
//        }
//
//        cmp = measure.compareTo(o.measure);
//        if (cmp != 0) {
//            return cmp;
//        }
//
//        cmp = threshold.compareTo(o.threshold);
//        if (cmp != 0) {
//            return cmp;
//        }
//
//        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MDCondition)) {
            return false;
        }
        MDCondition o = (MDCondition) obj;
        return attribute.equals(o.attribute) && (measure.equals(o.measure)) && (threshold.equals(o.threshold));
    }

    public String getMdID() {
        return mdID;
    }
}
