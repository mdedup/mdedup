package de.hpi.is.mdedup;

import de.hpi.is.mdedup.utils.SimilaritiesPair;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A template that is used across the different phases of the execution. A "Controller" is used to initiate the
 * execution and control all the steps required in a phase. From loading data, to executing the main process, and
 * finally exporting the results to the appropriate relational tables.
 */
public abstract class MDedupController {
    protected String dbUser;
    protected String dbPassword;
    protected String dbDriver;
    protected String dbConnection;

    protected MDedupIO mDedupIO;

    protected Map<Integer, SimilaritiesPair> sims;
    protected ArrayList<Integer> dpl;
    protected ArrayList<Integer> ndpl;

    public MDedupController(String dbUser, String dbPassword, String dbDriver, String dbConnection) {
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
        this.dbDriver = dbDriver;
        this.dbConnection = dbConnection;

        mDedupIO = new MDedupIO(dbDriver, dbConnection, dbUser, dbPassword);
    }

    public abstract void execute();

    public void importSimilarities(String querySims) {
        sims = mDedupIO.importSimilarities(querySims);
        dpl = sims.entrySet().stream().filter(e -> e.getValue().getPairClass().name().toLowerCase().equals("dpl"))
                .map(Map.Entry::getKey).collect(Collectors.toCollection(ArrayList::new));
        ndpl = sims.entrySet().stream().filter(e -> e.getValue().getPairClass().name().toLowerCase().equals("ndpl"))
                .map(Map.Entry::getKey).collect(Collectors.toCollection(ArrayList::new));
    }

}
