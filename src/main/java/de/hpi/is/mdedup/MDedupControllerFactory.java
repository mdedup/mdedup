package de.hpi.is.mdedup;

import de.hpi.is.mdedup.boosting.BoostingController;
import de.hpi.is.mdedup.boosting.classification.Classifier;
import de.hpi.is.mdedup.expansion.MDedupExpandController;
import de.hpi.is.mdedup.exploration.MDedupExplorationController;
import de.hpi.is.mdedup.exploration.MDedupExplorationIO;
import de.hpi.is.mdedup.prediction.MDedupPredictionController;
import de.hpi.is.mdedup.selection.MDedupSelectionController;
import de.hpi.is.mdedup.utils.SimpleArgsParser;
import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.entities.MDC;

import java.util.*;

/**
 * This class serves as the main starting point of all executions. It parses the arguments, selecting the appropriate
 * class to execute. To ease our experimenting we paired it with the accompanying Python project that orchestrates the
 * execution.
 *
 * Please refer to: https://gitlab.com/mdedup/mdedup_utils
 *
 * The following arguments are an example for executing MDC Selection on the dataset of restaurants.
 *
 * operation==selection dataset==restaurants query_hymd=="select * from mdedup.mdedup_hymd where dataset='restaurants'"
 * query_sims=="select * from mdedup.sims_restaurants" topk==64 max_duration==36000 db_user==XXX db_password==XXX
 * db_driver==com.mysql.cj.jdbc.Driver  * db_connection=="jdbc:mysql://localhost:3306/mdedup?testOnBorrow=true
 *  &useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"
 *
 */
public class MDedupControllerFactory {

    protected Map<String, String> params;

    public MDedupControllerFactory(String[] args) {
        params = SimpleArgsParser.parse(args);
    }

    public void execute() {
        String connectionURL = params.get("db_connection");
        if (connectionURL.charAt(0) == '\'') {
            connectionURL = connectionURL.substring(1, connectionURL.length()-1);
        }
        switch (params.get("operation")) {
            /**
             * Execute the MDC Selection step.
             */
            case "selection":
                MDedupSelectionController cntSelection = new MDedupSelectionController(
                    params.get("dataset"),
                    params.get("query_hymd"),
                    params.get("query_sims"),
                    Integer.valueOf(params.getOrDefault("topk", "64")),
                    Integer.valueOf(params.getOrDefault("max_duration", "-1")),
                    Integer.valueOf(params.getOrDefault("max_level", String.valueOf(Integer.MAX_VALUE))), // Not used
                    params.get("db_user"),
                    params.get("db_password"),
                    params.get("db_driver"),
                    connectionURL,
                    Boolean.valueOf(params.getOrDefault("enabled_expansion_phase", Boolean.TRUE.toString())),
                    params.getOrDefault("features_in_regression", "complete_feature_set")
                );
                cntSelection.execute();
                break;
            /**
             * Execute the MDC Expansion step.
             *
             * Two strategies are available: gaussian neighboring and uniform random.
             * That is why we first convert them into a list, along with the factor, that determines how much larger
             * the final sets should be relative to the MDC Selection set.
             */
            case "expansion":
                String sampleSizeByStrategyStr = params.get("sample_size_by_strategy");
                Map<String, Double> sampleSizeByStrategy = new HashMap<>();
                String[] strategies = sampleSizeByStrategyStr.split("#");

                for (String strategySize : strategies) {
                    String[] parts = strategySize.split("-");
                    sampleSizeByStrategy.put(parts[0], Double.valueOf(parts[1]));
                }
                MDedupExpandController cntExpansion = new MDedupExpandController(
                        params.get("db_user"),  // Database username
                        params.get("db_password"),  // Database password
                        params.get("db_driver"),  // Driver to use: e.g. com.mysql.cj.jdbc.Driver
                        connectionURL,  // IP of the database server
                        params.get("dataset"),
                        params.get("query_hymd"),  // Query to retrieve the MDs.
                        sampleSizeByStrategy,
                        Integer.valueOf(params.getOrDefault("topk", "64")),
                        Boolean.valueOf(params.getOrDefault("enabled_expansion_phase", Boolean.TRUE.toString())),
                        params.getOrDefault("features_in_regression", "complete_feature_set")
                );

                cntExpansion.execute();
                break;
            /**
             * Execute the MDC Exploration step. This step generates features for the selected and expanded MDCs of the
             * previous two steps. This is only executed in the training pipeline. However, the training pipeline also
             * uses its methods to generate features for the MDCs it considers during the lattice traversal.
             */
            case "exploration":
                MDedupExplorationController cntExploration = getExplorationController(connectionURL);
                cntExploration.execute();
                break;
            /**
             * Execute the MDC Prediction step.
             */
            case "prediction":
                cntExploration = getExplorationController(connectionURL);
                MDedupPredictionController cntPrediction = new MDedupPredictionController(
                        params.get("db_user"),
                        params.get("db_password"),
                        params.get("db_driver"),
                        connectionURL,

                        cntExploration,
                        params.get("dataset"),
                        Integer.valueOf(params.getOrDefault("topk", "64")),
                        Integer.valueOf(params.getOrDefault("max_level", String.valueOf(Integer.MAX_VALUE))),
                        params.get("regression_model"),

                        params.get("query_mdcs_exploration"),
                        Integer.valueOf(params.getOrDefault("max_duration", "-1")),
                        params.get("export_classified_pairs_by"),

                        Boolean.valueOf(params.getOrDefault("enabled_expansion_phase", Boolean.TRUE.toString())),
                        params.getOrDefault("features_in_regression", "complete_feature_set")
                );
                cntPrediction.execute();
                break;
            /**
             * Execute the boosting step.
             *
             * This step gets the pairs of the highest scored MDC, normally used in the application pipeline, trains
             * and further re-classifies them. This model is trained in pair similarities, in contrast to the MDC
             * that based its predictions only on data patterns.
             */
            case "boosting":
                List<String> attributes = new ArrayList<>(Arrays.asList(params.get("attributes").split(",")));
                List<String> restDatasets = new ArrayList<>(Arrays.asList(params.get("all_datasets").split(",")));
                restDatasets.remove(params.get("dataset"));

                Classifier.ClassifierType classificationAlgorithm = Classifier.ClassifierType.valueOf(params.get("classification_algorithm"));

                BoostingController cntCompetitors = new BoostingController(
                        params.get("db_user"),
                        params.get("db_password"),
                        params.get("db_driver"),
                        connectionURL,
                        params.get("dataset"),
                        restDatasets,
                        attributes,
                        classificationAlgorithm,
                        params.get("sql_gs_sims"),
                        params.get("sql_mdedup_prediction_pairs"),
                        params.getOrDefault("description", ""),
                        Integer.valueOf(params.getOrDefault("topk", "64")),
                        Boolean.valueOf(params.getOrDefault("enabled_expansion_phase", Boolean.TRUE.toString())),
                        params.getOrDefault("features_in_regression", "complete_feature_set")
                );
                cntCompetitors.execute();
                break;
        }
    }

    private MDedupExplorationController getExplorationController(String connectionURL) {

        MDedupExplorationIO mdexplio = new MDedupExplorationIO(
                params.get("db_driver"),
                connectionURL,
                params.get("db_user"),
                params.get("db_password")
        );

        Map<String, Map<String, String>> records = mdexplio.importTable(params.get("query_records"));
        Map<String, MD> mds = mdexplio.importMDs(params.get("query_hymd"));

        Map<String, MDC> mdcsSelection = mdexplio.importMDCsByPhase(params.get("query_mdcs_selection"));
        Map<String, MDC> mdcsExpansion = mdexplio.importMDCsByPhase(params.get("query_mdcs_expansion"));
        Map<String, MDC> mdcs = new HashMap<>(mdcsSelection);
        mdcs.putAll(mdcsExpansion);

        MDedupExplorationController controller = new MDedupExplorationController(
                params.get("db_user"),
                params.get("db_password"),
                params.get("db_driver"),
                connectionURL,
                params.get("dataset"),
                mdexplio,
                records,
                mds,
                mdcs,
                params.get("query_sims"));

        return controller;
    }

    public static void main(String[] args) {
        MDedupControllerFactory controllerFactory = new MDedupControllerFactory(args);
        controllerFactory.execute();
    }

}
