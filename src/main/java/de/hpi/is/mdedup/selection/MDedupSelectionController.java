package de.hpi.is.mdedup.selection;

import de.hpi.is.mdedup.MDedupController;
import de.hpi.is.mdedup.boosting.classification.utils.ClassificationDedupPairScored;
import de.hpi.is.mdedup.selection.utils.OptimalMDCInfo;
import de.hpi.is.mdedup.utils.DedupPair;
import de.hpi.is.mdedup.utils.SimilaritiesPair;
import de.hpi.is.mdedup.utils.classifiedpairs.MDCClassifiedPairsIO;
import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.entities.MDC;
import org.javatuples.Pair;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MDedupSelectionController extends MDedupController {

    private String dataset;
    private String queryHymd;
    private String querySims;
    private Integer maxDuration;
    private Integer maxLevel;

    private Integer topk;
    protected Boolean enabledExpansionPhase;
    protected String featuresInRegression;

    private MDedupSelectionIO mdedupio;
    private MDCClassifiedPairsIO clpio;

    public MDedupSelectionController(String dataset, String queryHymd, String querySims, Integer topk,
                                     Integer maxDuration, Integer maxLevel, String dbUser, String dbPassword,
                                     String dbDriver, String dbConnection, Boolean enabledExpansionPhase,
                                     String featuresInRegression) {
        super(dbUser, dbPassword, dbDriver, dbConnection);
        this.dataset = dataset;
        this.queryHymd = queryHymd;
        this.querySims = querySims;

        this.maxLevel = maxLevel;
        this.maxDuration = maxDuration;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
        this.dbDriver = dbDriver;
        this.dbConnection = dbConnection;

        this.topk = topk;
        this.enabledExpansionPhase = enabledExpansionPhase;
        this.featuresInRegression = featuresInRegression;

        mdedupio = new MDedupSelectionIO(dbDriver, dbConnection, dbUser, dbPassword);
        clpio = new MDCClassifiedPairsIO(dbDriver, dbConnection, dbUser, dbPassword);
    }

    public void execute() {
        Instant start = Instant.now();

        // Read pair similarities.
        importSimilarities(querySims);

        // Set the parameter top k.
        MDedupSelectionAlgorithm.SelectionStrategy selectionStrategy = MDedupSelectionAlgorithm.SelectionStrategy.TOPK;
        selectionStrategy.setK((double) topk);

        // Read MDs
        Map<String, MD> mds = mdedupio.importMDs(queryHymd);
        if (mds.size() == 1) {
            System.out.println("Only 1 MD was provided, there is nothing to combine");
            System.exit(-1);
        }

        System.out.println("Finished reading data");

        // Initialize the MDC Selection algorithm.
        MDedupSelectionAlgorithm selectionAlgorithm = new MDedupSelectionAlgorithmDedup(dataset, dpl, ndpl, mds, sims,
                selectionStrategy, maxLevel, maxDuration);

        // Retrieve the selected MDCs.
        Map<MDC, OptimalMDCInfo> optimalMDCInfoMap = selectionAlgorithm.execute();

        Instant finish = Instant.now();
        long totalExecutionDuration = Duration.between(start, finish).toMillis();

        // Reset the table in case of previous results for this dataset. (This is useful in the evaluation /
        // experimentation context)
        mdedupio.resetTable(dataset, maxLevel.toString(), topk.toString(), maxDuration.toString());
        System.out.println("Saving " + optimalMDCInfoMap.size() + " results.");

        // Export selected MDCs.
        mdedupio.exportTable(new MDedupSelectionIO.PrepareStatementParametersSelection(
                dataset, optimalMDCInfoMap, maxLevel, topk, selectionAlgorithm.getExecutionInterrupted(),
                maxDuration.longValue(), selectionAlgorithm.getExecutionDuration(), totalExecutionDuration,
                "selection", enabledExpansionPhase, featuresInRegression
        ));

        // Export the pairs as they are classified from the highest scored MDC.
        exportClassifiedPairs(optimalMDCInfoMap, mds);

        mdedupio.close();
    }


    private void exportClassifiedPairs(Map<MDC, OptimalMDCInfo> optimalMDCInfoMap, Map<String, MD> mds) {
        // Retrieve the MDC with the highest score (F1 in this case).
        Map.Entry<MDC, OptimalMDCInfo> mdcWithMaxScore = optimalMDCInfoMap.entrySet().stream().max(
                Comparator.comparing(mdc -> mdc.getValue().getScore())).get();

        Map<String, MDC> mdcsSelection = optimalMDCInfoMap.keySet().stream().collect(Collectors.toMap(MDC::getId,
                mdc -> mdc));

        OracleOfDedup oracle = new OracleOfDedup(dpl, ndpl, mds, sims);

        // Retrieve the pairs as they are classified by the MDC.
        List<SimilaritiesPair> pairsSatisfiedByBestMDC = oracle.getPairsSatisfiedOfMDC(mdcWithMaxScore.getKey().getId(),
                mds, mdcsSelection, sims);
        Set<Pair<String, String>> pairsSatisfiedByBestMDCSet = pairsSatisfiedByBestMDC.stream()
                .map(p -> new Pair<>(p.getId1(), p.getId2())).collect(Collectors.toSet());

        // The pairs matched from the MDC are labeled as duplicates, whereas the rest as non-duplicates.
        List<ClassificationDedupPairScored> pairsSatisfiedByBestMDCMapped = sims.values().stream()
                .map(p -> new ClassificationDedupPairScored(p.getDataset(), p.getId(), p.getId1(), p.getId2(),
                        p.getPairClass(),
                        pairsSatisfiedByBestMDCSet.contains(new Pair<>(p.getId1(), p.getId2())) ? DedupPair.PairClass.DPL : DedupPair.PairClass.NDPL,
                        1.0, 1.0
                )).collect(Collectors.toList());

        // Exporting the pairs.
        clpio.exportTable(new MDCClassifiedPairsIO.PrepareStatementParametersClassifiedPairs(
                dataset,
                pairsSatisfiedByBestMDCMapped,
                "mdedup_selection",
                mdcWithMaxScore.getKey().generateID(),
                "",
                topk,
                enabledExpansionPhase,
                featuresInRegression
        ));
        clpio.close();
    }

}
