package de.hpi.is.mdedup.selection.utils;

import de.hpi.is.mdedup.utils.ClassificationEvaluation;
import de.hpi.is.mdedup.utils.entities.MDC;

import java.util.Set;

public class OptimalMDCInfo {
    MDC mdc;
    String expansionProcess;
    Double score;
    Integer level;
    Integer topkPosition;
    Set<MDC> children;
    ClassificationEvaluation classificationEvaluation;
    Long durationToCreation;

    public OptimalMDCInfo(MDC mdc, String expansionProcess, Double score, Integer level, Integer topkPosition, Set<MDC> children, long durationToCreation) {
        this.mdc = mdc;
        this.expansionProcess = expansionProcess;
        this.score = score;
        this.level = level;
        this.topkPosition = topkPosition;
        this.children = children;
        this.durationToCreation = durationToCreation;
    }

    public Set<MDC> getChildren() {
        return children;
    }

    public void setClassificationEvaluation(ClassificationEvaluation classificationEvaluation) {
        this.classificationEvaluation = classificationEvaluation;
    }

    public ClassificationEvaluation getClassificationEvaluation() {
        return classificationEvaluation;
    }

    public String getPhase() {
        return expansionProcess;
    }

    public MDC getMdc() {
        return mdc;
    }

    public Double getScore() {
        return score;
    }

    public Integer getLevel() {
        return level;
    }

    public Integer getTopkPosition() {
        return topkPosition;
    }

    public Long getDurationToCreation() {
        return durationToCreation;
    }
}
