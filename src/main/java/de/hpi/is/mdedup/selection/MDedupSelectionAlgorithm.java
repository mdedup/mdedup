package de.hpi.is.mdedup.selection;

import de.hpi.is.mdedup.selection.utils.OptimalMDCInfo;
import de.hpi.is.mdedup.utils.ClassificationEvaluation;
import de.hpi.is.mdedup.utils.Oracle;
import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.entities.MDC;
import org.javatuples.Pair;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public abstract class MDedupSelectionAlgorithm {
    Integer maxDuration; // In seconds

    Integer maxLevel;

    protected String dataset;
    protected Map<String, MD> mds;
    protected Oracle oracle;
    protected Double samplingRatioMDs;

    protected Boolean isExecutionInterrupted;
    protected Long executionDuration;

    Boolean storeChildrenInformation;

    public enum SelectionStrategy {
        EXHAUSTIVE_SEARCH(100.0), TOPK(64);

        private double k;
        SelectionStrategy(double k) {
            this.k = k;
        }

        public double getK() {
            return k;
        }
        public void setK(double k) {
            this.k = k;
        }
    }
    private SelectionStrategy selectionStrategy = null;


    public MDedupSelectionAlgorithm(String dataset, Map<String, MD> mds,
                                    SelectionStrategy selectionStrategy, Integer maxLevel,
                                    Integer maxDuration, Boolean storeChildrenInformation, Oracle oracle, Double samplingRatioMDs) {
        this.dataset = dataset;
        this.mds = mds;
        this.oracle = oracle;
        this.maxDuration = maxDuration;
        this.storeChildrenInformation = storeChildrenInformation;
        this.selectionStrategy = selectionStrategy == null ? SelectionStrategy.TOPK: selectionStrategy;
        this.maxLevel = maxLevel;
        this.samplingRatioMDs = samplingRatioMDs;

        this.isExecutionInterrupted = Boolean.FALSE;
    }


    public Map<MDC, OptimalMDCInfo> execute() {
        Instant start = Instant.now();

        Map<MDC, Double> current = new HashMap<>(), previous = new HashMap<>(), optimal = new HashMap<>();

        List<MDC> singleMDCsScored = mds.keySet().parallelStream().map(x -> new MDC(Collections.singleton(x)))
                .collect(Collectors.toList());

        // Batch calculation for speed.
        oracle.batchCalculateScoresForMDCs(singleMDCsScored);

        List<Pair<String, Double>> mdsScored = singleMDCsScored.parallelStream().map(x ->
                new Pair<>(x.getId(), oracle.fetchScoreMDC(x))).collect(Collectors.toList());

        // Remove zero F-Measure MDs.
        mdsScored = mdsScored.stream().filter(p -> p.getValue1() > 0.0).collect(Collectors.toList());
        mdsScored.sort((o1, o2) -> -1 * o1.getValue1().compareTo(o2.getValue1()));

//        Map<String, MD> mdsSampled = mds;
        Map<String, MD> mdsSampled = mdsScored.stream().collect(Collectors.toMap(
                Pair::getValue0,
                p -> mds.get(p.getValue0())
        ));
        if (samplingRatioMDs < 1.0) {
            if (mdsScored.size() > (samplingRatioMDs * mds.size())) {
                mdsScored = mdsScored.subList(0, (int)(samplingRatioMDs * mds.size()));
            }

            Set<String> mdcIDsNonZero = mdsScored.stream().map(Pair::getValue0).collect(Collectors.toSet());
            mdsSampled = mds.entrySet().parallelStream().filter(e -> mdcIDsNonZero.contains(e.getKey())).collect(Collectors.toMap(
                    Map.Entry::getKey,
                    Map.Entry::getValue
            ));
        }

        // Keep only the TopK to combine
        mdsScored = mdsScored.subList(0, (int) Math.min(selectionStrategy.getK(), mdsScored.size()));

        mdsScored.forEach(p -> current.put(new MDC(Collections.singleton(p.getValue0())), p.getValue1()));
        previous.clear(); previous.putAll(current);
        optimal.putAll(current);

        List<Pair<MDC, Double>> currentList = current.entrySet().stream()
                .map(x -> new Pair<>(x.getKey(), x.getValue())).collect(Collectors.toList());
        currentList.sort((o1, o2) -> -1 * o1.getValue1().compareTo(o2.getValue1()));
        currentList = currentList.subList(0, (int) Math.min(selectionStrategy.getK(), currentList.size()));

        Map<MDC, OptimalMDCInfo> optimalExtraInfo = new HashMap<>();

        for (int i = 0; i < currentList.size(); ++i) {
            Pair<MDC, Double> p = currentList.get(i);
            optimalExtraInfo.put(p.getValue0(), new OptimalMDCInfo(p.getValue0(), "selection", p.getValue1(), 1,
                    i+1, new HashSet<>(), 0));
        }

        int level = 0;
        long startExecution = System.nanoTime();
        mainLoop:while(current.size() > 0) {
            ++level;
            current.clear();
            System.out.println("Discovery for dataset: " + dataset + " level " + level + " with optimal " + optimal.size());

            Map<MDC, Set<MDC>> candidates = new TreeMap<>();
            for (Map.Entry<MDC, Double> mdcmbScore : previous.entrySet()) {
                Set<String> restMDs = new HashSet<>(mdsSampled.keySet());
                restMDs.removeAll(mdcmbScore.getKey().getMDsIDs());
                for (String md : restMDs) {
                    Set<String> ids = new HashSet<>(mdcmbScore.getKey().getMDsIDs());
                    ids.add(md);
                    MDC candidate = new MDC(ids);
                    if (!candidates.containsKey(candidate)) {
                        candidates.put(candidate, new HashSet<>());
                    }
                    candidates.get(candidate).add(mdcmbScore.getKey());
                }
            }

            // Batch calculation for speed.
            oracle.batchCalculateScoresForMDCs(new ArrayList<>(candidates.keySet()));

            for (Map.Entry<MDC, Set<MDC>> candidate : candidates.entrySet()) {
                Double candidateScore = oracle.fetchScoreMDC(candidate.getKey());
                List<Double> parentsScores = candidate.getValue().stream().map(previous::get)
                        .collect(Collectors.toList());
                if (candidateScore > Collections.max(parentsScores)) {
                    current.put(candidate.getKey(), candidateScore);
                    if (storeChildrenInformation) {
                        for (MDC parent: candidate.getValue()) {
                            optimalExtraInfo.get(parent).getChildren().add(candidate.getKey());
                        }
                    }
                }
            }

            if (current.size() == 0) {
                break;
            }

            currentList = current.entrySet().stream()
                    .map(x -> new Pair<>(x.getKey(), x.getValue())).collect(Collectors.toList());

            // Remove dominated parents.
            for (Pair<MDC, Double> candidate: currentList) {
                optimal.keySet().removeAll(candidates.get(candidate.getValue0()));
            }

            currentList.sort((o1, o2) -> -1 * o1.getValue1().compareTo(o2.getValue1()));
            currentList = currentList.subList(0, (int) Math.min(selectionStrategy.getK(), currentList.size()));

            long duration = TimeUnit.SECONDS.convert(System.nanoTime() - startExecution, TimeUnit.NANOSECONDS);
            for (int i = 0; i < currentList.size(); ++i) {
                Pair<MDC, Double> p = currentList.get(i);
                optimalExtraInfo.put(p.getValue0(), new OptimalMDCInfo(p.getValue0(), "selection", p.getValue1(), level+1,
                        i+1, new HashSet<>(), duration));
            }

            previous.clear();
            currentList.forEach(p -> previous.put(p.getValue0(), p.getValue1()));
            currentList.forEach(p -> optimal.put(p.getValue0(), p.getValue1()));

            if(isExecutionTimeOver(startExecution)) {
                isExecutionInterrupted = Boolean.TRUE;
                break mainLoop;
            }
        }

        oracle.batchCalculateEvaluationsForMDCs(new ArrayList<>(optimalExtraInfo.keySet()));

        optimalExtraInfo.forEach((p, info) -> {
            ClassificationEvaluation classificationEvaluation = oracle.fetchEvaluationMDC(p);
            info.setClassificationEvaluation(classificationEvaluation);
        });

        Instant finish = Instant.now();
        executionDuration = Duration.between(start, finish).toMillis();

        return optimalExtraInfo;
    }

    private boolean isExecutionTimeOver(long startExecution) {
        long durationInSeconds = TimeUnit.SECONDS.convert(System.nanoTime() - startExecution, TimeUnit.NANOSECONDS);
        if (durationInSeconds >= maxDuration) {
            System.out.println("Execution time is over!");
            return true;
        } else {
            return false;
        }
    }

    public Oracle getOracle() {
        return oracle;
    }

    public Map<String, MD> getMds() {
        return mds;
    }

    public Boolean getExecutionInterrupted() {
        return isExecutionInterrupted;
    }

    public Long getExecutionDuration() {
        return executionDuration;
    }
}
