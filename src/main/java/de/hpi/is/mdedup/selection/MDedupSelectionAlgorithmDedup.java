package de.hpi.is.mdedup.selection;

import de.hpi.is.mdedup.utils.SimilaritiesPair;
import de.hpi.is.mdedup.utils.entities.MD;

import java.util.*;

public class MDedupSelectionAlgorithmDedup extends MDedupSelectionAlgorithm {

    public MDedupSelectionAlgorithmDedup(String dataset, ArrayList<Integer> dpl, ArrayList<Integer> ndpl,
                                         Map<String, MD> mds, Map<Integer, SimilaritiesPair> sims,
                                         SelectionStrategy selectionStrategy,
                                         Integer maxLevel, Integer maxDuration) {
        super(dataset, mds, selectionStrategy, maxLevel, maxDuration, Boolean.TRUE, new OracleOfDedup(dpl, ndpl, mds, sims), 1.0);
    }

}
