package de.hpi.is.mdedup.selection;

import de.hpi.is.mdedup.MDedupIO;
import de.hpi.is.mdedup.selection.utils.OptimalMDCInfo;
import de.hpi.is.mdedup.utils.ClassificationEvaluation;
import de.hpi.is.mdedup.utils.entities.MDC;
import org.apache.commons.lang3.StringUtils;

import java.math.RoundingMode;
import java.sql.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MDedupSelectionIO extends MDedupIO {

    public MDedupSelectionIO(String dbDriver, String dbConnection, String dbUser, String dbPassword) {
        super(dbDriver, dbConnection, dbUser, dbPassword);
    }

    public MDedupSelectionIO(Connection conn) {
        super(conn);
    }


    public List<List<Integer>> importMDCsSelection(String dataset) {
        return importMDCsByPhase(dataset, "selection");
    }


    public void resetTable(String dataset, String maxLevel, String topk, String maxDuration) {
        try {
            createTable();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // If dataset == null, then
        String tableName = "mdedup_mdcs";
        String sql = null;
        if (dataset == null) {
            sql = "TRUNCATE " + tableName + ";";
        } else {
            sql = "delete from " + tableName + " \n" +
                    "where dataset='" + dataset + "'";
            if (maxLevel != null && !maxLevel.equals("")) {
                sql += " and max_level="+maxLevel;
            }
            if (maxDuration != null && !maxDuration.equals("")) {
                sql += " and lattice_max_duration="+maxDuration;
            }
            if (topk != null && !topk.equals("")) {
                sql += " and topk="+topk;
            }
            sql += ";";
        }
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.execute();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public static class PrepareStatementParametersSelection extends PrepareStatementParameters {
        protected Map<MDC, OptimalMDCInfo> mdcsSelected;
        protected Integer maxLevel;

        protected Boolean isExecutionInterrupted;
        protected Long maxDuration;
        protected Long latticeExecutionDuration;
        protected Long totalExecutionDuration;

        protected String expansionProcess;

        protected List<Map<String, Object>> data;

        protected Integer topk;
        protected Boolean enabledExpansionPhase;
        protected String featuresInRegression;

        public PrepareStatementParametersSelection(String dataset, Map<MDC, OptimalMDCInfo> mdcsSelected,
                                                   Integer maxLevel, Integer topk, Boolean isExecutionInterrupted,
                                                   Long maxDuration, Long latticeExecutionDuration,
                                                   Long totalExecutionDuration, String expansionProcess,
                                                   Boolean enabledExpansionPhase, String featuresInRegression) {
            super(dataset, "mdedup_mdcs");
            this.mdcsSelected = mdcsSelected;
            this.maxLevel = maxLevel;
            this.topk = topk;
            this.isExecutionInterrupted = isExecutionInterrupted;
            this.maxDuration = maxDuration;
            this.latticeExecutionDuration = latticeExecutionDuration;
            this.totalExecutionDuration = totalExecutionDuration;
            this.expansionProcess = expansionProcess;

            this.enabledExpansionPhase = enabledExpansionPhase;
            this.featuresInRegression = featuresInRegression;
        }

        public List<Map<String, Object>> prepareData() {
            DecimalFormat df = new DecimalFormat("#.####");
            df.setRoundingMode(RoundingMode.HALF_UP);

            Set<String> header = new HashSet<>();
            data = mdcsSelected.entrySet().stream().map(e -> {
                Map<String, Object> r = new HashMap<>();

                r.put("dataset", dataset);
                r.put("ids", StringUtils.join(e.getKey().getMDsIDsSorted(), "_"));
                List<String> children = e.getValue().getChildren().stream().map(MDC::toString).sorted(String::compareTo)
                        .collect(Collectors.toList());
                r.put("children", String.join("|", children));

                r.put("phase", e.getValue().getPhase());

                r.put("max_level", maxLevel);
                r.put("level", e.getValue().getLevel());

                r.put("topk_position", e.getValue().getTopkPosition());

                r.put("topk", topk.toString());
                r.put("enabled_expansion_phase", enabledExpansionPhase);
                r.put("features_in_regression", featuresInRegression);

//                r.put("prediction_model", "");
//                r.put("prediction_model_parameters", "");
                r.put("prediction_score", e.getValue().getScore());

                ClassificationEvaluation ce = e.getValue().getClassificationEvaluation();

                r.put("precision_", ce.calculatePrecision());
                r.put("recall", ce.calculateRecall());
                r.put("f1", ce.calculateFMeasure(1.0));

                r.put("f05", ce.calculateFMeasure(0.5));
                r.put("f20", ce.calculateFMeasure(2.0));

//                r.put("auc_roc", -1.0);
//                r.put("auc_pr", -1.0);

                r.put("tp", ce.getTp());
                r.put("fn", ce.getFn());
                r.put("fp", ce.getFp());
                r.put("tn", ce.getTn());

                r.put("is_execution_interrupted", isExecutionInterrupted);
                r.put("lattice_max_duration", maxDuration);
                r.put("lattice_actual_duration", latticeExecutionDuration);
                r.put("lattice_md_duration", e.getValue().getDurationToCreation());
                r.put("total_execution_duration", totalExecutionDuration);

                if (header.size() == 0) {
                    header.addAll(r.keySet());
                }

                return r;
            }).collect(Collectors.toList());

            this.header = new ArrayList<>(header);
            this.header.sort(String::compareTo);

            return data;
        }
    }

}
