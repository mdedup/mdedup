package de.hpi.is.mdedup.selection;

import com.google.common.collect.TreeMultimap;
import de.hpi.is.mdedup.utils.*;
import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.entities.MDC;
import de.hpi.is.mdedup.utils.entities.MDCondition;
import org.javatuples.Pair;

import java.util.*;
import java.util.stream.Collectors;

public class OracleOfDedup extends Oracle {
    ArrayList<Integer> dpl, ndpl;
    Integer dplMax, ndplMax;

    Map<String, MD> mds;
    Map<Integer, SimilaritiesPair> sims;

    Map<String, TreeMultimap<Double, Integer>> am2sim2pairsDPL, am2sim2pairsNDPL;

    /* MDCondition <attribute, measure, threshold>   ---->     <TP, FP> */
    Map<String, Pair<BitSet, BitSet>> mdc2tpfp = new HashMap<String, Pair<BitSet, BitSet>>();

    /* MD <id>   ---->     <TP, FP> */
    Map<String, Pair<BitSet, BitSet>> md2tpfp = new HashMap<>();

    Map<String, ClassificationEvaluation> mdcsEvaluations;

    public OracleOfDedup(ArrayList<Integer> dpl, ArrayList<Integer> ndpl, Map<String, MD> mds,
                         Map<Integer, SimilaritiesPair> sims) {
        this.dpl = dpl;
        this.dplMax = Collections.max(dpl);
        this.ndpl = ndpl;
        this.ndplMax = Collections.max(ndpl);
        this.mds = mds;
        this.sims = sims;
        initialize();
    }

    public void initialize() {
        System.out.println("Oracle calculating indexes");
        buildIndexes();
        System.out.println("Oracle calculating classification results for MDConditions");
        calculateBitsetsMDConditions();
        System.out.println("Oracle calculating classification results for MDs");
        calculateBitsetsMDs();
    }

    private void buildIndexes() {
        am2sim2pairsDPL = buildIndex(DedupPair.PairClass.DPL);
        am2sim2pairsNDPL = buildIndex(DedupPair.PairClass.NDPL);
    }

    private Map<String, TreeMultimap<Double, Integer>> buildIndex(DedupPair.PairClass pairClass) {
        Map<String, TreeMultimap<Double, Integer>> am2sim2pairs = new HashMap<>();

        for (Map.Entry<Integer, SimilaritiesPair> pair: sims.entrySet()) {
            if (pair.getValue().getPairClass() == pairClass) {
                for (Map.Entry<String, Double> am2sim: pair.getValue().getSims().entrySet()) {
                    if (!am2sim2pairs.containsKey(am2sim.getKey())) {
                        am2sim2pairs.put(am2sim.getKey(), TreeMultimap.create());
                    }
                    am2sim2pairs.get(am2sim.getKey()).put(am2sim.getValue(), pair.getKey());
                }
            }
        }
        return am2sim2pairs;
    }

    private void calculateBitsetsMDConditions() {
        for (Map.Entry<String, MD> eMD: mds.entrySet()) {
            for (MDCondition mdc : eMD.getValue().getLhs()) {
                if (!mdc2tpfp.containsKey(mdc.toString())) {
                    ClassificationSets classificationSets = classificationMDCondition(mdc, false);

                    BitSet bsTP = bitsetifyIterator(classificationSets.getTp().iterator(), dplMax+1);
                    BitSet bsFP = bitsetifyIterator(classificationSets.getFp().iterator(), ndplMax+1);

                    mdc2tpfp.put(mdc.toString(), new Pair<>(bsTP, bsFP));
                }
            }
        }
    }

    private void calculateBitsetsMDs() {
        for (Map.Entry<String, MD> eMD: mds.entrySet()) {
            BitSet bsTP = new BitSet(dpl.size());
            bsTP.set(0, dplMax+1, true);
            BitSet bsFP = new BitSet(ndpl.size());
            bsFP.set(0, ndplMax+1, true);
            for (MDCondition mdc : eMD.getValue().getLhs()) {
                bsTP.and(mdc2tpfp.get(mdc.toString()).getValue0());
                bsFP.and(mdc2tpfp.get(mdc.toString()).getValue1());
            }
            md2tpfp.put(eMD.getKey(), new Pair<>(bsTP, bsFP));
        }
    }

    private Pair<BitSet, BitSet> mergeBitsetsMDs(MDC mdc) {
        BitSet bsTP = new BitSet(dpl.size());
        bsTP.set(0, dplMax+1, false);
        BitSet bsFP = new BitSet(ndpl.size());
        bsFP.set(0, ndplMax+1, false);

        for (String mdID: mdc.getMDsIDs()) {
            bsTP.or(md2tpfp.get(mdID).getValue0());
            bsFP.or(md2tpfp.get(mdID).getValue1());
        }
        return new Pair<>(bsTP, bsFP);
    }

    @Override
    public void batchCalculateScoresForMDCs(List<MDC> mdcList) {
        batchCalculateEvaluationsForMDCs(mdcList);
    }

    public Double fetchScoreMDC(MDC mdc) {
        return fetchEvaluationMDC(mdc).calculateFMeasure(1.0);
//        return fetchEvaluationMDC(mdc).calculateFMeasure(0.5);
    }

    @Override
    public void batchCalculateEvaluationsForMDCs(List<MDC> mdcList) {
        List<Pair<String, ClassificationEvaluation>> evaluations = mdcList.parallelStream().map(mdc -> {
            Pair<BitSet, BitSet> p = null;
            try {
                p = mergeBitsetsMDs(mdc);
            } catch (Exception e) {
                System.out.println(mdc.toString());
                e.printStackTrace();
            }

            ClassificationEvaluation evl = evaluateBitSets(p.getValue0(), p.getValue1());
            return new Pair<>(mdc.getId(), evl);
        }).collect(Collectors.toList());

        mdcsEvaluations = new HashMap<>();
        evaluations.forEach(p -> mdcsEvaluations.put(p.getValue0(), p.getValue1()));
    }

    public ClassificationEvaluation fetchEvaluationMDC(MDC mdc) {
//        Pair<BitSet, BitSet> p = mergeBitsetsMDs(mdc);
//        return evaluateBitSets(p.getValue0(), p.getValue1());
        return mdcsEvaluations.get(mdc.getId());
    }


    private ClassificationSets classificationMDCondition(MDCondition mdc, boolean calculateNegatives) {
        String am = mdc.getAttribute() + "-" + mdc.getMeasure();
        Double threshold = mdc.getThreshold();

        ClassificationSets clsfSts = null;

        try {
            HashSet<Integer> tp = am2sim2pairsDPL.get(am).keySet().tailSet(threshold).stream()
                    .map(thr -> am2sim2pairsDPL.get(am).get(thr)).flatMap(Collection::stream).collect(Collectors.toCollection(HashSet::new));
            HashSet<Integer> fp = am2sim2pairsNDPL.get(am).keySet().tailSet(threshold).stream()
                    .map(thr -> am2sim2pairsNDPL.get(am).get(thr)).flatMap(Collection::stream).collect(Collectors.toCollection(HashSet::new));

            HashSet<Integer> fn = null;
            HashSet<Integer> tn = null;

            if (calculateNegatives) {
                fn = new HashSet<>(dpl);
                fn.removeAll(tp);
                tn = new HashSet<>(ndpl);
                tn.removeAll(fp);
            }

            clsfSts = new ClassificationSets(tp, fp, fn, tn);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return clsfSts;
    }

//    private ClassificationEvaluation evaluateClassificationSets(ClassificationSets clsfSts) {
//        int tp = clsfSts.getTp().size();
//        int fp = clsfSts.getFp().size();
//        int fn = clsfSts.getFn().size();
//        int tn = clsfSts.getTn().size();
//
//        ClassificationEvaluation evaluation = new ClassificationEvaluation(tp, fp, tn, fn);
//
//        return evaluation;
//    }

    private ClassificationEvaluation evaluateBitSets(BitSet bsTP, BitSet bsFP) {
        int tp = bsTP.cardinality();
        int fn = dpl.size() - tp;

        int fp = bsFP.cardinality();
        int tn = ndpl.size() - fp;

        ClassificationEvaluation evaluation = new ClassificationEvaluation(tp, fp, tn, fn);

        return evaluation;
    }

    private BitSet bitsetifyIterator(Iterator<Integer> c, int size) {
        BitSet bs = new BitSet(size);
        bs.set(0, size-1, false);
        c.forEachRemaining(i -> bs.set(i, true));
        return bs;
    }
}
