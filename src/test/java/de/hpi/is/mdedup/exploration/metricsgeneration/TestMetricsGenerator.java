package de.hpi.is.mdedup.exploration.metricsgeneration;

import de.hpi.is.mdedup.utils.DedupPair;
import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.SimilaritiesPair;
import de.hpi.is.mdedup.utils.entities.MDC;
import info.debatty.java.stringsimilarity.Levenshtein;

import java.util.*;

public interface TestMetricsGenerator {

    default Map<String, Map<String, String>> createData () {
        Map<String, String> r1 = new HashMap<>();
        r1.put("fn", "john");
        r1.put("institute", "hpi");
        r1.put("age", "29");

        Map<String, String> r2 = new HashMap<>();
        r2.put("fn", "lan");
        r2.put("institute", "hpi");
        r2.put("age", "");

        Map<String, String> r3 = new HashMap<>();
        r3.put("fn", "john");
        r3.put("institute", "hpi");
        r3.put("age", "");

        Map<String, Map<String, String>> data = new HashMap<>();
        data.put("1", r1);
        data.put("2", r2);
        data.put("3", r3);

        return data;
    }

    default Map<Integer, SimilaritiesPair> createSims () {
        Map<String, Map<String, String>> data = createData();
        List<String> recordIDs = new ArrayList<>(data.keySet());

        Map<Integer, SimilaritiesPair> sims = new HashMap<>();

        Levenshtein lev = new Levenshtein();

        int counter = 0;
        for (int i = 0; i < recordIDs.size(); ++i) {
            String id1 = recordIDs.get(i);
            Map<String, String> r1 = data.get(recordIDs.get(i));
            for (int j = i+1; j < recordIDs.size(); ++j) {
                String id2 = recordIDs.get(j);
                Map<String, String> r2 = data.get(recordIDs.get(j));

                Map<String, Double> pairSims = new HashMap<>();

                for (String attr: r1.keySet()) {
                    String v1 = r1.getOrDefault(attr, "");
                    String v2 = r2.getOrDefault(attr, "");
                    double dst = lev.distance(v1, v2);
                    if (Double.isNaN(dst) || Double.isInfinite(dst)) {
                        dst = 1.0;
                    }
                    pairSims.put(attr + "_" + "lev", 1.0-(dst / Math.max(v1.length(), v2.length())));
                }

                SimilaritiesPair sp = new SimilaritiesPair("myDataset", id1 + id2,
                        id1, id2, DedupPair.PairClass.NDPL, pairSims);
                sims.put(++counter, sp);
            }
        }

        return sims;
    }

    default Map<String, MD> createMDs () {
        MD md1 = new MD("1", "myDataset", "fn-lev-0.7#institute-lev-0.9", "institute-lev-0.8");
        MD md2 = new MD("2", "myDataset", "fn-lev-0.8#age-lev-0.5", "institute-lev-0.8");
        MD md3 = new MD("3", "myDataset", "institute-lev-0.9", "fn-lev-1.0");
        MD md4 = new MD("4", "myDataset", "fn-lev-0.9", "fn-lev-1.0");

        Map<String, MD> mds = new HashMap<>();
        mds.put("1", md1);
        mds.put("2", md2);
        mds.put("3", md3);
        mds.put("4", md4);

        return mds;
    }

    default Map<String, MDC> createMDCs() {
        Set<String> mdc12 = new HashSet<>(Arrays.asList("1", "2"));
        Set<String> mdc3 = new HashSet<>(Arrays.asList("3"));
        Set<String> mdc4 = new HashSet<>(Arrays.asList("4"));

        Map<String, MDC> mdcs = new HashMap<>();
        mdcs.put("12", new MDC(mdc12));
        mdcs.put("3", new MDC(mdc3));
        mdcs.put("4", new MDC(mdc4));
        return mdcs;
    }

}
