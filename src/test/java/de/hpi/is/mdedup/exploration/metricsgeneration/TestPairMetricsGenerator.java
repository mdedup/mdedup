package de.hpi.is.mdedup.exploration.metricsgeneration;

import de.hpi.is.mdedup.exploration.metricsgenerators.PairMetricsGenerator;
import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.SimilaritiesPair;
import de.hpi.is.mdedup.utils.entities.MDC;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.Map;
import java.util.Set;

public class TestPairMetricsGenerator extends TestCase implements TestMetricsGenerator {

    protected Map<String, MD> mds;
    protected Map<String, Map<String, String>> data;
    protected Map<Integer, SimilaritiesPair> sims;
    protected Map<String, MDC> mdcs;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        data = createData();
        sims = createSims();
        mds = createMDs();
        mdcs = createMDCs();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testSupport() {
        PairMetricsGenerator gPMD = new PairMetricsGenerator(mds, data, sims, mdcs);

        System.out.println(gPMD.getSupportNormalized());
    }

    @Test
    public void testConfidence() {
        PairMetricsGenerator gPMD = new PairMetricsGenerator(mds, data, sims, mdcs);

        System.out.println(gPMD.getConfidenceNormalized());
    }

    @Test
    public void testLift() {
        PairMetricsGenerator gPMD = new PairMetricsGenerator(mds, data, sims, mdcs);

        System.out.println(gPMD.getLiftNormalized());
    }


    @Test
    public void testConviction() {
        PairMetricsGenerator gPMD = new PairMetricsGenerator(mds, data, sims, mdcs);

        System.out.println(gPMD.getConvictionNormalized());
    }

}
