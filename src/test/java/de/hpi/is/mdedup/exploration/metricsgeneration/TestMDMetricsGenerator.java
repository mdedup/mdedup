package de.hpi.is.mdedup.exploration.metricsgeneration;

import de.hpi.is.mdedup.exploration.metricsgenerators.MDMetricsGenerator;
import de.hpi.is.mdedup.utils.entities.MD;
import de.hpi.is.mdedup.utils.SimilaritiesPair;
import de.hpi.is.mdedup.utils.entities.MDC;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.Map;
import java.util.Set;

public class TestMDMetricsGenerator extends TestCase implements TestMetricsGenerator {

    protected Map<String, MD> mds;
    protected Map<String, Map<String, String>> data;
    protected Map<Integer, SimilaritiesPair> sims;
    protected Map<String, MDC> mdcs;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        data = createData();
        sims = createSims();
        mds = createMDs();
        mdcs = createMDCs();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testGetStatOfMDC() {
        MDMetricsGenerator gMD = new MDMetricsGenerator(mds, data, sims, mdcs);

        for (MDMetricsGenerator.MDPart part: MDMetricsGenerator.MDPart.values()) {
            for (MDMetricsGenerator.SetOperation setOperation: MDMetricsGenerator.SetOperation.values()) {
                System.out.println("Part: " + part + " setOperation: " + setOperation);
                System.out.println(gMD.getSetSizeStatOfMDCs(part, setOperation));
                System.out.println("");
            }
        }
    }

    @Test
    public void testThresholdStatsMDC() {
        MDMetricsGenerator gMD = new MDMetricsGenerator(mds, data, sims, mdcs);

        System.out.println(gMD.getThresholdMDCStats());
    }

    @Test
    public void testCardinalityStatsMDC() {
        MDMetricsGenerator gMD = new MDMetricsGenerator(mds, data, sims, mdcs);

        System.out.println(gMD.getCardinalityOfMDC());
    }
}
