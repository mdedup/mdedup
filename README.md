# MDedup - Duplicate Detection with Matching Dependencies

This project implements the steps described in the paper "Duplicate Detection with Matching Dependencies".

## Installation

Before executing the algorithms, it is necessary to install Java JDK 1.8 or later. A mvn clean package should generate the executable Jar file.

## Execution

It is advised to execute the different modules through [mdedup_utils](https://gitlab.com/mdedup/mdedup_utils), which is developed to automate the execution across steps and ease the arguments selection.

All executions start from de.hpi.is.mdedup.MDedupControllerFactory.

A brief description of the main packages:
*  selection: This is where the main algorithm that is described in the section "MDC selection" of the paper.
*  expansion: MDCs are expanded using the two approaches of (gaussian) neighbors and (uniform) random.
*  exploration: Features for MDCs are generated here, for both selection and expansion MDCs.
*  prediction: This is where the Gaussian Process regression model fetches the MDCs of exploration of the rest datasets to train and then be applied on another dataset (the remaining one, in our evalations).
*  boosting: A Support Vector Machine is trained upon the results produced either by selection or prediction and then refines the sets of duplicate and non-duplicate pairs.